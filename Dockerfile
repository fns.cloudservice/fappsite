FROM node:15.11.0-alpine AS build
RUN mkdir -p /appdata

WORKDIR /appdata
COPY package.json package-lock.json ./
RUN npm install -g npm@7.14.0  
RUN npm install -g @angular/cli@8.3.25
COPY . .
EXPOSE 3000
