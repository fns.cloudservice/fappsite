// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBjUw74UyzN07UiA1fOIn1QdU8smPh9n0M",
    authDomain: "fnsapp-c2770.firebaseapp.com",
    databaseURL: "https://fnsapp-c2770.firebaseio.com",
    projectId: "fnsapp-c2770",
    storageBucket: "fnsapp-c2770.appspot.com",
    messagingSenderId: "182105856334",
    appId: "1:182105856334:web:0773d22926ddeb86"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
