import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import {FirebaseUIModule, firebase, firebaseui} from 'firebaseui-angular';
import {AngularFireModule} from '@angular/fire';
import {AngularFireAuthModule} from '@angular/fire/auth';

if (environment.production) {
  enableProdMode();
}

//FirebaseUIModule.forRoot(firebaseUiAuthConfig: NativeFirebaseUIAuthConfig)


platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
