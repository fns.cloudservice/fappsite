import { Component, OnInit, Input } from '@angular/core';
import {SharedService} from '../service/shared.service';
import {BreadCrumbInfoLang, BreadCrumbInfoLangText} from './breadcrumb.lang';
import { Router } from '@angular/router';

@Component({
  selector: 'breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadCrumbComponent implements OnInit {
  public lang:BreadCrumbInfoLangText; 
  Pagetitle:string = "";
  isHome:boolean = false;
  constructor(private route:Router, private sharedService: SharedService) { 
    if(localStorage.getItem("lang") == "spanish"){
      this.lang = new BreadCrumbInfoLang().lang["spanish"];
    }else{
      this.lang = new BreadCrumbInfoLang().lang["english"];
    }
    this.sharedService.Pagetitle.subscribe( value => {
      this.Pagetitle = value;
    });
    
    if(location.href.toLowerCase().indexOf("home")>-1){
      this.isHome = true;
    }
  }

  ngOnInit() {
  }
  
  public Home(){
    this.route.navigateByUrl("home");    
  }

}
