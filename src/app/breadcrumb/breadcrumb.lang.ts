export class BreadCrumbInfoLang{
    public lang:any[];
    constructor(){        
        this.lang = new Array();
        var eng = new BreadCrumbInfoLangText();        
        eng.Step1 = "Step 1";
        eng.Step2 = "Step 2";

        var spn = new BreadCrumbInfoLangText();
        spn.Step1 = "Paso 1";
        spn.Step2 = "Paso 2";

        this.lang["english"] = eng;
        this.lang["spanish"] = spn;
    }
}

export class BreadCrumbInfoLangText{
    public Step1: string;
    public Step2: string;
}