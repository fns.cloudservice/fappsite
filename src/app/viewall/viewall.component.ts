import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import {SharedService} from '../service/shared.service';
import { MehkLoginResponse } from '../model/MehkLoginResponse';
import { Router } from '@angular/router';
import {AuthService} from '../service/auth.service';
import { ProfileResponse } from '../model/ProfileResponse';
import { NotifyResponse } from '../model/NotifyResponse';
import { profileInfo } from '../model/profile.model';
import { FAppService } from '../service/fapp.service';
import { headerInfo } from '../model/post.model';
import { ToastrService } from 'ngx-toastr';
import { NgxLoadingModule } from 'ngx-loading';
import { Message } from 'primeng/api';
import * as moment from 'moment';

@Component({
  selector: 'app-viewall',
  templateUrl: './viewall.component.html',
  styleUrls: ['./viewall.component.scss']
})
export class ViewallComponent implements OnInit {
  profileInfoData : profileInfo; savedData:ProfileResponse; headerInfo : headerInfo; loading = false; Notify : any; 

  constructor(private mehkService:FAppService, private sharedService:SharedService, private router:Router, private toastr: ToastrService, private auth: AuthService, 
    private fappService:FAppService, private route:Router) { 
      this.savedData = JSON.parse(localStorage.getItem("profileInfo")) as ProfileResponse;
      if(this.savedData && this.savedData.profileInfo){
        this.profileInfoData = this.savedData.profileInfo;
      }else{
        this.profileInfoData = new  profileInfo();
      }
      this.headerInfo = new headerInfo();
      this.sharedService.isReloadViewAll.subscribe( value => { 
        if(value){
          this.ngOnInit();
        }
      });
    }

  ngOnInit() {
    this.auth.isuserLoggedIn();
    this.getNotification();
  }

  public getNotification(){
    this.loading = true;
    this.headerInfo.mid = this.savedData.profileInfo.mid
    this.fappService.LoadNotificationInfo(this.headerInfo).subscribe(response => {
      if(response){
        const resp = response as NotifyResponse;
        if(response.status == "Success"){ 
          if(response.dtConnectionId == null){
            return;
          }
          this.Notify = [];
          if(response.dtConnectionId.length > 0){
            this.Notify = response.dtConnectionId;
          }
        }else{
          this.toastr.error(response.message);
        }
        this.loading = false;
      }
    });
  }

  public getRowNotification(iRow : any, key: any) : any {
    if(key == 'P')
      return this.Notify[iRow].postid;
    else if(key == 'M')
     return this.Notify[iRow].msg; 
    else if(key == 'I')
     return this.Notify[iRow].mid; 
    else if(key == 'IP')
     return this.Notify[iRow].imgpath; 
    else if(key == 'T'){
      var posttime = this.Notify[iRow].posttime, currentTime = Date.now().toString(), strRetVal = "";
      posttime = moment.unix(posttime/1000).format("DD MMM YYYY hh:mm a");
      currentTime = moment.unix(parseFloat(currentTime)/1000).format("DD MMM YYYY hh:mm a");

      //var ms = moment(currentTime,"DD/MM/YYYY HH:mm:ss").diff(moment(posttime,"DD/MM/YYYY HH:mm:ss"));
      //moment.unix(response.postInfo.posttime/1000).format("DD MMM YYYY hh:mm a");              d.days() + " " + d.hours() + " " + d.minutes()+ " " + d.seconds(); 
      var ms = moment(currentTime).diff(moment(posttime));
      var d = moment.duration(ms);

      //console.log(d.days(), d.hours(), d.minutes(), d.seconds());

      if(d.days() > 0){
        strRetVal = d.days().toString() + ' days ago.';
      }else if(d.hours() > 0){
        strRetVal = d.hours().toString() + ' hours ago.';
      }else if(d.minutes() > 0){
        strRetVal = d.minutes().toString() + ' minutes ago.';
      }else if(d.seconds() > 0){
        strRetVal = d.seconds().toString() + ' seconds ago.';
      }
      return strRetVal; 
    }
     
  }

  public ViewSinglePost(postid : any) {
    this.sharedService.vPostId.next(postid);
    this.route.navigateByUrl("viewpost");
  }

}
