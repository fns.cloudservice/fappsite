import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { AccountComponent } from './account/account.component';
import { LockComponent } from './lock/lock.component';
import { FriendlistComponent } from './friendlist/friendlist.component';
import { TimeLineComponent } from './timeline/timeline.component';
import { AboutComponent } from './about/about.component';
import { NearbyComponent } from './nearby/nearby.component';
import { NewsFeedComponent } from './newsfeed/newsfeed.component';
import { AlbumComponent } from './album/album.component';
import { ChangePwdComponent } from './changepwd/changepwd.component';
import { SignupComponent } from './signup/signup.component';
import { LandingComponent } from './landing/landing.component';
import { LoginComponent } from './login/login.component';
import {BreadCrumbComponent} from './breadcrumb/breadcrumb.component';
import { FileUploadComponent } from './fileupload/fileupload.component';
import { NewPostComponent } from './newpost/newpost.component';
import {FAppService} from './service/fapp.service';
import {DynamicScriptLoaderService} from './service/dynamicscriptLoader.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import {SharedService} from './service/shared.service';
import {AuthService} from './service/auth.service'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NgxLoadingModule } from 'ngx-loading';
import {FirebaseUIModule, firebase, firebaseui} from 'firebaseui-angular';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { AngularFirestoreModule} from 'angularfire2/firestore';
import { AngularFireStorageModule} from 'angularfire2/storage';

import { environment } from '../environments/environment';
import { DropZoneDirective } from './drop-zone.directive';
import { FileSizePipe } from './file-size.pipe';
import { UploadTaskComponent } from './upload-task/upload-task.component';
import { UploaderComponent } from './uploader/uploader.component';
import { DropzoneDirective } from './dropzone.directive';
import { UploadpreviewComponent } from './uploadpreview/uploadpreview.component';
import { TestpostComponent } from './testpost/testpost.component';
import { DisplayimgComponent } from './displayimg/displayimg.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CollapseModule } from 'ngx-bootstrap';
import { NavbarComponent } from './navbar/navbar.component';
import { DisplayimgpreviewComponent } from './displayimgpreview/displayimgpreview.component';
import { MypostComponent } from './mypost/mypost.component';
import { spostComponent } from './controls/spost/spost.component';
import { TSPostComponent } from './controls/tspost/tspost.component';
import { TestnewpostComponent } from './testnewpost/testnewpost.component';
import { TestalbumComponent } from './testalbum/testalbum.component';
import { SlideComponent } from './controls/slide/slide.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import {AlertsComponent} from "./controls/alerts/alerts.component";
import {ConfirmService, ConfirmState, ConfirmModalComponent, ConfirmTemplateDirective} from './service/confirm.service';
import { HelpComponent } from './help/help.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { TermsComponent } from './terms/terms.component';
import { SearchComponent } from './search/search.component';
import { PreviewSlideComponent } from './controls/previewslide/previewslide.component';
import { SingleFriendComponent } from './controls/singlefriend/singlefriend.component';
import { PopOverSFComponent } from './controls/popoversf/popoversf.component';
import { PopOverComponent } from './controls/popover/popover.component';
import { SFRComponent } from './controls/sfr/sfr.component';
import { SFSComponent } from './controls/sfs/sfs.component';
import { FindfriendComponent } from './findfriend/findfriend.component';
import { MessageComponent } from './message/message.component';
import { SinglepostComponent } from './controls/singlepost/singlepost.component';
import { SinglepostsilderComponent } from './controls/singlepostsilder/singlepostsilder.component';
import { ViewsinglepostComponent } from './viewsinglepost/viewsinglepost.component';
import { ViewallComponent } from './viewall/viewall.component';
import { ChatComponent } from './chat/chat.component';
import { FriendComponent } from './controls/friend/friend.component';
import { FriendrequestComponent } from './controls/friendrequest/friendrequest.component';
import { PopupfriendComponent } from './controls/popupfriend/popupfriend.component';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { LoadingBarModule, LoadingBarComponent } from '@ngx-loading-bar/core';
import { NgxScrollEventModule } from 'ngx-scroll-event';
import { FollowersComponent } from './followers/followers.component';
import { FollowerslistComponent } from './controls/followerslist/followerslist.component';
import { PostComponent } from './post/post.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';


//import { GrowlModule } from 'primeng/primeng';

const firebaseUiAuthConfig: firebaseui.auth.Config = {
  signInFlow: 'popup', signInOptions: [
    {
      provider: firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      // Required to enable this provider in one-tap sign-up.
      authMethod: 'https://accounts.google.com',
      // Required to enable ID token credentials for this provider.
      // This can be obtained from the Credentials page of the Google APIs
      // clientId: '1453162809-t9otkobkfiqgb1l6fb32cnq53arnoqnu.apps.googleusercontent.com'
      clientId: '182105856334-181d8gus05seljq1713qtvvcefofhef5.apps.googleusercontent.com'
    }, firebase.auth.FacebookAuthProvider.PROVIDER_ID, firebase.auth.TwitterAuthProvider.PROVIDER_ID, firebase.auth.EmailAuthProvider.PROVIDER_ID
  ], tosUrl: 'home', privacyPolicyUrl: '<your-privacyPolicyUrl-link>', credentialHelper: firebaseui.auth.CredentialHelper.ACCOUNT_CHOOSER_COM
};

@NgModule({
  entryComponents: [
    SinglepostComponent
  ],
  declarations: [
    AppComponent, HeaderComponent, FooterComponent, HomeComponent, AccountComponent, LockComponent, SignupComponent, LandingComponent, ProfileComponent,
    TimeLineComponent, AlbumComponent, AboutComponent, NearbyComponent, NewsFeedComponent, LoginComponent, BreadCrumbComponent, ChangePwdComponent, DropZoneDirective,
    FileUploadComponent, NewPostComponent, FileSizePipe, UploaderComponent, UploadTaskComponent, DropzoneDirective, UploadpreviewComponent, TestpostComponent,
    DisplayimgComponent, NavbarComponent, DisplayimgpreviewComponent, MypostComponent, spostComponent, TestnewpostComponent, TestalbumComponent, SlideComponent,
    AlertsComponent, ConfirmModalComponent, ConfirmTemplateDirective, HelpComponent, PrivacyComponent, TermsComponent, SearchComponent, PreviewSlideComponent,
    FindfriendComponent, MessageComponent, SingleFriendComponent, PopOverSFComponent, PopOverComponent, SFRComponent, SFSComponent, FriendlistComponent,
    TSPostComponent, SinglepostComponent, SinglepostsilderComponent, ViewsinglepostComponent, ViewallComponent, ChatComponent, FriendComponent, FriendrequestComponent, 
    PopupfriendComponent, FollowersComponent, FollowerslistComponent, PostComponent
  ],
  imports: [
    BrowserModule, AppRoutingModule, HttpClientModule, FormsModule, BrowserAnimationsModule, ReactiveFormsModule, AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule, FirebaseUIModule.forRoot(firebaseUiAuthConfig), AngularFirestoreModule, AngularFireStorageModule, ModalModule.forRoot(), CollapseModule, NgbModule,
    ToastrModule.forRoot({ timeOut: 2000, positionClass: 'toast-top-right', closeButton: true }),NgxLoadingModule.forRoot({}),LoadingBarHttpClientModule,LoadingBarRouterModule,
    LoadingBarModule,NgxScrollEventModule
  ],
  providers: [AuthService,FAppService, SharedService, DynamicScriptLoaderService, ConfirmService, ConfirmState,  {provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
