import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FAppService } from '../service/fapp.service';
import {SharedService} from '../service/shared.service';
import { ProfileResponse } from '../model/ProfileResponse';
import { profileInfo } from '../model/profile.model';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../service/auth.service';
import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  profileInfoData : profileInfo;
  savedData:ProfileResponse;
  url:string;

  task: AngularFireUploadTask;
  percentage: Observable<number>;
  snapshot: Observable<any>;
  downloadURL: Observable<string>;
  isHovering: boolean;
  userdefaultImgPath:string="../assets/layouts/layout4/img/usr.jpg";

  @ViewChild('fileInput', { static: true }) fileInput: ElementRef;
  CurrentUser: string;
  userImgPath: string;

  constructor(private mehkService:FAppService, private sharedService:SharedService, private router:Router, private toastr: ToastrService, private auth: AuthService,
    private storage: AngularFireStorage, private db: AngularFirestore) {
    //this.FetchProfileInfo();
    //debugger;
    this.sharedService.CurrentUser.subscribe( value => { this.CurrentUser = value; });
    this.sharedService.userImgPath.subscribe( value => { this.userImgPath = value; });
    this.savedData = JSON.parse(localStorage.getItem("profileInfo")) as ProfileResponse;
    if(this.savedData && this.savedData.profileInfo){
      this.profileInfoData = this.savedData.profileInfo;
    }else{
      this.profileInfoData = new  profileInfo();
    }
    if(this.profileInfoData.filepath == ""){
      this.profileInfoData.filepath = this.userdefaultImgPath;
    }
    this.profileInfoData.uid = this.auth.currentUserId;
    this.profileInfoData.emailid = this.auth.currentEmailId;
   }

  ngOnInit() {
    this.auth.isuserLoggedIn();
    this.LoadprofileJsFiles();
  }

  ngAfterContentChecked() {
    
  }

  public NavToAbout(){
    this.router.navigateByUrl("about");
  }

  public SaveProfileInfo(){
    this.mehkService.ProfileInfoFApp(this.profileInfoData).subscribe(response => {
      if(response){
        if(response.status=="Success"){
          this.savedData.profileInfo = this.profileInfoData;
          localStorage.setItem("profileInfo", JSON.stringify(this.savedData));
          this.toastr.success(response.message);//, null, {closeButton: true}
          this.router.navigateByUrl("profile");
        }else{
          this.toastr.error(response.message);
        }
      }else{
        this.toastr.error("something gone wrong !");
      }
    });
  }

  isActive(snapshot) {
    return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes
  }

  public onFileChange(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      const allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
      if (!allowedExtensions.exec(file.name)) {
          alert('Please upload image having extensions .jpeg/.jpg/.png/.gif only');
          this.fileInput.nativeElement.value = '';
          return false;
      } if ((file.size / 4096 / 4096 ) > 1) {
        alert('Please upload image size upto 4 MB');
          this.fileInput.nativeElement.value = '';
          return false;
      } else {
        // alert("FileSize : " + file.size / 4096 / 4096);
        // var FileSize = file.size / 1024 / 1024; // in MB
        // alert(FileSize);
        // if (FileSize > 2) {
        //     alert('File size exceeds 2 MB');
        //     return false;
        // }
        //debugger;
          reader.readAsDataURL(file);
          this.profileInfoData.file_name = file.name;
          reader.onload = () => {
            //this.profileInfoData.file = reader.result.toString().split(',')[1];
            //console.log(this.profileInfoData);

            const path = `fns/${new Date().getTime()}_${file.name}`;
            const customMetadata = { app: 'My AngularFire-powered PWA!' };
            this.task = this.storage.upload(path, file, { customMetadata })
            this.percentage = this.task.percentageChanges();
            this.snapshot   = this.task.snapshotChanges()
            const ref = this.storage.ref(path);
            //this.downloadURL = ref.getDownloadURL();

            setTimeout(() => { const url = ref.getDownloadURL().subscribe(url => {
                const Url = url; // for ts
                this.url = url // with this you can use it in the html
               // console.log(Url);
                this.profileInfoData.filepath = url;
                this.sharedService.userImgPath.next(this.profileInfoData.filepath);
                this.SaveProfileInfo();
            }) }, 1000);

            //this.toastr.success("Your profile picture uploaded successfully!");

            this.snapshot = this.task.snapshotChanges().pipe(
              tap(snap => {
                if (snap.bytesTransferred === snap.totalBytes) {
                  // Update firestore on completion
                  this.db.collection('photos').add( { path, size: snap.totalBytes })
                }
              })
            )
          };
      }
    }
  }

  public FetchProfileInfo(){
    this.mehkService.ProfileInfoFApp(this.profileInfoData).subscribe(response => {
      if(response){
        if(response.status=="Success"){
          //this.savedData.profileInfo = this.profileInfoData;
          //localStorage.setItem("UserInfo", JSON.stringify(this.savedData));
          //this.router.navigateByUrl("time");
          this.toastr.success(response.message);
        }else{
          this.toastr.error(response.message);
        }
      }else{
        this.toastr.error("something gone wrong !");
      }
    });
  }
  removeFile(){
    this.profileInfoData.filepath=this.userdefaultImgPath;
    this.sharedService.userImgPath.next(this.profileInfoData.filepath);
    this.SaveProfileInfo();

  }

  public loadScript(url) {
    //console.log('preparing to load...')
    let node = document.createElement('script');
    node.src = url;
    node.type = 'text/javascript';
    document.getElementsByTagName('head')[0].appendChild(node);
 }

 private LoadprofileJsFiles(){
  this.loadScript('../../assets/global/plugins/jquery.min.js');
  this.loadScript('../assets/global/plugins/bootstrap/js/bootstrap.min.js');
  this.loadScript('../assets/global/plugins/js.cookie.min.js');
  this.loadScript('../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js');
  this.loadScript('../assets/global/plugins/jquery.blockui.min.js');
  this.loadScript('../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
  this.loadScript('../assets/global/plugins/jquery-ui/jquery-ui.min.js');
  this.loadScript('../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js');
  this.loadScript('../assets/global/plugins/jquery.sparkline.min.js');
  this.loadScript('../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');
  this.loadScript('../assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js');
  this.loadScript('../assets/global/plugins/select2/js/select2.full.min.js');
  this.loadScript('../assets/global/scripts/app.min.js');
  this.loadScript('../assets/pages/scripts/profile.min.js');
//  this.loadScript('../assets/apps/scripts/todo-2.min.js');
  this.loadScript('../assets/pages/scripts/components-date-time-pickers.min.js');
  this.loadScript('../assets/pages/scripts/portfolio-1.min.js');
  this.loadScript('../assets/pages/scripts/ui-modals.min.js');
  this.loadScript('../assets/layouts/layout4/scripts/layout.min.js');
  this.loadScript('../assets/layouts/layout4/scripts/demo.min.js');
  this.loadScript('../assets/layouts/global/scripts/quick-sidebar.min.js');
  this.loadScript('../assets/layouts/global/scripts/quick-nav.min.js');
}



}
