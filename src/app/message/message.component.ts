import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {SharedService} from '../service/shared.service';
import { MehkLoginResponse } from '../model/MehkLoginResponse';
import {AuthService} from '../service/auth.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {

  constructor(private router:Router, private sharedService: SharedService, private auth: AuthService) { }

  ngOnInit() {
    this.auth.isuserLoggedIn();
  }

  ngAfterContentChecked() {
    
  }

}
