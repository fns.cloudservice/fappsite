import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd,ActivatedRoute } from '@angular/router';
import { AuthService } from '../service/auth.service';
import { FAppService } from '../service/fapp.service';
import { ProfileResponse } from '../model/ProfileResponse';
import {Location} from '@angular/common';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {parameter:string;PostId:string;

  constructor(private route:Router, private router:ActivatedRoute,private auth:AuthService,private fapp:FAppService, private _location: Location) { }

  ngOnInit() {
    this.auth.isuserLoggedIn();
    this.parameter= this.router.snapshot.paramMap.get("id")
   
    this.PostId=this.parameter
    console.log(this.parameter,this.PostId)
  }

  ngAfterContentChecked() {
   
  }
  
  private OnBackClick(){
    // let el: HTMLElement = document.getElementById("btnBack");
    // el.setAttribute("href", "#/search"); 
    // el.click();

    //this.route.navigateByUrl("search");
    this._location.back();
  }

}
