import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FAppService } from '../service/fapp.service';
import {SharedService} from '../service/shared.service';
import { ProfileResponse } from '../model/ProfileResponse';
import { profileInfo } from '../model/profile.model';
import { aboutInfo } from '../model/about.model';
import { ToastrService } from 'ngx-toastr';
import {AuthService} from '../service/auth.service';

@Component({
  selector: 'about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  aboutInfoData : aboutInfo;
  profileInfoData : profileInfo;
  savedData:ProfileResponse;
  gender:string;

  constructor(private fappService:FAppService, private sharedService:SharedService, private router:Router, private toastr: ToastrService, private auth: AuthService) {
    //this.FetchAboutInfo();
    this.savedData = JSON.parse(localStorage.getItem("profileInfo")) as ProfileResponse;
    if(this.savedData && this.savedData.profileInfo){
      this.profileInfoData = this.savedData.profileInfo;
      this.gender = this.profileInfoData.gender == "M" ? "Male" : this.profileInfoData.gender == "F" ? "Female" : "Other";
    }else{
      this.profileInfoData = new  profileInfo();
    }

   }

  ngOnInit() {
    this.auth.isuserLoggedIn();
  }
  
  ngAfterContentChecked() {
 
  }
  
  public NavToProfile(){
    this.router.navigateByUrl("profile");
  }

  public NavToHome(){
    this.router.navigateByUrl("home");
  }

  public FetchAboutInfo(){
    this.fappService.AboutInfoFApp(this.aboutInfoData).subscribe(response => {
      if(response){
        if(response.status=="Success"){    
          //this.savedData.aboutInfo = this.aboutInfoData;
          localStorage.setItem("UserInfo", JSON.stringify(this.savedData));
          //this.router.navigateByUrl("time");
          this.toastr.success(response.message, 'FAPP WebSite');
        }else{
          this.toastr.error(response.message, 'FAPP WebSite');
        }
      }else{
        this.toastr.error("something gone wrong !", 'FAPP WebSite');
      }
    });
  }

}
