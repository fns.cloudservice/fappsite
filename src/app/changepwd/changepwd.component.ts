import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FAppService } from '../service/fapp.service';
import {SharedService} from '../service/shared.service';
import { MehkLoginResponse } from '../model/MehkLoginResponse';
import { profileInfo } from '../model/profile.model';
import { ToastrService } from 'ngx-toastr';
import {AuthService} from '../service/auth.service';

@Component({
  selector: 'changepwd',
  templateUrl: './changepwd.component.html',
  styleUrls: ['./changepwd.component.scss']
})
export class ChangePwdComponent implements OnInit {
  profileInfoData : profileInfo;
  savedData:MehkLoginResponse;
  
  constructor(private mehkService:FAppService, private sharedService:SharedService, private router:Router, private toastr: ToastrService, private auth: AuthService) {
    //this.FetchProfileInfo();
    if(this.savedData && this.savedData.profileInfo){
      this.profileInfoData = this.savedData.profileInfo;
    }else{
      this.profileInfoData = new  profileInfo();
    }
    
    
   }

  ngOnInit() {
  }
  
  ngAfterContentChecked() {
    this.auth.isuserLoggedIn();
  }

  public SaveProfileInfo(){debugger;
    this.mehkService.ProfileInfoFApp(this.profileInfoData).subscribe(response => {
      if(response){
        if(response.status=="Success"){    
          this.savedData.profileInfo = this.profileInfoData;
          localStorage.setItem("UserInfo", JSON.stringify(this.savedData));
          //this.router.navigateByUrl("time");
          this.toastr.success(response.message, 'FAPP WebSite');
        }else{
          this.toastr.error(response.message, 'FAPP WebSite');
        }
      }else{
        this.toastr.error("something gone wrong !", 'FAPP WebSite');
      }
    });
  }

}
