import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {LandingLang,LandingLangText} from './landing.lang';
import {SharedService} from '../service/shared.service';
import { MehkLoginResponse } from '../model/MehkLoginResponse';

@Component({
  selector: 'app-home',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  public lang:LandingLangText;
  savedData:MehkLoginResponse;
  
  constructor(private router:Router, private sharedService: SharedService) {
    this.sharedService.isShowHome.next(false);
    this.sharedService.Pagetitle.next("Landing");
    //this.sharedService.LoginEvent.emit(true);
    this.savedData = JSON.parse(localStorage.getItem("UserInfo")) as MehkLoginResponse ;
    if(localStorage.getItem("lang") == "spanish"){
      this.lang = new LandingLang().lang["spanish"];
    }else{
      this.lang = new LandingLang().lang["english"];
    }
    if(this.savedData != null && this.savedData.signUpUsers){
      // this.signupData = savedData.signUpUsers;
      // this.terms = true;
      // this.isexistingUser = true;
    }else{
      this.router.navigateByUrl("login");
    }  
   }

  ngOnInit() {
  }
  public MehkApplication(){
    this.router.navigateByUrl("application");
  }
}
