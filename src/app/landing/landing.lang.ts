export class LandingLang{
    public lang:any[];
    constructor(){        
        this.lang = new Array();
        var eng = new LandingLangText();        
        eng.MehkPermitApplicationh2 = "MEHK PERMIT APPLICATION - RIVERSIDE COUNTY";
        eng.Para1 = "This is the microenterprise home kitchen permit application for Riverside county. This will allow you to open and operate a home based food sales business under the new AB-626 law.";
        eng.Steps = "STEPS";
        eng.Step1 = "1- Click the next button and complete the online form.";
        eng.Step2 = "2- Click the SUBMIT button at the end of the form and a signed copy will be emailed to you and also sent to the county office. ";
        eng.cost = "The cost for the application is $651 and will have to be paid at your local county office. ";
        eng.lastpara = "If you haven't already, check out our Complete Riverside County AB 626 Start Up Guide and if you get hung up just email us at chefhelp@homemadefood.app and we would be more than happy to go over the process with you and answer any questions. Also feel free to start a live chat with one of our team members in the bottom right of your screen if you need help now. ";
        eng.happycooking = "Happy Cooking!";
        eng.team = "The MEHK Team ";
        eng.Next ="Next";

        var spn = new LandingLangText();
        spn.MehkPermitApplicationh2 = "SOLICITUD DE PERMISO MEHK - CONDADO DE RIVERSIDE";
        spn.Para1 = "Esta es la solicitud de permiso de cocina de hogar microempresa para el condado de Riverside. Esto le permitirá abrir y operar un negocio de venta de alimentos en el hogar según la nueva ley AB-626.";
        spn.Steps = "PASAS";
        spn.Step1 = "1- Haga clic en el siguiente botón y complete el formulario en línea.";
        spn.Step2 = "2- Haga clic en el botón ENVIAR al final del formulario y se le enviará una copia firmada por correo electrónico que también se enviará a la oficina del condado.";
        spn.cost = "El costo de la solicitud es de $ 651 y deberá pagarse en la oficina local del condado.";
        spn.lastpara = "Si aún no lo ha hecho, consulte nuestra Guía de inicio completa del condado de Riverside AB 626 y, si se queda colgado, envíenos un correo electrónico a chefhelp@homemadefood.app y estaremos encantados de revisar el proceso con usted y responder cualquier pregunta. preguntas También puede comenzar un chat en vivo con uno de los miembros de nuestro equipo en la parte inferior derecha de su pantalla si necesita ayuda ahora.";
        spn.happycooking = "¡Feliz cocina!";
        spn.team = "El equipo MEHK";
        spn.Next ="Siguiente";

        this.lang["english"] = eng;
        this.lang["spanish"] = spn;
    }
}

export class LandingLangText{
    public Introductionh2:string;
    public MehkPermitApplicationh2:string;
    public Para1:string;
    public Steps:string;
    public Step1:string;
    public Step2:string;
    public cost:string;
    public lastpara:string;
    public happycooking:string;
    public team:string;
    public Next:string;
}