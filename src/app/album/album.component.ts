import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {SharedService} from '../service/shared.service';
import { MehkLoginResponse } from '../model/MehkLoginResponse';
import { DynamicScriptLoaderService } from '../service/dynamicscriptLoader.service';
import {AuthService} from '../service/auth.service';

declare function fnLoadJs(): any;
declare function fnRemoveAlbumdiv(): any;

@Component({
  selector: 'album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.scss']
})
export class AlbumComponent implements OnInit {
  savedData:MehkLoginResponse;
  
  constructor(private router:Router, private sharedService: SharedService, private dynamicScriptLoader: DynamicScriptLoaderService, private auth: AuthService) {
    console.log('constructor');
      localStorage.setItem("isdivcreated", "N");
   }

  ngOnInit() {
    this.auth.isuserLoggedIn();
    //this.loadScripts();
    //fnLoadJs();
    console.log('ngOnInit');
    this.LoadJsFiles();
    //this.LoadAllJsFiles();
  }

  ngAfterViewInit() {
    console.log('ngAfterViewInit');
    //We loading the player script on after view is loaded
    //this.loadScripts();
    //fnLoadJs();
    //this.LoadAllJsFiles();
    //this.LoadJsFiles();
    //fnRemoveAlbumdiv();
  }

  ngAfterContentChecked() {
    
  }
  
  OnClick(){
    setTimeout(()=> { fnRemoveAlbumdiv(); } , 750);
  }

  private loadScripts() {
    // You can load multiple scripts by just providing the key as argument into load method of the service
    this.dynamicScriptLoader.load('jquery','bootstrap','js.cookie','slimscroll','jquery.blockui','bootstrap-switch','bootstrap-fileinput','jquery.sparkline',
    'bootstrap-datepicker','cubeportfolio','app.min','profile','components-date-time-pickers','portfolio1','ui-modals','layout','demo','quick-sidebar',
    'quick-nav').then(data => {
      // Script Loaded Successfully
    }).catch(error => console.log(error));
  }

  public loadScript(url) {
    //console.log('preparing to load...')
    let node = document.createElement('script');
    node.src = url;
    node.type = 'text/javascript';
    document.getElementsByTagName('head')[0].appendChild(node);
 }

 private LoadJsFiles(){
    this.loadScript('../assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js'); 
    //this.loadScript('../assets/global/scripts/app.min.js'); 
    this.loadScript('../assets/pages/scripts/portfolio-1.min.js'); 
} 

 private LoadJsFiles_old(){
    this.loadScript('../../assets/global/plugins/jquery.min.js');
    this.loadScript('../assets/global/plugins/bootstrap/js/bootstrap.min.js');
    this.loadScript('../assets/global/plugins/js.cookie.min.js');     
    this.loadScript('../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); 
    this.loadScript('../assets/global/plugins/jquery.blockui.min.js'); 
    this.loadScript('../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); 
    this.loadScript('../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js');
    this.loadScript('../assets/global/plugins/jquery.sparkline.min.js');     
    this.loadScript('../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js'); 
    this.loadScript('../assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js'); 
    this.loadScript('../assets/global/scripts/app.min.js'); 
    this.loadScript('../assets/pages/scripts/profile.min.js');
    this.loadScript('../assets/pages/scripts/components-date-time-pickers.min.js');     
    this.loadScript('../assets/pages/scripts/portfolio-1.min.js'); 
    //this.loadScript('../assets/pages/scripts/ui-modals.min.js'); 
    this.loadScript('../assets/layouts/layout4/scripts/layout.min.js'); 
    this.loadScript('../assets/layouts/layout4/scripts/demo.min.js');
    this.loadScript('../assets/layouts/global/scripts/quick-sidebar.min.js');     
    this.loadScript('../assets/layouts/global/scripts/quick-nav.min.js'); 
 }

 private LoadAllJsFiles(){
  this.loadScript('../../assets/global/plugins/jquery.min.js');
  this.loadScript('../assets/global/plugins/bootstrap/js/bootstrap.min.js');
  this.loadScript('../assets/global/plugins/js.cookie.min.js');     
  this.loadScript('../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); 
  this.loadScript('../assets/global/plugins/jquery.blockui.min.js'); 
  this.loadScript('../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
  this.loadScript('../assets/global/plugins/jquery-ui/jquery-ui.min.js');  
  this.loadScript('../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js');
  this.loadScript('../assets/global/plugins/jquery.sparkline.min.js');     
  this.loadScript('../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js'); 
  this.loadScript('../assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js'); 
  this.loadScript('../assets/global/plugins/select2/js/select2.full.min.js');
  this.loadScript('../assets/global/scripts/app.min.js'); 
  this.loadScript('../assets/pages/scripts/profile.min.js');
  this.loadScript('../assets/apps/scripts/todo-2.min.js');
  this.loadScript('../assets/pages/scripts/components-date-time-pickers.min.js');     
  this.loadScript('../assets/pages/scripts/portfolio-1.min.js'); 
  this.loadScript('../assets/pages/scripts/ui-modals.min.js'); 
  this.loadScript('../assets/layouts/layout4/scripts/layout.min.js'); 
  this.loadScript('../assets/layouts/layout4/scripts/demo.min.js');
  this.loadScript('../assets/layouts/global/scripts/quick-sidebar.min.js');     
  this.loadScript('../assets/layouts/global/scripts/quick-nav.min.js'); 
}

}
