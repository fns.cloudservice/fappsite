import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {SharedService} from '../service/shared.service';
import { MehkLoginResponse } from '../model/MehkLoginResponse';
import {AuthService} from '../service/auth.service';
import { ToastrService } from 'ngx-toastr';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { finalize, tap } from 'rxjs/operators';


declare function fnWizardClick(e): any;
declare function fnCTAClick(): any;
declare function fnMoveNext(): any;
declare function fnMoveBack(): any;
declare function fnRemoveAlbumdiv(): any;

@Component({
  selector: 'testnewpost',
  templateUrl: './testnewpost.component.html',
  styleUrls: ['./testnewpost.component.scss']
})
export class TestnewpostComponent implements OnInit {
  savedData:MehkLoginResponse; isHovering: boolean; htmlToAdd:string; imgURL: any; public imagePath; iUploadedFileCount : number;
  task: AngularFireUploadTask; percentage: Observable<number>; snapshot: Observable<any>; downloadURL: string;
  postConent:string;
  imgdesc;string;

  @ViewChild('dz', { static: true }) dz: ElementRef;
  @ViewChild('cta', { static: true }) cta: ElementRef;

  files: File[] = [];

  toggleHover(event: boolean) {
    this.isHovering = event;
  }

  onDrop(files: FileList) {
    //this.files = [];
    for (let i = 0; i < files.length; i++) {
      this.files.push(files.item(i));
    }
  }

  constructor(private router:Router, private sharedService: SharedService, private auth: AuthService, private storage: AngularFireStorage, private db: AngularFirestore, private toastr: ToastrService) {
    this.sharedService.FileEvent.subscribe( data => {
      if(data){
        console.log("data is : " + data.name);
        for (let i = 0; i < this.files.length; i++) {
          if(data.name == this.files[i].name)
            this.files.splice(i, 1);
        }
      }
    });
    this.iUploadedFileCount = 0;
    localStorage.setItem("ctab", "1");
   }

   ngOnInit() {
   // this.LoadJsFiles();
   
  }

  onChange(data: any): void {
    var desc = data.image.description;
    console.log(data);
    console.log(" desc : " + desc);
    this.imgdesc = desc;
  }

  OnPreviewOpen(data: any): void {
    //this.OnPreviewOpen.s;


    console.log(data);
  }

  ngAfterViewChecked(){

  }

  ngAfterContentChecked() {
    //this.auth.isuserLoggedIn();
  }

  OnSelectFiles(){
    let el: HTMLElement = this.cta.nativeElement;
    el.click();
  }

  OnBack(){
    fnMoveBack();
  }

  OnContinue(){
    fnMoveNext();
  }

  OnMenuClick(e : Event){
    fnWizardClick(e);
  }

  startDisplay(event: FileList) {
    //this.files = [];
    for (let i = 0; i < event.length; i++) {
        this.files.push(event.item(i));
    }
  }

  OnUploadFiles(){
    for (let i = 0; i < this.files.length; i++) {
      this.startUpload(this.files[i]);
    }
    this.toastr.success(this.iUploadedFileCount + " files uploaded successfully!");
  }

  startUpload(file: File) {

    var mimeType = file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.toastr.warning(file.name + " is not uploaded! Only images are supported.");
      return;
    }

    this.iUploadedFileCount += 1;

    // The storage path
    const path = `post/${Date.now()}_${file.name}`;

    // Reference to storage bucket
    const ref = this.storage.ref(path);

    // The main task
    this.task = this.storage.upload(path, file);

    // Progress monitoring
    this.percentage = this.task.percentageChanges();

    this.snapshot   = this.task.snapshotChanges().pipe(
      tap(console.log),
      // The file's download URL
      finalize( async() =>  {
        this.downloadURL = await ref.getDownloadURL().toPromise();

        this.db.collection('files').add( { downloadURL: this.downloadURL, path });
      }),
    );
  }

  isActive(snapshot) {
    return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes;
  }

  OnClick(){
    setTimeout(()=> { fnRemoveAlbumdiv(); } , 750);
  }

  public loadScript(url) {
    //console.log('preparing to load...')
    let node = document.createElement('script');
    node.src = url;
    node.type = 'text/javascript';
    document.getElementsByTagName('head')[0].appendChild(node);
 }

 private LoadJsFiles(){
    this.loadScript('../assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js');
    //this.loadScript('../assets/global/scripts/app.min.js');
    this.loadScript('../assets/pages/scripts/portfolio-1.min.js');
}

}

