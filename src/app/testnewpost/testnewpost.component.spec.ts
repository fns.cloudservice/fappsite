import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestnewpostComponent } from './testnewpost.component';

describe('TestnewpostComponent', () => {
  let component: TestnewpostComponent;
  let fixture: ComponentFixture<TestnewpostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestnewpostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestnewpostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
