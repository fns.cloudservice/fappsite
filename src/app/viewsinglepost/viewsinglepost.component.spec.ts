import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewsinglepostComponent } from './viewsinglepost.component';

describe('ViewsinglepostComponent', () => {
  let component: ViewsinglepostComponent;
  let fixture: ComponentFixture<ViewsinglepostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewsinglepostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewsinglepostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
