import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import {AlertsComponent} from "../controls/alerts/alerts.component"; 
import { Router, NavigationEnd } from '@angular/router';
import { FAppService } from '../service/fapp.service';
import { SharedService } from '../service/shared.service';
import { ConfirmService } from '../service/confirm.service';
import { ProfileResponse } from '../model/ProfileResponse';
import { profileInfo } from '../model/profile.model';
import { PostResponse } from '../model/PostResponse';
import { postInfo } from '../model/post.model';
import { ToastrService } from 'ngx-toastr';
import {AuthService} from '../service/auth.service';
import { finalize, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import * as _ from 'lodash';

@Component({
  selector: 'app-viewsinglepost',
  templateUrl: './viewsinglepost.component.html',
  styleUrls: ['./viewsinglepost.component.scss']
})
export class ViewsinglepostComponent implements OnInit {
  posts: string[] = [];//["IP5DIQ5KVYYT6YYVIJT43","4TDEJfDDGVV5DKOTTTQ4P","YQGGDJHGTH3OJ4EKII4DQ"];
  people: any[] = [ {"PostId": "IP5DIQ5KVYYT6YYVIJT43"},{"PostId": "4TDEJfDDGVV5DKOTTTQ4P"},{"PostId": "YQGGDJHGTH3OJ4EKII4DQ"} ];
  savedData:ProfileResponse; profileInfoData : profileInfo; postInfo : postInfo; vPostId : any;

  @ViewChild(AlertsComponent, { static: true }) alert: AlertsComponent;

  constructor(private fappService:FAppService, private sharedService:SharedService, private router:Router, private toastr: ToastrService, 
    private auth: AuthService, private confirmService: ConfirmService) { 
    this.savedData = JSON.parse(localStorage.getItem("profileInfo")) as ProfileResponse;
    this.postInfo = new postInfo();
    this.sharedService.vPostId.subscribe( value => { 
      this.vPostId = value;
      this.ngOnInit();
     });
  }

  ngOnInit() {
    this.auth.isuserLoggedIn();
    this.LoadpostInfo(); 
  }

  ngAfterContentChecked() {
    
  }

  LoadpostInfo(){
    this.posts = [];
    var postId = this.vPostId;
    if(postId === undefined){
      return false;
    }
    this.posts.push(postId);
  }

}
