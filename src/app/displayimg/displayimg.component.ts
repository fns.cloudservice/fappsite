import { Component, OnInit, Input, ChangeDetectorRef, ElementRef, ViewChild, TemplateRef  } from '@angular/core';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AngularFirestore } from '@angular/fire/firestore';
import {SharedService} from '../service/shared.service';
import { finalize, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';


@Component({
  selector: 'displayimg',
  templateUrl: './displayimg.component.html',
  styleUrls: ['./displayimg.component.scss']
})
export class DisplayimgComponent implements OnInit {
  @Input() file: File;

  ImgDesc:string;
  imgURL: any; fSize ; number;
  public imagePath;
  public errormsg: string;
  public fName: string;
  modalRef: BsModalRef;
  config = { backdrop: true, ignoreBackdropClick: false };

  @ViewChild('start', { static: false }) start: ElementRef; 
  @ViewChild('comment', { static: false }) comment: ElementRef;

  constructor(private modalService: BsModalService,private storage: AngularFireStorage, private db: AngularFirestore, private sharedService: SharedService) { }

  ngOnInit() {
    this.startDisplay();
    //setTimeout(()=> { this.start.nativeElement.click(); } , 5000);
  }

  OnComment(){
    if(this.ImgDesc != ""){
      this.comment.nativeElement.classList.add('fa-commenting-o');
      this.comment.nativeElement.classList.remove('fa-comment-o');
    }
    else{
      this.comment.nativeElement.classList.add('fa-comment-o');
      this.comment.nativeElement.classList.remove('fa-commenting-o');
    }
    this.sharedService.Img_desc.next(this.ImgDesc + "#" + this.file.name);
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }

  preview(files) {
    if (files.length === 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.errormsg = "Only images are supported.";
      return;
    }
 
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
  }

  startDisplay() {

    var mimeType = this.file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.errormsg = "Only images are supported.";
      //return;
    }

    var reader = new FileReader();
    this.imagePath = this.file;
    this.fSize = this.file.size;
    this.fName = this.file.name;
    reader.readAsDataURL(this.file); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }

  }

  OnDeleteImg(obj : any){
    //debugger;
    //obj.remove();
    this.sharedService.FileEvent.emit(obj);

  }
}
