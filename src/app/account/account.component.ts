import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {FAppService} from '../service/fapp.service';
import {SharedService} from '../service/shared.service';
import { MehkLoginResponse } from '../model/MehkLoginResponse';
import {AuthService} from '../service/auth.service';
import { accountInfo } from '../model/account.model';
import { ToastrService } from 'ngx-toastr';

declare function fnCloseModel():any;

@Component({
  selector: 'account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  savedData:MehkLoginResponse;
  accountInfo : accountInfo;

  constructor(private fappService:FAppService, private router:Router, private sharedService: SharedService, private auth: AuthService, private toastr: ToastrService) {
    
    
   }

  ngOnInit() {
    this.auth.isuserLoggedIn();
  }
  
  ngAfterContentChecked() {
    
  }
  
  public NavToHome(){
    this.router.navigateByUrl("home");
  }
  
  public DeactivateAccount(){
    this.accountInfo = new accountInfo();
    this.accountInfo.uid = this.auth.currentUserId;
    this.accountInfo.isdeactivated = true;
    this.accountInfo.isdeleted = false;
    this.CallService();
  }

  public DeleteAccount(){
    this.accountInfo = new accountInfo();
    this.accountInfo.uid = this.auth.currentUserId;
    this.accountInfo.isdeactivated = false;
    this.accountInfo.isdeleted = true;
    this.CallService();
  }

  public CallService(){
    fnCloseModel();
    this.fappService.manageAccountInfo(this.accountInfo).subscribe(response => {
      if(response){
        if(response.status=="Success"){    
          this.toastr.warning(response.message);
          this.auth.signOut();
        }else{
          this.toastr.error(response.message);
        }
      }else{
        this.toastr.error("something gone wrong !");
      }
      fnCloseModel();
    });
  }

  


}
