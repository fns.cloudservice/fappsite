import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import {AlertsComponent} from "../controls/alerts/alerts.component";
import { Router, NavigationEnd } from '@angular/router';
import { FAppService } from '../service/fapp.service';
import { SharedService } from '../service/shared.service';
import { ConfirmService } from '../service/confirm.service';
import { ProfileResponse } from '../model/ProfileResponse';
import { profileInfo } from '../model/profile.model';
import { PostResponse } from '../model/PostResponse';
import { postInfo, ConnectionInfo } from '../model/post.model';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../service/auth.service';
import { SignalRService } from '../service/signalr.service';
import { finalize, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
//import * as _ from 'lodash';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { Message } from 'primeng/api';
import { NgxScrollEvent } from 'ngx-scroll-event';

@Component({
  selector: 'timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimeLineComponent implements OnInit {
  posts: string[] = [];//["IP5DIQ5KVYYT6YYVIJT43","4TDEJfDDGVV5DKOTTTQ4P","YQGGDJHGTH3OJ4EKII4DQ"];
  people: any[] = [ {"PostId": "IP5DIQ5KVYYT6YYVIJT43"},{"PostId": "4TDEJfDDGVV5DKOTTTQ4P"},{"PostId": "YQGGDJHGTH3OJ4EKII4DQ"} ];
  savedData:ProfileResponse; profileInfoData : profileInfo; postInfo : postInfo;strConnectionId : string; fireAsycLoad: string;
  private _hubConnection: HubConnection; msgs: Message[] = [];connectionInfo : ConnectionInfo; noofBottomClicks : number;showAlert:Boolean=false;

  @ViewChild(AlertsComponent, { static: true }) alert: AlertsComponent;

  constructor(private fappService:FAppService, private sharedService:SharedService, private router:Router, private toastr: ToastrService,
    private auth: AuthService, private confirmService: ConfirmService, private signalR:SignalRService) {
    this.savedData = JSON.parse(localStorage.getItem("profileInfo")) as ProfileResponse;
    this.postInfo = new postInfo();
    this.noofBottomClicks = 0; this.fireAsycLoad = 'Y';
    this.signalR.connectionIdReceived.subscribe( data => {
      //this.UpdateConnectionId('I');
    });
  }

  ngOnInit() {
    this.auth.isuserLoggedIn();
    //this.OpenHubConnection();
    /**** Commente since implemented globally */
    // if(!this.signalR.connectionExists){
    //   this.signalR.startConnection();
    //   this.signalR.addTransferFeedDataListener();
    //   this.subscribeToEvents();
    // }
    /**** End Of Commente since implemented globally */

    // if(this.signalR.connectionExists){
    //   this.signalR.addTransferFeedDataListener();
    //   this.subscribeToEvents();
    // }
    this.LoadpostInfo();
  }

  ngOnDestroy() {
    // console.log('OnDestroy Called!');
    // this.UpdateConnectionId('R');
    // this.signalR.connectionExists = false;
   }

  ngAfterContentChecked() {
    
  }

  private subscribeToEvents(): void {
      // if connection exists it can call of method.
      // this.signalR.connectionEstablished.subscribe(() => {
      //     //this.canSendMessage = true;
      // });

      // finally our service method to call when response received from server event and transfer response to some variable to be shwon on the browser.
      this.signalR.messageReceived.subscribe((message: any) => {
          //debugger;
          this.toastr.info(message[0].summary);
      });
  }

  OpenHubConnection(){
    if (this._hubConnection === undefined) {
      this._hubConnection = new HubConnectionBuilder().withUrl("http://localhost:44394/notify").build();//new HubConnection('http://localhost:44394/notify');//http://localhost:1874/notify
      this._hubConnection
        .start()
        .then(() => {
          console.log('Connection started!');
          var hub = this._hubConnection ;
           var connectionUrl = hub["connection"].transport.webSocket.url ;
           console.log(connectionUrl);
           const urlParams = new URLSearchParams(connectionUrl);
           this.strConnectionId = this.replaceAll(connectionUrl,"ws://localhost:44394/notify?id=","");//urlParams.get('id');//connectionUrl.snapshot.queryParamMap.get("id");
           console.log(this.strConnectionId);
          })
        .catch(err => console.log('Error while establishing connection :(' + err));

      this._hubConnection.on('BroadcastMessage', (type: string, payload: string) => {
        this.msgs = [];
        this.msgs.push({ severity: type, summary: payload });
        console.log(this.msgs);
        this.toastr.info(payload);
      });
    }

  }

  replaceAll = function (str, find, replace):string {
    //var str = this;
    return str.replace(new RegExp(find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), replace);
  };

  LoadpostInfo(){
    this.postInfo.mid = this.savedData.profileInfo.mid
    this.fappService.LoadTimeLineInfo(this.postInfo).subscribe(response => {
      if(response){
        const resp = response as PostResponse;
        if(response.key == "S" && response.status){
          if(response.postInfo == null){
            this.showAlert=true;
            return;
          }
          if(response.postInfo)
            var postIds = response.postInfo.postids.split(",");
            for (let i = 0; i < postIds.length; i++) {
              this.posts.push(postIds[i]);
            }
        }else{
          this.toastr.error(response.message);
        }
      }else{
        this.toastr.error("something gone wrong !");
      }
    });
  }

  LoadAdditionalpostInfo(){
    this.noofBottomClicks += 1;
    this.postInfo.postid = this.noofBottomClicks.toString();
    this.postInfo.mid = this.savedData.profileInfo.mid
    this.fappService.LoadTimeLineInfo(this.postInfo).subscribe(response => {
      if(response){
        const resp = response as PostResponse;
        this.fireAsycLoad = 'Y';
        if(response.key == "S" && response.status){
          if(response.postInfo == null){
            return;
          }
          if(response.postInfo)
            var postIds = response.postInfo.postids.split(",");
            for (let i = 0; i < postIds.length; i++) {
              this.posts.push(postIds[i]);
            }
        }else{
          this.toastr.error(response.message);
        }
      }else{
        this.toastr.error("something gone wrong !");
      }
    });
  }

  UpdateConnectionId(action : string){
      this.connectionInfo = new ConnectionInfo();
      this.connectionInfo.mid = this.savedData.profileInfo.mid;
      this.connectionInfo.connectionid = this.signalR.connectionId;
      this.connectionInfo.crtime = Date.now().toString();
      this.connectionInfo.action = action;

      this.fappService.UpdateConnectionId(this.connectionInfo).subscribe(response => {
        if(response){
          //this.toastr.success(response.message);
        }
      });
  }

  public handleScroll(event: NgxScrollEvent) {
    //console.log('scroll occurred', event.originalEvent);
    if (event.isReachingBottom) {
      //console.log(`the user is reaching the bottom`);
      if(this.fireAsycLoad == 'Y'){
        this.LoadAdditionalpostInfo();
        this.fireAsycLoad = 'N';
      }
    }
    if (event.isReachingTop) {
      //console.log(`the user is reaching the top`);
    }
    if (event.isWindowEvent) {
      //console.log(`This event is fired on Window not on an element.`);
    }
 
  }

}
