export class SignUpLang{
    public lang:any[];
    constructor(){        
        this.lang = new Array();
        var eng = new SignUpLangText();        
        eng.NameofChef = "Name of Chef";
        eng.EmailId = "Email id";
        eng.ConfirmEmailId = "Confirm Email id";
        eng.Password = "Password";
        eng.ConfirmPassword = "Confirm Password";
        eng.PhoneNumber = "Phone Number";
        eng.State = "State";
        eng.Region = "County";
        eng.Iaccept = "I accept the";
        eng.Terms = "Terms of Service.";
        eng.Submit = "Submit";
        eng.Firstname = "First Name";
        eng.Lastname ="Last Name";
        eng.Username ="Username";
        eng.firstnameisrequired = "firstname is required";
        eng.lastnameisrequired = "lastname is required";
        eng.emailidisrequired = "emailid is required";
        eng.enteravalidemailid = "enter a valid emailid";
        eng.passwordisrequired = "password is required";
        eng.validphonenumberisrequired = "valid phone number is required";

        var spn = new SignUpLangText();
        spn.NameofChef = "Nombre del chef";
        spn.EmailId = "Identificación de correo";
        spn.ConfirmEmailId = "Confirmar ID de correo electrónico";
        spn.Password = "Contraseña";
        spn.ConfirmPassword = "Confirmar contraseña";
        spn.PhoneNumber = "Número de teléfono";
        spn.State = "Estado";
        spn.Region = "Condado";
        spn.Iaccept = "acepto el";
        spn.Terms = "Términos de servicio.";
        spn.Submit = "Enviar";
        spn.Firstname ="Nombre de pila";
        spn.Lastname ="Apellido";
        spn.Username ="Nombre de usuario";
        spn.firstnameisrequired = "Se requiere el primer nombre";
        spn.lastnameisrequired = "el apellido es requerido";
        spn.emailidisrequired = "se requiere emailid";
        spn.enteravalidemailid = "ingresa un email válido";
        spn.passwordisrequired = "se requiere contraseña";
        spn.validphonenumberisrequired = "se requiere un número de teléfono válido";

        this.lang["english"] = eng;
        this.lang["spanish"] = spn;
    }
}

export class SignUpLangText{
    public NameofChef: string;
    public EmailId: string;
    public ConfirmEmailId: string;
    public Password: string;
    public ConfirmPassword: string;
    public PhoneNumber: string;
    public State: string;
    public Region: string;
    public Iaccept: string;
    public Terms:string;
    public Submit:string;
    public Firstname:string;
    public Lastname:string;
    public Username:string;

    public firstnameisrequired:string;
    public lastnameisrequired:string;
    public enteravalidemailid:string;
    public emailidisrequired:string;
    public passwordisrequired:string;
    public validphonenumberisrequired:string;
}