import { Component, OnInit } from '@angular/core';
import { SignUp } from '../model/signup.model';
import { Router } from '@angular/router';
import { FAppService } from '../service/fapp.service';
import { SharedService } from '../service/shared.service';
import { MehkLoginResponse } from '../model/MehkLoginResponse';
import {SignUpLang, SignUpLangText} from './signup.lang';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  public lang:SignUpLangText;
  signupData : SignUp;
  terms:boolean=false;
  isexistingUser:boolean=false;
  isdisabled:boolean=false;

  constructor(private route:Router, private fappService:FAppService, private sharedService:SharedService, private toastr: ToastrService) {
    this.sharedService.isShowHome.next(true);
    this.sharedService.Pagetitle.next("Signup");
    var savedData = JSON.parse(localStorage.getItem("UserInfo")) as MehkLoginResponse ;
    if(savedData != null){
      this.route.navigateByUrl("application");
    }
    if(localStorage.getItem("lang") == "spanish"){
      this.lang = new SignUpLang().lang["spanish"];
    }else{
      this.lang = new SignUpLang().lang["english"];
    }

    if(savedData != null && savedData.signUpUsers){
      this.signupData = savedData.signUpUsers;
      this.terms = true;
      this.isexistingUser = true;
    }else{
      this.signupData = new  SignUp();
    }  
    this.signupData.action =  this.isexistingUser ? "Y" : "N";this.signupData.result =  "result";

    this.signupData.state = "CA";
    this.signupData.region = "1";
  }

  ngOnInit() {
  }
  
  public stateChange(){
    //alert(this.selectedState);
  }

  public regionChange(){
    //alert(this.selectedRegion);
  }

  public ValidateForm(){//debugger;
    if(this.signupData.firstname == "" || this.signupData.firstname === undefined){
      this.toastr.error('Firstname is required!');
      return false;
    }
    else if(this.signupData.lastname == "" || this.signupData.lastname === undefined){
      this.toastr.error('Last is required!');
      return false;
    }
    else if(this.signupData.emailid == "" || this.signupData.emailid === undefined){
      this.toastr.error('EmailId is required!');
      return false;
    }
    else if(!this.isEmail(this.signupData.emailid)){
      this.toastr.error('Enter Valid EmailId!');
      return false;
    }
    else if(this.signupData.confirmemailid == "" || this.signupData.confirmemailid === undefined){
      this.toastr.error('Confirm EmailId is required!');
      return false;
    }
    else if(!this.isEmail(this.signupData.confirmemailid)){
      this.toastr.error('Enter Valid Confirm EmailId!');
      return false;
    }
    else if(this.signupData.emailid != this.signupData.confirmemailid){
      this.toastr.error('Email Id & Confirm Email Id does not match!');
      return false;
    }
    else if(!this.isexistingUser && (this.signupData.pwd == "" || this.signupData.pwd === undefined)){
      this.toastr.error('Password is required!');
      return false;
    }
    else if(!this.isexistingUser && (this.signupData.confirmpwd == "" || this.signupData.confirmpwd === undefined)){
      this.toastr.error('Confirm Password is required!');
      return false;
    }
    else if(!this.isexistingUser && (this.signupData.pwd != this.signupData.confirmpwd)){
      this.toastr.error('Password & Confirm Password are not matching!');
      return false;
    }
    else if(this.signupData.phone_number == "" || this.signupData.phone_number === undefined){
      this.toastr.error('Phone number is required!');
      return false;
    }
    else if(this.signupData.phone_number.length != 10){
      this.toastr.error('10 digit phone number is required!');
      return false;
    }
    else if(this.signupData.state === undefined || this.signupData.state == "0"){
      this.toastr.error('State is required!');
      return false;
    }
    else if(this.signupData.region === undefined || this.signupData.region == "0"){
      this.toastr.error('Region is required!');
      return false;
    }
    return true;
  }
  public SubmitSignUp(){
    if(!this.ValidateForm()){
      return;
    }
    // this.mehkService.SignUpFApp(this.signupData).subscribe(response => {
    //   if(response){
    //     if(response.status=="Success"){   
    //       if(!this.isexistingUser)     {
    //         this.route.navigateByUrl("login");
    //       } 
    //       this.toastr.success(response.message); 
    //     }else{
    //       this.toastr.error(response.message); 
    //     }
    //   }else{
    //     this.toastr.error("something gone wrong !");
    //   }
    // });
  }

  isEmail(search:string):boolean
    {
        var  searchfind:boolean;

        var regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

        searchfind = regexp.test(search);

        console.log(searchfind)
        return searchfind
    }
}
