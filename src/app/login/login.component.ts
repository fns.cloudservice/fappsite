import { Component, OnInit, HostBinding } from '@angular/core';
import {FAppService} from '../service/fapp.service';
import {LoginMehkRequest} from '../model/LoginMehkRequest';
import {MehkLoginResponse} from '../model/MehkLoginResponse';
import {SharedService} from '../service/shared.service';
import { Router } from '@angular/router';
import {LoginInfoLang, LoginInfoLangText} from './login.lang';
import { ToastrService } from 'ngx-toastr';
import {AuthService} from '../service/auth.service';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { loginInfo } from '../model/login.model';
import { ProfileResponse } from '../model/ProfileResponse';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { interval, timer } from 'rxjs';
import { map, take, delay, withLatestFrom, finalize, tap } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public lang:LoginInfoLangText;
  savedData:ProfileResponse;
  authState: any = null;
  angularFireAuth: AngularFireAuthModule;
  loginInfo : loginInfo;
  userdefaultImgPath:string="../assets/layouts/layout4/img/usr.jpg";

  public Login :LoginMehkRequest;
  constructor(private fappService:FAppService, private sharedService:SharedService, private router:Router, private toastr: ToastrService, private auth: AuthService, 
    public loader: LoadingBarService) {
    this.sharedService.loggedIn.next(false);
    this.sharedService.Pagetitle.next("Signin");
    this.Login = new LoginMehkRequest();
    //this.angularFireAuth.authState.subscribe(this.firebaseAuthChangeListener);

  }

  ngOnInit() {
    //this.loader.start();
  }

  ngAfterViewInit() {
    //this.endloader();
  }

  private endloader(){
    this.loader.complete();
    this.loader.stop();
  }

  ngAfterContentChecked() {
    if(this.auth.currentUser){
      this.loginInfo = new loginInfo();
      this.loginInfo.uid = this.auth.currentUserId;
      this.loginInfo.email = this.auth.currentEmailId;
      this.loginInfo.displayName = this.auth.currentDisplayName;

      this.fappService.verifyUserInfo(this.loginInfo).subscribe(response => {
        if(response){
          const resp = response as ProfileResponse;
          if(response.key == "S" && response.status){
            localStorage.setItem("profileInfo", JSON.stringify(resp));
            this.savedData = JSON.parse(localStorage.getItem("profileInfo")) as ProfileResponse;
            this.SetProfileImg();
            this.sharedService.loggedIn.next(true);
            this.sharedService.savedDataResponse.next(JSON.stringify(resp))
            //this.toastr.success(response.message, 'FAPP WebSite');
          }else{
            this.toastr.error(response.message);
          }
        }else{
          this.toastr.error("something gone wrong !");
        }
      });
        this.router.navigateByUrl("home");
    }
    else{
      //this.sharedService.loggedIn.next(false);
    }
  }

  public SetProfileImg(){
    if(this.savedData.profileInfo == null)
    return;

    if(this.savedData.profileInfo.filepath != null && this.savedData.profileInfo.filepath != ""){
      this.sharedService.userImgPath.next(this.savedData.profileInfo.filepath);
    }
    else if(this.auth.currentUser){
      this.sharedService.userImgPath.next(this.auth.currentUser.photoURL);
    }
    else{
      this.sharedService.userImgPath.next(this.userdefaultImgPath);
    }
  }

  public MehkNavToForgotPwd(){
    this.router.navigateByUrl("forgotpwd");
  }

  public MehkLogin(){

  }





}
