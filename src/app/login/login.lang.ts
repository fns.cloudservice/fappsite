export class LoginInfoLang{
    public lang:any[];
    constructor(){        
        this.lang = new Array();
        var eng = new LoginInfoLangText();        
        eng.EmailId = "Email Id";
        eng.Password = "Password";
        eng.Username ="Username";
        eng.Submit = "Submit";
        eng.ForgotUsernamePassword = "Forgot Username / Password";
        eng.Clickhere = "Click here";
        eng.emailidisrequired = "emailid is required";
        eng.enteravalidemailid = "enter a valid emailid";
        eng.pwdisrequired = "Password is required";

        var spn = new LoginInfoLangText();
        spn.EmailId = "Identificación de correo";
        spn.Password = "Contraseña";
        spn.Username ="Nombre de usuario";
        spn.Submit = "Enviar";
        spn.ForgotUsernamePassword = "Olvidó su nombre de usuario / contraseña";
        spn.Clickhere = "Haga clic aquí.";
        spn.emailidisrequired = "se requiere emailid";
        spn.enteravalidemailid = "ingresa un email válido";
        spn.pwdisrequired = "se requiere contraseña";

        this.lang["english"] = eng;
        this.lang["spanish"] = spn;
    }
}

export class LoginInfoLangText{
    public EmailId: string;
    public Password: string;
    public Username: string;
    public Submit: string;
    public ForgotUsernamePassword: string;
    public Clickhere: string;
    public enteravalidemailid:string;
    public emailidisrequired:string;
    public pwdisrequired:string;
}