export class Application
{
    public mehk_user_id:number;
    public applfor: string;
    public firstname: string;
    public middlename: string;
    public lastname: string;
    public dlnumber: string;
    public dldate: string;
    //public dob: Date;
    public dob: string;
    public dbafiled: string;
    public dbaname: string;
    public streetAddress: string;
    public addressLine2: string;
    public city: string;
    public State: string;
    public Region: string;
    public zipCode: string;
    public bStreetAddress: string;
    public bAddressLine2: string;
    public bCity: string;
    public bState: string;
    public bRegion: string;
    public bZIPCode: string;
    public businessPhone: string;
    public homePhone: string;
    public webSite: string;
    public expectedOpenDate: string;

    public emailid: string;
    public action: string; 
    public result: string;   
}