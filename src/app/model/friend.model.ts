export class friendInfo
{
    public mid:string;
    public mids:string;
    public imgPath:string;
    public displayName:string;
    public location:string;
    public email:string;
    public rmid:string;
    public since:string;
    public action:string;
    public search:string;
}