import { followersInfo } from './Followers.model'

export class FollowersResponse{
    public followersInfo:followersInfo;
    public message :string;
    public key :string;

}