export class SignUp
{
    public mehk_user_id:Number;
    public firstname: string;
    public lastname: string;
    public emailid: string;
    public confirmemailid: string;
    public pwd: string;
    public confirmpwd: string;
    public phone_number: string;
    public state: string;
    public region: string;
    public action: string;
    public result: string;
    public token: string;
}