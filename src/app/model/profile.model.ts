export class profileInfo
{
    public uid:string;
    public mid:string;
    public profileid:string;
    public firstname: string;
    public lastname: string;
    public gender: string;
    public language: string;
    public location: string;
    public emailid: string;
    public aboutme: string;
    public myinterest: string;
    public filepath: string;
    public file_name: string;
    public file: string;
}