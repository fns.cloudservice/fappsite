import {postInfo} from './post.model'

export class NotifyResponse{
    public notifyInfo : any;
    public status :boolean;
    public message :string;
    public key :string;
}