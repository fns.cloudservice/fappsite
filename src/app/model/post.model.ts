export class postInfo
{
    public mid:string;
    public postids:string;
    public posttime:string;
    public postid:string;
    public isdeleted:string;
    public description: string;
    public ispublic: string;
    public likes: string;
    public MediaJson: string;
    public connectionid:string;
}
export class ConnectionInfo
{
    public mid:string;
    public connectionid:string;
    public crtime:string;
    public action:string;
}
export class headerInfo
{
    public mid:string;
}
export class chatEntryInfo
{
    public fmid:string;
    public tmid:string;
    public msg:string;
    public msgtime:string;
    public dsnm:string;
    public imgpath:string;
    public flag:string;
}