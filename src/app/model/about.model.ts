export class aboutInfo
{
    public mehk_user_id:number;
    public firstname: string;
    public lastname: string;
    public gender: string;
    public mobileno: string;
    public emailid: string;
    public aboutme: string;
    public myinterest: string;
    public password: string;
}