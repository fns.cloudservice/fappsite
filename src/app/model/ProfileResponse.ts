import {profileInfo} from './profile.model'

export class ProfileResponse{
    public profileInfo : profileInfo;
    public status :boolean;
    public message :string;
    public key :string;
}