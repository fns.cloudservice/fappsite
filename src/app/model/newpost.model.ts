export class newpostInfo
{
    public postid:string;
    public memberid:string;
    public posttime:string;
    public isdeleted:string;
    public description: string;
    public seqn: string;
    public ispublic: string;
    public likes: string;
    public filepath: string;
    public file_name: string;
    public img_desc: string;
    public img_content: any;
    public file: File;
    public postjson: any;
    public mediajson: any;
}