import {postInfo} from './post.model'

export class PostResponse{
    public postInfo : postInfo;
    public status :boolean;
    public message :string;
    public key :string;
}