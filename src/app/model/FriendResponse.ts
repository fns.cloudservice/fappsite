import { friendInfo } from './friend.model'

export class FriendResponse{
    public friendInfo : friendInfo;
    public status :boolean;
    public message :string;
    public key :string;
}