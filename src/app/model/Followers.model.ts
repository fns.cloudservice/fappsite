export class followersInfo{

    public displayName:string;
    public location:string;
    public email:string;
    public imgPath:string;
    public since:string;
}