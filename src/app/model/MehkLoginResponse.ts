import {aboutInfo} from './about.model';
import {profileInfo} from './profile.model';
import { Application } from './application.model';
import {SignUp} from './signup.model'

export class MehkLoginResponse{
    public aboutInfo : aboutInfo;
    public profileInfo : profileInfo;

    public signUpUsers : SignUp;
    public applicationInfo : Application;
    public status :boolean;
    public message :string;
}