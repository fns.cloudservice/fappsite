import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {HomeLang,HomeLangText} from './home.lang';
import {SharedService} from '../service/shared.service';
import {AuthService} from '../service/auth.service';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { ProfileResponse } from '../model/ProfileResponse';
import { profileInfo } from '../model/profile.model';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { Message } from 'primeng/api';
import { Observable} from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { interval, timer } from 'rxjs';
import { map, take, delay, withLatestFrom, finalize, tap } from 'rxjs/operators';

declare function fnLoadJs():any;

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  profileInfoData : profileInfo; savedData:ProfileResponse; public lang:HomeLangText; strConnectionId : string;
  private _hubConnection: HubConnection; msgs: Message[] = [];

  constructor(private router:Router, private sharedService: SharedService, private auth: AuthService, private toastr: ToastrService, public loader: LoadingBarService) {
    this.sharedService.isShowHome.next(true);
    this.sharedService.loggedIn.next(true);

    this.savedData = JSON.parse(localStorage.getItem("profileInfo")) as ProfileResponse;
    if(this.savedData && this.savedData.profileInfo){
      this.profileInfoData = this.savedData.profileInfo;
    }else{
      this.profileInfoData = new  profileInfo();
    }
    this.DisplayUserInfo();
   }

  ngOnInit() {
    //this.LoadAllJsFiles();
    this.auth.isuserLoggedIn();
    this.loader.start();
  }

  ngAfterViewInit() {
    this.endloader();
  }

  private endloader(){
    this.loader.complete();
    this.loader.stop();
  }

  ngAfterContentChecked() {
    
  }

  public DisplayUserInfo(){
      if(this.auth.currentUser){
          this.sharedService.CurrentUser.next(this.auth.currentUser.displayName);
      }
  }

  public logout():void {
    console.log('Your logout message here');
   }

  public loadScript(url) {
    //console.log('preparing to load...')
    let node = document.createElement('script');
    node.src = url;
    node.type = 'text/javascript';
    document.getElementsByTagName('head')[0].appendChild(node);
 }

  replaceAll = function (str, find, replace):string {
    //var str = this;
    return str.replace(new RegExp(find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), replace);
  };

  private LoadAllJsFiles(){
    this.loadScript('../../assets/global/plugins/jquery.min.js');
    this.loadScript('../assets/global/plugins/bootstrap/js/bootstrap.min.js');
    this.loadScript('../assets/global/plugins/js.cookie.min.js');
    this.loadScript('../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js');
    this.loadScript('../assets/global/plugins/jquery.blockui.min.js');
    this.loadScript('../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
    this.loadScript('../assets/global/plugins/jquery-ui/jquery-ui.min.js');
    this.loadScript('../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js');
    this.loadScript('../assets/global/plugins/jquery.sparkline.min.js');
    this.loadScript('../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');
    this.loadScript('../assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js');
    this.loadScript('../assets/global/plugins/select2/js/select2.full.min.js');
    this.loadScript('../assets/global/plugins/bootstrap-toastr/toastr.min.js');
    this.loadScript('../assets/global/scripts/app.min.js');
    this.loadScript('../assets/pages/scripts/ui-toastr.min.js');
    this.loadScript('../assets/pages/scripts/profile.min.js');
    this.loadScript('../assets/apps/scripts/todo-2.min.js');
    this.loadScript('../assets/pages/scripts/components-date-time-pickers.min.js');
    this.loadScript('../assets/pages/scripts/portfolio-1.min.js');
    this.loadScript('../assets/pages/scripts/ui-modals.min.js');
    this.loadScript('../assets/layouts/layout4/scripts/layout.min.js');
    this.loadScript('../assets/layouts/layout4/scripts/demo.min.js');
    this.loadScript('../assets/layouts/global/scripts/quick-sidebar.min.js');
    this.loadScript('../assets/layouts/global/scripts/quick-nav.min.js');
    //this.loadScript('../assets/pages/scripts/fapp.js');
 }

}
