export class HomeLang{
    public lang:any[];
    constructor(){        
        this.lang = new Array();
        var eng = new HomeLangText();        
        eng.BannerContent = "Use Your Culinary Skills to Become An Entrepreneur";
        eng.StartHomeKitchenPara1 = "In order to start your own home kitchen, DPHC has mandated that you procure a MEKH registration certification.";
        eng.StartHomeKitchenPara2 = "This website provides a simplified step by step process to prepare the registration forms and related documents to submit a request for MEHK registration to DPHC. DPHC will perform a kitchen inspection and this website provides the easy guidance to get prepared for the inspection as well.";
        eng.StartHomeKitchenPara3 = "To get the step by step application process started, please click below button.";
        eng.GetStarted = "Get Started";
        eng.Introductionh2 = "Introduction to MEHK";
        eng.IntroductionPara = "“Microenterprise home kitchen operation” is a food facility that is operated by a resident in a private home where food is stored, handled, and prepared for and may be served to consumers. The department of public health of california has set some guidelines to establish a MEHK business. Some of the basic guidelines are as follows:";
        eng.Question1 = "What can you Sell ?";
        eng.Answer1 = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry. Lorem ipsum dolor";
        eng.Question2 = "Where can you Sell ?";
        eng.Answer2 = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry. Lorem ipsum dolor";
        eng.Question3 = "How can you Sell ?";
        eng.Answer3 = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry. Lorem ipsum dolor";
        eng.Question4 = "What is your earning potential ?";
        eng.Answer4 = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry. Lorem ipsum dolor";
        eng.HowItWorks ="How it works";
        eng.Login ="Login / Signup";
        eng.EnterDetails ="Enter the details";
        eng.ReviewPrint ="Review & Print";
        eng.Submit ="Sign & Submit";

        var spn = new HomeLangText();
        spn.BannerContent = "Usa tus habilidades culinarias para convertirte en un empresario";
        spn.StartHomeKitchenPara1 = "Para comenzar su propia cocina en casa, DPHC le ha ordenado que obtenga una certificación de registro MEKH.";
        spn.StartHomeKitchenPara2 = "Este sitio web proporciona un proceso simplificado paso a paso para preparar los formularios de registro y los documentos relacionados para enviar una solicitud de registro de MEHK a DPHC. DPHC realizará una inspección de cocina y este sitio web proporciona una guía fácil para prepararse para la inspección también.";
        spn.StartHomeKitchenPara3 = "Para iniciar el proceso de solicitud paso a paso, haga clic en el botón de abajo.";
        spn.GetStarted = "Empezar";
        spn.Introductionh2 = "Introducción a MEHK";
        spn.IntroductionPara = "La “operación de la cocina de la microempresa” es una instalación de alimentos que es operada por un residente en un hogar privado donde los alimentos se almacenan, se manejan y se preparan para los consumidores. El departamento de salud pública de California ha establecido algunas pautas para establecer un negocio MEHK. Algunas de las pautas básicas son las siguientes:";
        spn.Question1 = "¿Qué puedes vender?";
        spn.Answer1 = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry. Lorem ipsum dolor";
        spn.Question2 = "¿Dónde puedes vender?";
        spn.Answer2 = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry. Lorem ipsum dolor";
        spn.Question3 = "¿Cómo se puede vender?";
        spn.Answer3 = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry. Lorem ipsum dolor";
        spn.Question4 = "What is your earning potential ?";
        spn.Answer4 = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry. Lorem ipsum dolor";
        spn.HowItWorks = "Cómo funciona";
        spn.Login ="Iniciar sesión Registrarse";
        spn.EnterDetails ="Ingrese los detalles";
        spn.ReviewPrint ="Revisión e impresión";
        spn.Submit ="Firmar y enviar";

        this.lang["english"] = eng;
        this.lang["spanish"] = spn;
    }
}

export class HomeLangText{
    public BannerContent: string;
    public StartHomeKitchenPara1: string;
    public StartHomeKitchenPara2: string;
    public StartHomeKitchenPara3: string;
    public GetStarted: string;
    public Introductionh2:string;
    public IntroductionPara:string;
    public Question1:string;
    public Answer1:string;
    public Question2:string;
    public Answer2:string;
    public Question3:string;
    public Answer3:string;
    public Question4:string;
    public Answer4:string;
    public HowItWorks:string;
    public Login:string;
    public EnterDetails:string;
    public ReviewPrint:string;
    public Submit:string;
}