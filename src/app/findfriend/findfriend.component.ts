import { MehkLoginResponse } from '../model/MehkLoginResponse';
import { Component, OnInit, ElementRef, ViewChild, HostListener } from '@angular/core';
import {AlertsComponent} from "../controls/alerts/alerts.component"; 
import { Router, NavigationEnd } from '@angular/router';
import { FAppService } from '../service/fapp.service';
import { SharedService } from '../service/shared.service';
import { ConfirmService } from '../service/confirm.service';
import { ProfileResponse } from '../model/ProfileResponse';
import { profileInfo } from '../model/profile.model';
import { FriendResponse } from '../model/FriendResponse';
import { friendInfo } from '../model/friend.model';
import { ToastrService } from 'ngx-toastr';
import {AuthService} from '../service/auth.service';
import { finalize, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import * as _ from 'lodash';

@Component({
  selector: 'app-findfriend',
  templateUrl: './findfriend.component.html',
  styleUrls: ['./findfriend.component.scss']
})
export class FindfriendComponent implements OnInit {
  Friends: string[] = []; friendInfo : friendInfo; savedData:ProfileResponse; profileInfoData : profileInfo; FriendsCount:any;
  FriendRequests: string[] = []; FriendrequestCount:any; PopOverMId: string; IsOpened:string;

  constructor(private fappService:FAppService, private sharedService:SharedService, private router:Router, private toastr: ToastrService, 
    private auth: AuthService, private confirmService: ConfirmService) {
      this.savedData = JSON.parse(localStorage.getItem("profileInfo")) as ProfileResponse;
      this.friendInfo = new friendInfo();
    
   }

 ngOnInit() {
  this.auth.isuserLoggedIn();
 }
 
 ngAfterContentChecked() {
   
 }

 OnGeneralSearchFriends(){
    this.LoadGeneralSearchList(); 
  }

  LoadGeneralSearchList(){
    this.Friends = [];
    this.friendInfo.mid = this.savedData.profileInfo.mid;
    if(this.friendInfo.search == null){
      this.friendInfo.search = "";
    }
    this.fappService.LoadGeneralSearchList(this.friendInfo).subscribe(response => {
      if(response){
        const resp = response as FriendResponse;
        if(response.key == "S" && response.status){ 
          if(response.friendInfo)
            var mIds = response.friendInfo.mids.split(",");
            if(mIds === undefined || mIds == null){
              this.FriendsCount = 0;
              return;
            }
            this.FriendsCount = mIds.length;
            for (let i = 0; i < mIds.length; i++) {
              this.Friends.push(mIds[i]);
            }
        }else{
          this.toastr.error(response.message);
        }
      }else{
        this.toastr.error("something gone wrong !");
      }
    });
  }

  OnManageRequest(mid : string, rmid : string, displayName : string, action : string){
    var title = 'Confirm friend request';
    var msg = 'Do you really want to send friend request to ' + displayName;
    
    this.confirmService.confirm({ title:title, message: msg + '?' }).then(
      () => {
        this.friendInfo.mid = mid; this.friendInfo.rmid = rmid; 
        this.friendInfo.action = action; this.friendInfo.since = Date.now().toString();
        this.fappService.ManageRequestInfo(this.friendInfo).subscribe(response => {
          if(response){
            const resp = response as FriendResponse;
            if(response.key == "S"){ 
              this.toastr.success(response.message);
              this.LoadGeneralSearchList();
            }else{
              this.toastr.error(response.message);
            }
          }else{
            this.toastr.error("something gone wrong !");
          }
        });
      },
      () => {
        //console.log('not deleting...');
      });
    
  }


}
