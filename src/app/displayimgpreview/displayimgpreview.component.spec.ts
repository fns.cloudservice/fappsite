import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayimgpreviewComponent } from './displayimgpreview.component';

describe('DisplayimgpreviewComponent', () => {
  let component: DisplayimgpreviewComponent;
  let fixture: ComponentFixture<DisplayimgpreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayimgpreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayimgpreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
