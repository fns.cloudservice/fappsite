import { Component, OnInit, Input, ChangeDetectorRef, ElementRef, ViewChild, TemplateRef  } from '@angular/core';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AngularFirestore } from '@angular/fire/firestore';
import {SharedService} from '../service/shared.service';
import { finalize, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'displayimgpreview',
  templateUrl: './displayimgpreview.component.html',
  styleUrls: ['./displayimgpreview.component.scss']
})
export class DisplayimgpreviewComponent implements OnInit {
  @Input() file: File;

  ImgDesc:string;
  imgURL: any; fSize ; number;
  public imagePath;
  public errormsg: string;
  public fName: string;
  modalRef: BsModalRef;
  config = { backdrop: true, ignoreBackdropClick: false };

  constructor(private modalService: BsModalService,private storage: AngularFireStorage, private db: AngularFirestore, private sharedService: SharedService) { 

  }

  ngOnInit() {
    this.startDisplay();
  }

  startDisplay() {

    var mimeType = this.file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.errormsg = "Only images are supported.";
      //return;
    }

    var reader = new FileReader();
    this.imagePath = this.file;
    this.fSize = this.file.size;
    this.fName = this.file.name;
    reader.readAsDataURL(this.file); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }

  }

  OnClick(){
    //setTimeout(()=> { fnRemoveAlbumdiv(); } , 750);
  }

}
