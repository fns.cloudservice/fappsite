import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import {AlertsComponent} from "../controls/alerts/alerts.component"; 
import { Router, NavigationEnd } from '@angular/router';
import { FAppService } from '../service/fapp.service';
import { SharedService } from '../service/shared.service';
import { ConfirmService } from '../service/confirm.service';
import { ProfileResponse } from '../model/ProfileResponse';
import { profileInfo } from '../model/profile.model';
import { PostResponse } from '../model/PostResponse';
import { postInfo } from '../model/post.model';
import { ToastrService } from 'ngx-toastr';
import {AuthService} from '../service/auth.service';
import { finalize, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import * as _ from 'lodash';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})
export class HelpComponent implements OnInit {

  constructor(private fappService:FAppService, private sharedService:SharedService, private router:Router, private toastr: ToastrService, 
    private auth: AuthService, private confirmService: ConfirmService) { }

  ngOnInit() {
    this.auth.isuserLoggedIn();
  }

  ngAfterContentChecked() {
    
  }

}
