import { Component, NgModule, Injectable, EventEmitter, AfterViewInit } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class SharedService {
    LoginEvent = new EventEmitter<any>();
    FileEvent = new EventEmitter<any>();
    public isShowHome: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public CurrentUser: BehaviorSubject<string> = new BehaviorSubject<string>("");
    public loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public userImgPath: BehaviorSubject<string> = new BehaviorSubject<string>("");
    public Pagetitle: BehaviorSubject<string> = new BehaviorSubject<string>("");
    public Img_desc: BehaviorSubject<string> = new BehaviorSubject<string>("");
    public langSwitchText: BehaviorSubject<string> = new BehaviorSubject<string>("English (US)");
    public popoverMId: BehaviorSubject<string> = new BehaviorSubject<string>("");
    public notifyCount: BehaviorSubject<string> = new BehaviorSubject<string>("0");
    public vPostId: BehaviorSubject<string> = new BehaviorSubject<string>("");
    public isReloadViewAll: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public dynamicChatInfo: BehaviorSubject<string> = new BehaviorSubject<string>("");
    public currentChatUserStatus: BehaviorSubject<string> = new BehaviorSubject<string>("");
    public updateindividualchatStatus: BehaviorSubject<string> = new BehaviorSubject<string>("");
    public receiptStatus: BehaviorSubject<string> = new BehaviorSubject<string>("");
    public msgCount: BehaviorSubject<string> = new BehaviorSubject<string>("");
    public cummsgCount: BehaviorSubject<any> = new BehaviorSubject<any>(0);
    public cmtMessage: BehaviorSubject<string> = new BehaviorSubject<string>("");
    public Likes: BehaviorSubject<string> = new BehaviorSubject<string>("0");
    public ImgLikes: BehaviorSubject<string> = new BehaviorSubject<string>("0");
    public FollowUnFollow: BehaviorSubject<string> = new BehaviorSubject<string>("");
    public inSearchUrl : BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public hubConnection: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public savedDataResponse: BehaviorSubject<string> = new BehaviorSubject<string>("");
} 
