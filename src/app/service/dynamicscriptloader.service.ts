import { Injectable } from '@angular/core';

interface Scripts {
  name: string;
  src: string;
}

export const ScriptStore: Scripts[] = [
  { name: 'jquery', src: '../assets/global/plugins/jquery.min.js' },
  { name: 'bootstrap', src: '../assets/global/plugins/bootstrap/js/bootstrap.min.js' },
  { name: 'js.cookie', src: '../assets/global/plugins/js.cookie.min.js' },
  { name: 'slimscroll', src: '../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' },
  { name: 'jquery.blockui', src: '../assets/global/plugins/jquery.blockui.min.js' },
  { name: 'bootstrap-switch', src: '../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' },
  { name: 'bootstrap-fileinput', src: '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js' },
  { name: 'jquery.sparkline', src: '../assets/global/plugins/jquery.sparkline.min.js' },
  { name: 'bootstrap-datepicker', src: '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js' },
  { name: 'cubeportfolio', src: '../assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' },
  { name: 'app.min', src: '../assets/global/scripts/app.min.js' },
  { name: 'profile', src: '../assets/pages/scripts/profile.min.js' },
  { name: 'components-date-time-pickers', src: '../assets/pages/scripts/components-date-time-pickers.min.js' },
  { name: 'portfolio1', src: '../assets/pages/scripts/portfolio-1.min.js' },
  { name: 'ui-modals', src: '../assets/pages/scripts/ui-modals.min.js' },
  { name: 'layout', src: '../assets/layouts/layout4/scripts/layout.min.js' },
  { name: 'demo', src: '../assets/layouts/layout4/scripts/demo.min.js' },
  { name: 'quick-sidebar', src: '../assets/layouts/global/scripts/quick-sidebar.min.js' },
  { name: 'quick-nav', src: '../assets/layouts/global/scripts/quick-nav.min.js' }
];

declare var document: any;

@Injectable()
export class DynamicScriptLoaderService {

  private scripts: any = {};

  constructor() {
    ScriptStore.forEach((script: any) => {
      this.scripts[script.name] = {
        loaded: false,
        src: script.src
      };
    });
  }

  load(...scripts: string[]) {
    const promises: any[] = [];
    scripts.forEach((script) => promises.push(this.loadScript(script)));
    return Promise.all(promises);
  }

//   loadScript(name: string) {debugger;
//     return new Promise((resolve, reject) => {
//       if (!this.scripts[name].loaded) {
//         //load script
//         let script = document.createElement('script');
//         script.type = 'text/javascript';
//         script.src = this.scripts[name].src;
//         if (script.readyState) {  //IE
//             script.onreadystatechange = () => {
//                 if (script.readyState === "loaded" || script.readyState === "complete") {
//                     script.onreadystatechange = null;
//                     this.scripts[name].loaded = true;
//                     resolve({script: name, loaded: true, status: 'Loaded'});
//                 }
//             };
//         } else {  //Others
//             script.onload = () => {
//                 this.scripts[name].loaded = true;
//                 resolve({script: name, loaded: true, status: 'Loaded'});
//             };
//         }
//         script.onerror = (error: any) => resolve({script: name, loaded: false, status: 'Loaded'});
//         document.getElementsByTagName('head')[0].appendChild(script);
//       } else {
//         resolve({ script: name, loaded: true, status: 'Already Loaded' });
//       }
//     });
//   }

loadScript(name: string) {
    if (!this.scripts[name].loaded) {
        //load script
        let script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = this.scripts[name].src;
        if (script.readyState) {  //IE
            script.onreadystatechange = () => {
                if (script.readyState === "loaded" || script.readyState === "complete") {
                    script.onreadystatechange = null;
                    this.scripts[name].loaded = true;
                    //resolve({script: name, loaded: true, status: 'Loaded'});
                }
            };
        } else {  //Others
            script.onload = () => {
                this.scripts[name].loaded = true;
                //resolve({script: name, loaded: true, status: 'Loaded'});
            };
        }
        //script.onerror = (error: any) => resolve({script: name, loaded: false, status: 'Loaded'});
        document.getElementsByTagName('head')[0].appendChild(script);
      } else {
        //resolve({ script: name, loaded: true, status: 'Already Loaded' });
      }
  }


}