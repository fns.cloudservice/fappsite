import { Injectable, EventEmitter } from '@angular/core';
import { Message } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject } from 'rxjs';
import { ProfileResponse } from '../model/ProfileResponse';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { postInfo, ConnectionInfo } from '../model/post.model';
import { FAppService } from '../service/fapp.service';
import {SharedService} from '../service/shared.service';

@Injectable({
  providedIn: 'root'
})
export class SignalRService {
    public connectionExists: Boolean; msgs: Message[] = []; connectionId : string; savedData:ProfileResponse; cnotifyCount : any;
    public connectionEstablished: EventEmitter <Boolean> ; connectionInfo : ConnectionInfo; currentdynamicmsg : any
    public connectionIdReceived: EventEmitter<any>;
    public messageReceived: EventEmitter<any>;

    private hubConnection: HubConnection

    constructor(private fappService:FAppService, private toastr: ToastrService, private sharedService: SharedService) {
        //debugger;
        this.savedData = JSON.parse(localStorage.getItem("profileInfo")) as ProfileResponse;
        // Constructor initialization
        this.connectionEstablished = new EventEmitter<Boolean>();
        this.connectionIdReceived = new EventEmitter<any>();
        this.messageReceived = new EventEmitter<any>();
        this.connectionExists = false;
        this.sharedService.savedDataResponse.subscribe(value=>{
          if(value !=""){
            localStorage.setItem("ProfileData",value)
            this.savedData=JSON.parse(localStorage.getItem("ProfileData"))
            this.startConnection();
            this.addTransferFeedDataListener();
          }
        })
        //this.startConnection();
    }

  public startConnection = () => {
    //console.log(" From Start Connection",localStorage.getItem("ProfileData"))
    this.hubConnection = new HubConnectionBuilder()
                            .withUrl('http://localhost:44394/notify')
                            .build();

    this.hubConnection
      .start()
      .then(() => {
          console.log('Connection started!');
          this.connectionEstablished.emit(true);
          this.connectionExists = true;
          this.hubConnection.serverTimeoutInMilliseconds = 100000; // 100 second

          var hub = this.hubConnection ;
          var connectionUrl = hub["connection"].transport.webSocket.url ;
          this.connectionId = this.replaceAll(connectionUrl,"ws://localhost:44394/notify?id=","");
           console.log(this.connectionId);
           this.UpdateConnectionId('I');
           //this.connectionIdReceived.emit(this.connectionId);
    })
      .catch(err => {
         // console.log(" From error",localStorage.getItem("ProfileData"))
          console.log('Error while starting connection: ' + err);
          this.UpdateConnectionId('R');
          setTimeout(() => { if(this.sharedService.loggedIn){ this.hubConnection.start(); } }, 1000);
          this.connectionExists = false;
          this.connectionEstablished.emit(false);
    })
  }

  public addTransferFeedDataListener = () => {
    this.hubConnection.on('BroadcastMessage', (type: string, payload: string) => {
      if(type == 'cmtsuccess'){
        this.sharedService.cmtMessage.next(payload);
        return;
      }
      if(type == 'likesuccess'){
        this.sharedService.Likes.next(payload);
        return;
      }
      if(type == 'imglikesuccess'){
        this.sharedService.ImgLikes.next(payload);
        return;
      }
      if(type == 'icustatus'){
        this.sharedService.updateindividualchatStatus.next(payload);
        return;
      }
      if(type == 'receipt'){
        this.sharedService.receiptStatus.next(payload);
        return;
      }
      if(type == 'msgCount'){
        this.sharedService.msgCount.next(payload);
        return;
      }
      if(payload == '' || payload.indexOf('...') > -1){
        this.sharedService.currentChatUserStatus.next(payload);
      }
      else if(payload.indexOf('#') > -1){
        this.currentdynamicmsg = payload;
        this.sharedService.dynamicChatInfo.next(type);
      }else{
        this.msgs = [];
        this.msgs.push({ severity: type, summary: payload });
        console.log(this.msgs);
        //this.messageReceived.emit(this.msgs);
        this.cnotifyCount = localStorage.getItem("notifyCount") === undefined ? 0 : localStorage.getItem("notifyCount");
        this.cnotifyCount = parseInt(this.cnotifyCount) + 1;
        //this.toastr.info(payload);
        this.sharedService.notifyCount.next(this.cnotifyCount);
        this.sharedService.isReloadViewAll.next(true);
      }
      });
  }

  replaceAll = function (str, find, replace):string {
    //var str = this;
    return str.replace(new RegExp(find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), replace);
  };

  UpdateConnectionId(action : string){
    //console.log("update Connection id signlar", localStorage.getItem("ProfileData"))
    if(this.savedData != null){
      this.connectionInfo = new ConnectionInfo();
      this.connectionInfo.mid = this.savedData.profileInfo.mid;
      this.connectionInfo.connectionid = this.connectionId;
      this.connectionInfo.crtime = Date.now().toString();
      this.connectionInfo.action = action;
  
      this.fappService.UpdateConnectionId(this.connectionInfo).subscribe(response => {
        if(response){
          //this.toastr.success(response.message);
        }
      });
    }
}

}
