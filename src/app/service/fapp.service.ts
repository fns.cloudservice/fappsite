import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import config from '../config';
import { Observable } from 'rxjs';
import {LoginMehkRequest} from '../model/LoginMehkRequest';
import { Condition } from 'selenium-webdriver';
import { MehkLoginResponse } from '../model/MehkLoginResponse';
import { aboutInfo } from '../model/about.model';
import { profileInfo } from '../model/profile.model';
import { loginInfo } from '../model/login.model';
import { accountInfo } from '../model/account.model';
import { newpostInfo } from '../model/newpost.model';
import { postInfo, ConnectionInfo, headerInfo, chatEntryInfo } from '../model/post.model';
import { friendInfo } from '../model/friend.model';
import { searchInfo } from '../model/search.model';

@Injectable()
export class FAppService {
    constructor(public httpClient: HttpClient) { }

    public verifyUserInfo(LoginInfoRequest:loginInfo): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/LoginInfo/';

            const body = LoginInfoRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public manageAccountInfo(accountInfoRequest:accountInfo): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/AccountInfo/';

            const body = accountInfoRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public ProfileInfoFApp(ProfileInfoRequest:profileInfo): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/ProfileInfo/';

            const body = ProfileInfoRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public AboutInfoFApp(AboutInfoRequest:aboutInfo): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/AboutInfo/';

            const body = AboutInfoRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public NewPostInfoFApp(NewPostInfoRequest:newpostInfo): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/NewPostInfo/';

            const body = NewPostInfoRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public LoadPostInfo(PostInfoRequest:postInfo): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/PostInfo/';
            PostInfoRequest.mid=localStorage.getItem("mid")
            const body = PostInfoRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public LoadMyPostIdInfo(PostInfoRequest:postInfo): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/MyPostInfo/';

            const body = PostInfoRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public SavePostCommentInfo(CommentInfoRequest:any): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/SavePostComment/';

            const body = CommentInfoRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public SavePostDescInfo(PostInfoRequest:postInfo): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/SavePostDescInfo/';

            const body = PostInfoRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public SaveLikesInfo(CommentInfoRequest:any): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/SaveLikesInfo/';

            const body = CommentInfoRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public RemovePostInfo(PostInfoRequest:postInfo): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/RemovePostInfo/';

            const body = PostInfoRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public LoadFriendRequestList(FriendInfoRequest:friendInfo): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/FriendRequest/';

            const body = FriendInfoRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public LoadFriendList(FriendInfoRequest:friendInfo): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/FriendList/';

            const body = FriendInfoRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public LoadMemberInfo(FriendInfoRequest:friendInfo): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/MemberInfo/';

            const body = FriendInfoRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public ManageRequestInfo(FriendInfoRequest:friendInfo): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/ManageRequest/';

            const body = FriendInfoRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public LoadGeneralSearchList(FriendInfoRequest:friendInfo): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/GeneralSearch/';

            const body = FriendInfoRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public LoadTimeLineInfo(PostInfoRequest:postInfo): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/TimeLine/';

            const body = PostInfoRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public UpdateConnectionId(ConnectionInfoRequest:ConnectionInfo): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/UpdateConnectionInfo/';

            const body = ConnectionInfoRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public SearchProfile(searchInfoRequest:searchInfo): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/Search';

            const body = searchInfoRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public ManageRequestFromElasticInfo(FriendInfoRequest:friendInfo): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/ManageRequest/';

            const body = FriendInfoRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public LoadNotificationInfo(headerRequest:headerInfo): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/Notification/';

            const body = headerRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public updateLivePageInfo(headerRequest:headerInfo): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/updateLivePageInfo/';

            const body = headerRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public ChatUserList(headerRequest:headerInfo): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/ChatUserList/';

            const body = headerRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public GetInitialChatInfo(chatEntryInfoRequest:chatEntryInfo): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/GetInitialChatInfo/';

            const body = chatEntryInfoRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public OnUpdateChat(chatEntryInfoRequest:chatEntryInfo): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/ChatEntry/';

            const body = chatEntryInfoRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public OnUpdateChatTypingStaus(chatEntryInfoRequest:chatEntryInfo): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/ChatStatus/';

            const body = chatEntryInfoRequest;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }

    public follow(FollowInfo:any): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/Follow/';

            const body = FollowInfo;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }
    public followersInfo(FollowersInfo:any): Observable<any> {
        return new Observable((observer) => {
            const httpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
                'Access-Control-Allow-Origin': '*'
            });
            const options = {
                headers: httpHeaders
            };
            const URL = config.FApp.ApiUrl + 'fapp/FollowersInfo/';

            const body = FollowersInfo;

            this.httpClient.post(URL, body, options).subscribe(data => {
                observer.next(data);
            }, error => {
                console.log(error);
                observer.next(false);
            });
        });
    }
   
    
    

}