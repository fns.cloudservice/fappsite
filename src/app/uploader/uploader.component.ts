import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {SharedService} from '../service/shared.service';
import { MehkLoginResponse } from '../model/MehkLoginResponse';
import {AuthService} from '../service/auth.service';

@Component({
    selector: 'uploader',
    templateUrl: './uploader.component.html',
    styleUrls: ['./uploader.component.scss']
})
export class UploaderComponent implements OnInit {
  savedData:MehkLoginResponse;
  
  isHovering: boolean;

  files: File[] = [];

  toggleHover(event: boolean) {
    this.isHovering = event;
  }

  onDrop(files: FileList) {
    for (let i = 0; i < files.length; i++) {
      this.files.push(files.item(i));
    }
  }

  constructor(private router:Router, private sharedService: SharedService, private auth: AuthService) {
    
    
   }
   

   startUpload(event: FileList) {
        for (let i = 0; i < event.length; i++) {
            this.files.push(event.item(i));
        }
  }

  ngOnInit() {
    //this.LoadAllJsFiles();
  }
  
  ngAfterContentChecked() {
    //this.auth.isuserLoggedIn();
  }
  
  public loadScript(url) {
    //console.log('preparing to load...')
    let node = document.createElement('script');
    node.src = url;
    node.type = 'text/javascript';
    document.getElementsByTagName('head')[0].appendChild(node);
 }

  private LoadAllJsFiles(){
    this.loadScript('../../assets/global/plugins/jquery.min.js');
    this.loadScript('../assets/global/plugins/bootstrap/js/bootstrap.min.js');
    this.loadScript('../assets/global/plugins/js.cookie.min.js');     
    this.loadScript('../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); 
    this.loadScript('../assets/global/plugins/jquery.blockui.min.js'); 
    this.loadScript('../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');

    this.loadScript('../assets/global/plugins/dropzone/dropzone.min.js');  
    
    this.loadScript('../assets/global/scripts/app.min.js'); 

    this.loadScript('../assets/pages/scripts/form-dropzone.min.js');

    this.loadScript('../assets/layouts/layout4/scripts/layout.min.js'); 
    this.loadScript('../assets/layouts/layout4/scripts/demo.min.js');
    this.loadScript('../assets/layouts/global/scripts/quick-sidebar.min.js');     
    this.loadScript('../assets/layouts/global/scripts/quick-nav.min.js'); 
 }

}
