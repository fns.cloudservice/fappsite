import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadpreviewComponent } from './uploadpreview.component';

describe('UploadpreviewComponent', () => {
  let component: UploadpreviewComponent;
  let fixture: ComponentFixture<UploadpreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadpreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadpreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
