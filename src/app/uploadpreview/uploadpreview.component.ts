import { Component, OnInit, Input, ChangeDetectorRef, ElementRef, ViewChild } from '@angular/core';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { finalize, tap } from 'rxjs/operators';

@Component({
  selector: 'uploadpreview',
  templateUrl: './uploadpreview.component.html',
  styleUrls: ['./uploadpreview.component.scss']
})
export class UploadpreviewComponent implements OnInit {

  @Input() file: File;

  task: AngularFireUploadTask;

  percentage: Observable<number>;
  snapshot: Observable<any>;
  downloadURL: string;

  @ViewChild('start', { static: false }) start: ElementRef; 

  constructor(private storage: AngularFireStorage, private db: AngularFirestore) { }

  ngOnInit() {
    this.startUpload();
    setTimeout(()=> { this.start.nativeElement.click(); } , 5000);
  }

  startUpload() {

    // The storage path
    const path = `test/${Date.now()}_${this.file.name}`;

    // Reference to storage bucket
    const ref = this.storage.ref(path);

    // The main task
    this.task = this.storage.upload(path, this.file);

    // Progress monitoring
    this.percentage = this.task.percentageChanges();

    this.snapshot   = this.task.snapshotChanges().pipe(
      tap(console.log),
      // The file's download URL
      finalize( async() =>  {
        this.downloadURL = await ref.getDownloadURL().toPromise();

        this.db.collection('files').add( { downloadURL: this.downloadURL, path });
      }),
    );
  }

  isActive(snapshot) {
    return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes;
  }

  OnDeleteImg(url:string){
    //debugger;
    //url="https://firebasestorage.googleapis.com/v0/b/fnsapp-c2770.appspot.com/o/test%2F1566743500156_user-20.jpg?alt=media&token=3cd2c862-0ada-4c2b-a21c-58a8e35ff7fb";
    var filename = url.substr(url.lastIndexOf("test%2F") + 7,(url.lastIndexOf("?") - (url.lastIndexOf("test%2F") + 7)  ))
    var mFirebaseStorage = this.storage.storage.ref();
    var desertRef = mFirebaseStorage.child("test/" + filename);//images/desert.jpg
    
    // Delete the file
    desertRef.delete().then(function() {
      // File deleted successfully
    }).catch(function(error) {
      // Uh-oh, an error occurred!
    });

  }

}
