import { Component, OnInit } from '@angular/core';
import { ProfileResponse } from '../model/ProfileResponse';
import { FAppService } from '../service/fapp.service';
import { ToastrService } from 'ngx-toastr';
import {AuthService} from '../service/auth.service';
import { profileInfo } from '../model/profile.model';
import { followersInfo } from '../model/Followers.model';
import {FollowersResponse} from '../model/FollowerResponse'



@Component({
  selector: 'app-followers',
  templateUrl: './followers.component.html',
  styleUrls: ['./followers.component.scss']
})
export class FollowersComponent implements OnInit {
  savedData:ProfileResponse; profileInfoData : profileInfo;Name:string;followersInfo:followersInfo;followersResponse:FollowersResponse;FollowersInfo:any[]=[];
  showAlert:boolean=false;
  constructor(private auth: AuthService,private fappService:FAppService,private toastr: ToastrService,) {
    this.savedData = JSON.parse(localStorage.getItem("profileInfo")) as ProfileResponse;
    this.followersInfo = new followersInfo();
   }

  ngOnInit() {
    this.auth.isuserLoggedIn();
    this.followersList();
  }
  ngAfterContentChecked() {
    
  }
followersList(){
  this.FollowersInfo=[];
  let obj ={fmid:this.savedData.profileInfo.mid}
  this.fappService.followersInfo(obj).subscribe(response => {
    if(response){
     
      const resp = response as FollowersResponse;
      if(response.key ==='Success'){ 
        console.log(response)
        this.Name=response.followersList
        for(let item of response.followersList){
          this.FollowersInfo.push(item);
        }

          //this.toastr.success(response.message);

      }else{
        this.showAlert=true;
        //this.toastr.error(response.message);
      }
    }else{
      this.toastr.error("something gone wrong !");
    }
  });




}

}
