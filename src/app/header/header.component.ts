import { Component, OnInit, ElementRef, ViewChild, ChangeDetectorRef, HostListener, OnDestroy, Output, EventEmitter  } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { postInfo, ConnectionInfo } from '../model/post.model';
import { SignalRService } from '../service/signalr.service';
import {SharedService} from '../service/shared.service';
import { MehkLoginResponse } from '../model/MehkLoginResponse';
import { Router } from '@angular/router';
import {AuthService} from '../service/auth.service';
import { ProfileResponse } from '../model/ProfileResponse';
import { NotifyResponse } from '../model/NotifyResponse';
import { profileInfo } from '../model/profile.model';
import { FAppService } from '../service/fapp.service';
import { headerInfo, chatEntryInfo } from '../model/post.model';
import { ToastrService } from 'ngx-toastr';
import { NgxLoadingModule } from 'ngx-loading';
import { Message } from 'primeng/api';
import * as moment from 'moment';
import { parse } from 'querystring';


declare function fnShowHideSideBar(): any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  profileInfoData : profileInfo; savedData:ProfileResponse; public ON: string; public success: string; public info: string; public warning: string; public danger: string;
  loggedIn:boolean=false; CurrentUser:string="Nick"; userImgPath:string="../assets/layouts/layout4/img/avatar9.jpg"; userdefaultImgPath:string="../assets/layouts/layout4/img/usr.jpg";
  headerInfo : headerInfo; Notify : any; loading = false; notifyCount : any = 0; private _hubConnection: HubConnection; msgs: Message[] = [];connectionInfo : ConnectionInfo;
  chatToggleClass:any; chatOnlineClass:any; ChatUserList : any; chatEntryInfo : chatEntryInfo; cmateid : any; InitialChatInfo : any; dynamicChatInfo : any; chatMsg : any;
  chatuserImgPath:string;chatuserName:string; currentChatUserStatus: any; toconid:any; prevmsgDate:any; prevmsgDateClass:any; sortedInitialChatInfo : any; cummsgCount:any;
  searchText:string; showNotify:boolean=false;

  @ViewChild('username', { static: false }) username: ElementRef;
  @ViewChild('usrmsg', { static: true }) usrmsg: ElementRef;

  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.OnSideBarClick('C');
  }

  constructor(private route:Router, private sharedService: SharedService, private auth: AuthService, private fappService:FAppService, private toastr: ToastrService,
     private signalR:SignalRService) {
    this.sharedService.CurrentUser.subscribe( value => { this.CurrentUser = value; });
    this.sharedService.userImgPath.subscribe( value => { this.userImgPath = value; });
    /*this.sharedService.loggedIn.subscribe( value => { 
      this.loggedIn = value; 
    });*/
    this.sharedService.notifyCount.subscribe( value => { this.notifyCount = value; });
    this.sharedService.dynamicChatInfo.subscribe( value => { this.OnDynamicChatInfo(this.signalR.currentdynamicmsg); });
    this.sharedService.currentChatUserStatus.subscribe( value => {
      this.currentChatUserStatus = value;

    });
    this.sharedService.updateindividualchatStatus.subscribe( value => {
      if(this.ChatUserList != undefined){
        for(var i = 0; i <= this.ChatUserList.length; i++ ){
          if(this.ChatUserList[i] != undefined && this.ChatUserList[i].dsnm == value){
            this.ChatUserList[i].isOnline = 'Online';
          }
        }
      }
    });
    this.sharedService.receiptStatus.subscribe( value => {
      if(value != ''){
        this.UpdateDeliveryStatus(value);
      }
    });
    this.sharedService.msgCount.subscribe( value => {
      console.log('V : ' + value)
      if(value != ''){
        this.UpdatemsgCount(value);
      }
    });
    // this.sharedService.cummsgCount.subscribe( value => {
    //   if(value != '' && value != '0'){
    //     this.cummsgCount = parseInt(this.cummsgCount) + parseInt(value);
    //   }
    // });
    this.savedData = JSON.parse(localStorage.getItem("profileInfo")) as ProfileResponse;
    if(this.savedData != null){
      if(this.savedData && this.savedData.profileInfo){
        this.profileInfoData = this.savedData.profileInfo;
      }else{
        this.profileInfoData = new  profileInfo();
      }
    }
    this.headerInfo = new headerInfo();
    this.chatEntryInfo = new chatEntryInfo();
    this.cummsgCount = 0;
    this.sharedService.savedDataResponse.subscribe(value=>{
      if(value!=""){
        this.savedData = JSON.parse(value);
        this.getNotification();
        this.showNotify =true;
      }
    })
  }

  ngOnInit() {
    if(!this.signalR.connectionExists && window.location.href!="http://localhost:4200/#/login"){
      this.signalR.startConnection();
      this.signalR.addTransferFeedDataListener();
      this.showNotify =true;
    }
    this.OnChatMemberClick('','C','','');
  if(this.savedData != null){
    this.displayuserInfo();
    this.getNotification();
   }
   else{
     return;
   }
   
  }

  ngOnDestroy() {
    alert('On Destroy called')
    if(this.savedData != null){
      console.log('OnDestroy Called!');
      this.UpdateConnectionId('R');
      this.signalR.connectionExists = false;
    }
   }

  ngAfterContentChecked() {
    if(this.auth.currentUser){
      this.sharedService.CurrentUser.next(this.auth.currentUser.displayName);
      this.loggedIn=true;
    }
    else{
      this.loggedIn=false;
    }
  }

  public displayuserInfo(){
    if(this.auth.currentUser){
      this.username.nativeElement.innerHTML = this.auth.currentUser.displayName;
      this.sharedService.CurrentUser.next(this.auth.currentUser.displayName);
    }
    this.SetProfileImg();
  }

  public SetProfileImg(){
    if(this.savedData != null && this.savedData.profileInfo.filepath != null && this.savedData.profileInfo.filepath != ""){
      this.sharedService.userImgPath.next(this.savedData.profileInfo.filepath);
    }
    else if(this.auth.currentUser){
      this.sharedService.userImgPath.next(this.auth.currentUser.photoURL);
    }
    else{
      this.sharedService.userImgPath.next(this.userdefaultImgPath);
    }
  }

  public updateLivePageInfo(){
    if(this.savedData == null){
      return;
    }
    this.loading = true;
    this.headerInfo.mid = this.savedData.profileInfo.mid
    this.fappService.updateLivePageInfo(this.headerInfo).subscribe(response => {
      if(response){
        // const resp = response as NotifyResponse;
        // if(response.status == "Success"){
        //   if(response.dtConnectionId == null){
        //     return;
        //   }
        //   this.ChatUserList = [];

        //   if(response.dtConnectionId.length > 0){
        //     this.ChatUserList = response.dtConnectionId;
        //   }
        //   this.UpdateCumulativemsgCount();
        // }else{
        //   this.toastr.error(response.message);
        // }
        this.loading = false;
      }
    });
  }

  public getChatUserList(){
      if(this.savedData==null){
      return;
    }
    this.loading = true;
    this.headerInfo.mid = this.savedData.profileInfo.mid
    this.fappService.ChatUserList(this.headerInfo).subscribe(response => {
      if(response){
        const resp = response as NotifyResponse;
        if(response.status == "Success"){
          if(response.dtConnectionId == null){
            return;
          }
          this.ChatUserList = [];
          //this.notifyCount = response.dtConnectionId.length;
          //this.sharedService.notifyCount.next(this.notifyCount);
          //localStorage.setItem("notifyCount",this.notifyCount);
          //this.sharedService.isReloadViewAll.next(true);

          if(response.dtConnectionId.length > 0){
            this.ChatUserList = response.dtConnectionId;
          }
          this.UpdateCumulativemsgCount();
        }else{
          this.toastr.error(response.message);
        }
        this.loading = false;
      }
    });
  }

  public GetInitialChatInfo(){
    //this.usrmsg.nativeElement.innerHTML = '';
    if(this.savedData==null){
      return;
    }
    this.loading = true;
    this.chatEntryInfo.fmid = this.savedData.profileInfo.mid;
    this.chatEntryInfo.tmid = this.cmateid;
    this.fappService.GetInitialChatInfo(this.chatEntryInfo).subscribe(response => {
      if(response){
        if(response.status == "Success"){
          if(response.dtConnectionId == null){
            return;
          }
          this.InitialChatInfo = [];

          this.toconid = response.notifymsg;
          if(response.dtConnectionId.length > 0){
            this.InitialChatInfo = response.dtConnectionId;
            console.log(this.InitialChatInfo);
          }
          this.ResetMsgCount(this.chatEntryInfo.tmid);
        }else{
          this.toastr.error(response.message);
        }
        this.loading = false;
      }
    });
  }

  ResetMsgCount(mid : any){
    if(this.ChatUserList != undefined){
      for(var i = 0; i < this.ChatUserList.length; i++){
        if(this.ChatUserList[i].mateid == mid){
          this.ChatUserList[i].newMsgCount =  0;
        }
      }
      this.UpdateCumulativemsgCount();
    }
  }


  public OnUpdateChatTypingStaus(event){
    // if (event.key === "Enter") {
    //   console.log(event);
    //   return;
    // }
    //this.loading = true;
    if(this.toconid === undefined){
      return;
    }
    if(this.savedData==null){
      return;
    }
    this.chatEntryInfo.fmid = this.savedData.profileInfo.mid;
    this.chatEntryInfo.tmid = this.cmateid;
    this.chatEntryInfo.msg = this.toconid;
    this.chatEntryInfo.flag = this.chatMsg === undefined || this.chatMsg == '' ? 'E' : 'S';
    this.fappService.OnUpdateChatTypingStaus(this.chatEntryInfo).subscribe(response => {
      if(response){
        if(response.status == "Success"){
          console.log(this.currentChatUserStatus);
        }else{
          this.toastr.error(response.message);
        }
        //this.loading = false;
      }
    });
  }

  public OnUpdateChat(){
    if(this.chatMsg === undefined || this.chatMsg == ''){
      return false;
    }
    if(this.savedData==null){
      return;
    }
    //this.loading = true;
    this.chatEntryInfo.fmid = this.savedData.profileInfo.mid;
    this.chatEntryInfo.tmid = this.cmateid;
    this.chatEntryInfo.msg = this.chatMsg;
    this.chatEntryInfo.msgtime = Date.now().toString();
    this.chatEntryInfo.flag = 'S';
    this.UpdatePostOutMsgInstantly(this.chatEntryInfo.msg, this.chatEntryInfo.msgtime);

    this.fappService.OnUpdateChat(this.chatEntryInfo).subscribe(response => {
      if(response){
        if(response.status == "Success"){
          if(response.dtConnectionId == null){
            return;
          }
          this.dynamicChatInfo = [];

          if(response.dtConnectionId.length > 0){
            this.dynamicChatInfo = response.dtConnectionId;
            // var dynamicChat = '<div class="post out"><div class="message bg-green-soft bg-font-green-soft" style="margin-right: 2px"><span class="arrow"></span>';
            // dynamicChat += '<span class="body" style="color: #FFF!important;">' + this.getCurrentRowOfDynamicChatInfo(0,'msg')
            // + '</span><span class="datetime" style="color: #FFF!important;">' + this.getCurrentRowOfDynamicChatInfo(0,'msgtime')
            // +'</span><i class="fa fa-check tooltips" data-original-title="success input!"></i></div></div>';
            // this.usrmsg.nativeElement.insertAdjacentHTML('beforeend', dynamicChat);
            //this.InitialChatInfo.push({ asort: null, flag : 'S', dsnm: null, fmid: null, msg:this.chatEntryInfo.msg, msgtime:this.chatEntryInfo.msgtime, tconid:null, tmid:null, usertype:'C' });
            this.UpdateInitialChatInfo(this.dynamicChatInfo);
            console.log(this.InitialChatInfo)
            this.chatMsg = '';
          }
        }else{
          this.toastr.error(response.message);
        }
        //this.loading = false;
      }else{
        this.toastr.error('Oops!!! We are unable to deliver your message!');
      }
    });
  }

  UpdateDeliveryStatus(strinput : any){
    if(this.InitialChatInfo != undefined){
      var strsplit = strinput.split(',');
      for(var i = 0; i < this.InitialChatInfo.length; i++){
        if(this.InitialChatInfo[i].tmid == strsplit[0] && this.InitialChatInfo[i].flag == 'S' && strsplit[1] == 'D'){
          this.InitialChatInfo[i].flag =  'D';
        }else if(this.InitialChatInfo[i].tmid == strsplit[0] && (this.InitialChatInfo[i].flag == 'S' || this.InitialChatInfo[i].flag == 'D') && strsplit[1] == 'R'){
          this.InitialChatInfo[i].flag =  'R';
        }
      }
    }
  }

  UpdatemsgCount(strinput : any){
    console.log(this.ChatUserList);
    if(this.ChatUserList != undefined){
      var strsplit = strinput.split(',');
      if(strsplit[1] == "0"){
        return;
      }
      for(var i = 0; i < this.ChatUserList.length; i++){
        if(this.ChatUserList[i].mateid == strsplit[0]){
          this.ChatUserList[i].newMsgCount =  strsplit[1];
        }
      }
    }
    this.UpdateCumulativemsgCount();
  }

  UpdateCumulativemsgCount(){
    this.cummsgCount = 0;
    if(this.ChatUserList != undefined){
      for(var i = 0; i < this.ChatUserList.length; i++){
        this.cummsgCount = parseInt(this.cummsgCount) + parseInt(this.ChatUserList[i].newMsgCount == null ? 0 : this.ChatUserList[i].newMsgCount);
      }
      //this.cummsgCount =
    }else{
      this.cummsgCount = parseInt(this.cummsgCount) + 1;
    }
    //this.sharedService.cummsgCount.next(this.cummsgCount)
  }

  UpdateInitialChatInfo(dynamicChatInfo : any){
    this.InitialChatInfo.find(item => item.msgtime == this.chatEntryInfo.msgtime).conid = dynamicChatInfo[0].conid;
    this.InitialChatInfo.find(item => item.msgtime == this.chatEntryInfo.msgtime).dsnm = dynamicChatInfo[0].dsnm;
    this.InitialChatInfo.find(item => item.msgtime == this.chatEntryInfo.msgtime).flag = dynamicChatInfo[0].flag;
    this.InitialChatInfo.find(item => item.msgtime == this.chatEntryInfo.msgtime).fmid = dynamicChatInfo[0].fmid;
    this.InitialChatInfo.find(item => item.msgtime == this.chatEntryInfo.msgtime).imgpath = dynamicChatInfo[0].imgpath;
    this.InitialChatInfo.find(item => item.msgtime == this.chatEntryInfo.msgtime).msg = dynamicChatInfo[0].msg;
    this.InitialChatInfo.find(item => item.msgtime == this.chatEntryInfo.msgtime).tmid = dynamicChatInfo[0].tmid;
    this.InitialChatInfo.find(item => item.msgtime == this.chatEntryInfo.msgtime).usertype = dynamicChatInfo[0].usertype;
  }

  sortInitialChatInfo(){
    var sortedArray = this.InitialChatInfo.slice(0);
    sortedArray.sort((left, right) => {
        if (left.msgtime < right.msgtime) return -1;
        if (left.msgtime > right.msgtime) return 1;
        return 0;
    });
    console.log('sorting')
    console.log(sortedArray);
  }

  public UpdatePostOutMsgInstantly(msg:any, msgtime: any){
    this.InitialChatInfo.push({ fmid: null, tmid:null, msg:this.chatEntryInfo.msg, msgtime:parseInt(this.chatEntryInfo.msgtime), flag : 'C', asort: null, dsnm: null,  tconid:null, usertype:'C', imgpath: null });
    //console.log(this.InitialChatInfo)
    // var dynamicChat = '<div class="post out"><div class="message bg-green-soft bg-font-green-soft" style="margin-right: 2px"><span class="arrow"></span>';
    // dynamicChat += '<span class="body" style="color: #FFF!important;">' + msg
    // + '</span><span class="datetime" style="color: #FFF!important;">' + moment.unix(msgtime/1000).format("hh:mm a")
    // +'</span><i class="fa fa-check tooltips" data-original-title="success input!"></i></div></div>';
    // this.usrmsg.nativeElement.insertAdjacentHTML('beforeend', dynamicChat);
    this.chatMsg = '';
  }

  public OnDynamicChatInfo(strChatInfo : any){
    if(strChatInfo === undefined || this.InitialChatInfo === undefined){
      return;
    }
    var chatinfo = strChatInfo.split("#");
    this.InitialChatInfo.push({ fmid: null, tmid:null, msg: chatinfo[0], msgtime: parseInt(chatinfo[1]), flag : chatinfo[5], asort: null, dsnm: chatinfo[2],  tconid:null, usertype: 'O',imgpath: chatinfo[3] });

    // var strClass = "post in";//chatinfo[4] == "C" ? "post out" : "post in";
    // //console.log(chatinfo);
    // var dynamicChat = '<div class="'+ strClass +'"><div class="message bg-font-grey-salt" style="margin-left: 2px"><span class="arrow"></span>';
    // dynamicChat += '<span class="body" style="color: #FFF!important;">' + chatinfo[0]
    // + '</span><span class="datetime" style="color: #FFF!important;"> ' + moment.unix(chatinfo[1]/1000).format("hh:mm a") + '</span></div></div>';
    // this.usrmsg.nativeElement.insertAdjacentHTML('beforeend', dynamicChat);
    this.sharedService.currentChatUserStatus.next("");
  }

  public getNotification(){
    if(this.savedData==null){
      return;
    }
    this.loading = true;
    this.headerInfo.mid = this.savedData.profileInfo.mid
    this.fappService.LoadNotificationInfo(this.headerInfo).subscribe(response => {
      if(response){
        const resp = response as NotifyResponse;
        if(response.status == "Success"){
          if(response.dtConnectionId == null){
            return;
          }
          this.Notify = [];
          this.notifyCount = response.dtConnectionId.length;
          this.sharedService.notifyCount.next(this.notifyCount);
          localStorage.setItem("notifyCount",this.notifyCount);
          this.sharedService.isReloadViewAll.next(true);

          if(response.dtConnectionId.length > 0){
            this.Notify = response.dtConnectionId;
          }
        }else{
          this.toastr.error(response.message);
        }
        this.loading = false;
      }
    });
  }

  public getRowNotification(iRow : any, key: any) : any {
    if(key == 'P')
      return this.Notify[iRow].postid;
    else if(key == 'M')
     return this.Notify[iRow].msg;
    else if(key == 'I')
     return this.Notify[iRow].mid;
  }

  public getCurrentRowChatUser(iRow : any, key: any) : any {
    if(key == 'dsnm')
      return this.ChatUserList[iRow].dsnm == null ? 'No Name' : this.ChatUserList[iRow].dsnm;
    else if(key == 'isOnline')
     return this.ChatUserList[iRow].isOnline;
    else if(key == 'imgpath')
     return  this.ChatUserList[iRow].imgpath == null ? this.userdefaultImgPath : this.ChatUserList[iRow].imgpath;
    else if(key == 'mateid')
     return this.ChatUserList[iRow].mateid;
    else if(key == 'newMsgCount')
     return this.ChatUserList[iRow].newMsgCount == null ? '0' : this.ChatUserList[iRow].newMsgCount;
  }

  public getCurrentRowOfInitialChatInfo(iRow : any, key: any) : any {
    if(key == 'dsnm')
      return this.InitialChatInfo[iRow].dsnm;
    else if(key == 'msg')
     return this.InitialChatInfo[iRow].msg;
    else if(key == 'msgtime'){
      var strmsgdt = moment.unix(this.InitialChatInfo[iRow].msgtime/1000).format("DD MMM YYYY");//DD MMM YYYY hh:mm a
      var strmsgtime = moment.unix(this.InitialChatInfo[iRow].msgtime/1000).format("hh:mm a");
      return strmsgtime;
    }else if(key == 'msgdate'){
      var strmsgdt = moment.unix(this.InitialChatInfo[iRow].msgtime/1000).format("DD MMM YYYY");

      if(this.prevmsgDate === undefined || this.prevmsgDate == "" || this.prevmsgDate != strmsgdt){
        this.prevmsgDate = strmsgdt;
      }
      else if(iRow != 0){
        strmsgdt = '';
      }
      //console.log('iRow :' + iRow + ' msgdt : ' + strmsgdt);
      return strmsgdt;
    }
    else if(key == 'imgpath')
     return this.InitialChatInfo[iRow].imgpath;
    else if(key == 'mateid')
     return this.InitialChatInfo[iRow].mateid;
  }

  public getCurrentRowOfDynamicChatInfo(iRow : any, key: any) : any {
    if(key == 'dsnm')
      return this.dynamicChatInfo[iRow].dsnm;
    else if(key == 'msg')
     return this.dynamicChatInfo[iRow].msg;
    else if(key == 'msgtime'){
      var strmsgdt = moment.unix(this.dynamicChatInfo[iRow].msgtime/1000).format("DD MMM YYYY");//DD MMM YYYY hh:mm a
      var strmsgtime = moment.unix(this.dynamicChatInfo[iRow].msgtime/1000).format("hh:mm a");
      return strmsgtime;
    }else if(key == 'msgdate'){
      var strmsgdt = moment.unix(this.dynamicChatInfo[iRow].msgtime/1000).format("DD MMM YYYY");

      if(this.prevmsgDate === undefined || this.prevmsgDate == "" || this.prevmsgDate != strmsgdt){
        this.prevmsgDate = strmsgdt;
      }
      else{
        strmsgdt = '';
      }
      console.log(strmsgdt)
      return strmsgdt;
    }
    else if(key == 'imgpath')
     return this.dynamicChatInfo[iRow].imgpath;
    else if(key == 'mateid')
     return this.dynamicChatInfo[iRow].mateid;
  }

  public ViewSinglePost(postid : any) {
    this.sharedService.vPostId.next(postid);
    this.route.navigateByUrl("viewpost");
  }

  public OnViewAll() {
    this.route.navigateByUrl("viewall");
   }

  public logout():void {
    this.signalR.UpdateConnectionId('R')
    this.sharedService.CurrentUser.next("");
    this.sharedService.userImgPath.next(this.userdefaultImgPath);
    this.sharedService.loggedIn.next(false);
    this.auth.signOut();
    this.showNotify=false;
    console.log('connection Disconnected!')
    //this.route.navigateByUrl("login");
   }

   public OnHeaderNavClick(mnu){
    if(mnu == "P"){
      this.route.navigateByUrl("profile");
    }else if(mnu == "A"){
      this.route.navigateByUrl("account");
    }else if(mnu == "LK"){
      this.route.navigateByUrl("lock");
    }else if(mnu == "LO"){
      console.log("In OnHeaderNavClick home in header");
      this.route.navigateByUrl("home");
    }else if(mnu == "FL"){
      this.route.navigateByUrl("friendlist");
    }else if(mnu == "FF"){
      this.route.navigateByUrl("findfriend");
    }else if(mnu == "NB"){
      this.route.navigateByUrl("nearby");
    }else if(mnu == "TL"){
      this.route.navigateByUrl("timeline");
    }else if(mnu == "AB"){
      this.route.navigateByUrl("about");
    }else if(mnu == "NF"){
      this.route.navigateByUrl("newsfeed");
    }else if(mnu == "MSG"){
      this.route.navigateByUrl("message");
    }else if(mnu == "AL"){
      this.route.navigateByUrl("album");
    }else if(mnu == "DB"){
      this.route.navigateByUrl("home");
    }else if(mnu == "LI"){
      this.route.navigateByUrl("login");
    }else if(mnu == "CP"){
      this.route.navigateByUrl("changepwd");
    }else if(mnu == "NP"){
      this.route.navigateByUrl("newpost");
    }else if(mnu == "MP"){
      this.route.navigateByUrl("mypost");
    }else if(mnu == "FW"){
      this.route.navigateByUrl("followers");
    }else if(mnu == "SE"){
      this.route.navigateByUrl("search");
    }else if(mnu == "PR"){
      this.route.navigateByUrl("privacy");
    }else if(mnu == "HP"){
      this.route.navigateByUrl("help");
    }else if(mnu == "TE"){
      this.route.navigateByUrl("terms");
    }

  }

  UpdateConnectionId(action : string){
    if(this.savedData==null){
      return;
    }
      this.connectionInfo = new ConnectionInfo();
      this.connectionInfo.mid = this.savedData.profileInfo.mid;
      this.connectionInfo.connectionid = this.signalR.connectionId;
      this.connectionInfo.crtime = Date.now().toString();
      this.connectionInfo.action = action;

      this.fappService.UpdateConnectionId(this.connectionInfo).subscribe(response => {
        if(response){
          //this.toastr.success(response.message);
        }
      });
  }

  OnSideBarClick(action : any){
    let body = document.getElementsByTagName('body')[0];
    if(action == 'O'){
      this.getChatUserList();
      body.classList.add('page-quick-sidebar-open');
    }
    else{
        this.updateLivePageInfo();
        body.classList.remove('page-quick-sidebar-open');
      }
  }
  onmenuClick(){
    //console.log("reached here!")
    fnShowHideSideBar()
  }

  OnMoreClick(){
    var li = document.getElementById("moreli");
    li.classList.add('open');
  }

  OnChatMemberClick(mateid:any, key:any, dsnm:any, imgPath:any){
    this.cmateid = mateid;
    if(key == 'O'){
      this.chatuserName = dsnm;
      this.chatuserImgPath = imgPath;
      this.GetInitialChatInfo();
      this.chatToggleClass = 'tab-pane active page-quick-sidebar-chat page-quick-sidebar-content-item-shown';
    }
    else{
      this.chatuserImgPath = this.userdefaultImgPath;
      this.updateLivePageInfo();
      this.chatToggleClass = 'tab-pane active page-quick-sidebar-chat';
    }

  }

  GetOnlineChatClass(iRow : any) : any {
    if(this.ChatUserList[iRow].isOnline == 'Online' ){
      this.chatOnlineClass = 'label label-sm label-success';
    }
    else
      this.chatOnlineClass = 'label label-sm label-default';

      return this.chatOnlineClass;
  }

  GetInitialChatClass(iRow : any) : any {
    var InitialChatClass = ''
    if(this.InitialChatInfo[iRow].usertype == 'C' ){
      InitialChatClass = 'post out';
    }
    else
    InitialChatClass = 'post in';

      return InitialChatClass;
  }

  GetInitialChatTickClass(iRow : any) : any {
    var InitialChatClass = ''
    if(this.InitialChatInfo[iRow].usertype == 'C' && this.InitialChatInfo[iRow].flag == 'S'){
      InitialChatClass = 'fa fa-check tooltips';
    }else if(this.InitialChatInfo[iRow].usertype == 'C' && this.InitialChatInfo[iRow].flag == 'D'){
      InitialChatClass = 'fa fa-check tooltips font-yellow';
    }else if(this.InitialChatInfo[iRow].usertype == 'C' && this.InitialChatInfo[iRow].flag == 'R'){
      InitialChatClass = 'fa fa-check tooltips font-green';
    }else if(this.InitialChatInfo[iRow].usertype == 'C' && this.InitialChatInfo[iRow].flag == 'E'){
      InitialChatClass = 'fa fa-exclamation-triangle';//fa fa-exclamation-circle  fa fa-exclamation
    }else
      InitialChatClass = 'fa fa-check tooltips hide';

      return InitialChatClass;
  }

  GetInitialChatMessageClass(iRow : any) : any {
    var InitialChatClass = ''
    if(this.InitialChatInfo[iRow].usertype == 'C' ){
      InitialChatClass = 'message bg-green-soft bg-font-green-soft';
    }
    else
    InitialChatClass = 'message bg-font-grey-salt';//bg-grey-salt

      return InitialChatClass;
  }

  GetInitialChatMsgDateClass(iRow : any) : any {
    var InitialChatMsgDateClass = 'label label-sm label-success';
    var strmsgdt = moment.unix(this.InitialChatInfo[iRow].msgtime/1000).format("DD MMM YYYY");

    if(this.prevmsgDateClass === undefined || this.prevmsgDateClass == "" || this.prevmsgDateClass != strmsgdt){
      this.prevmsgDateClass = strmsgdt;
    }
    else if(iRow != 0){
      strmsgdt = '';
    }

    if(strmsgdt == '' ){
      InitialChatMsgDateClass = 'label label-sm label-success hide';
    }
    return InitialChatMsgDateClass;
  }

  OnSearchClick(){
    var divSearch = document.getElementById("divSearch");
    var spnddl = document.getElementById("spnddl");

    if(divSearch.classList.contains('open') && (this.searchText == undefined || this.searchText == '')) {
      divSearch.classList.remove('open');
      spnddl.classList.add('hide');
    }else{
      if(!divSearch.classList.contains('open')) {
        divSearch.classList.add('open');
        spnddl.classList.remove('hide');
      }
    }
    if(this.searchText===undefined || this.searchText===''){
      return;
    }
    if(window.location.hash ==="#/search"){
      
      localStorage.setItem("searchText",this.searchText)
      this.searchText="";
      this.sharedService.inSearchUrl.next(true)

      
    }else{
      localStorage.setItem("searchText",this.searchText)
      this.searchText="";
      this.route.navigateByUrl('search')
    }
  }

  OnChangeSearchType(searchType){
    var spnsearchType = document.getElementById("spnsearchType");
    spnsearchType.innerText = searchType;

    if(searchType == 'Post')
      localStorage.setItem("searchType","P");
    else
      localStorage.setItem("searchType","M");
  }


}

