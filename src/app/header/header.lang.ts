export class HeaderInfoLang{
    public lang:any[];
    constructor(){        
        this.lang = new Array();
        var eng = new HeaderInfoLangText();
        eng.Home = "Home";        
        eng.MyAccount = "My Account";
        eng.ChangePassword = "Change Password";
        eng.SignOut = "Sign Out";
        eng.SignIn = "Sign In";
        eng.Permit = "My Application";


        var spn = new HeaderInfoLangText();
        spn.Home = "Casa";  
        spn.MyAccount = "Mi cuenta";
        spn.ChangePassword = "Cambia la contraseña";
        spn.SignOut = "Desconectar";
        spn.SignIn = "Registrarse";
        spn.Permit = "Mi aplicación";

        this.lang["english"] = eng;
        this.lang["spanish"] = spn;
    }
}

export class HeaderInfoLangText{
    public Home: string;
    public MyAccount: string;
    public ChangePassword: string;
    public SignOut: string;
    public SignIn: string;
    public Permit: string;
}