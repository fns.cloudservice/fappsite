import { Component,HostListener } from '@angular/core';
import {AuthService} from './service/auth.service'
import { LoadingBarService } from '@ngx-loading-bar/core';
import { interval, timer } from 'rxjs';
import { map, take, delay, withLatestFrom, finalize, tap } from 'rxjs/operators';
import{ SignalRService} from '../app/service/signalr.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @HostListener('window:unload', [ '$event' ])
  unloadHandler(event) {
  }

  @HostListener('window:beforeunload', [ '$event' ])
  beforeUnloadHandler(event) {
    this.signalR.UpdateConnectionId('R');
   
  }
  user = null;
  title = 'Fashion & Style Diary';
  constructor(private auth: AuthService, public loader: LoadingBarService ,private signalR:SignalRService ){

  }

  ngOnInit() {
    // this.auth.currentUser().subscribe(
    //   (user) => this.user = user);

    //this.auth.authState
    this.loader.start();
  }

  ngAfterViewInit() {
    console.log('app init stop');
    this.endloader();
  }

  private endloader(){
    this.loader.complete();
    this.loader.stop();
  }

}
