import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { AccountComponent } from './account/account.component';
import { LockComponent } from './lock/lock.component';
import { FriendlistComponent } from './friendlist/friendlist.component';
import { TimeLineComponent } from './timeline/timeline.component';
import { AboutComponent } from './about/about.component';
import { NewsFeedComponent } from './newsfeed/newsfeed.component';
import { AlbumComponent } from './album/album.component';
import { NearbyComponent } from './nearby/nearby.component';
import { SignupComponent } from './signup/signup.component';
import { LandingComponent } from './landing/landing.component';
import { LoginComponent } from './login/login.component';
import { ChangePwdComponent } from './changepwd/changepwd.component';
import { FileUploadComponent } from './fileupload/fileupload.component';
import { NewPostComponent } from './newpost/newpost.component';
import { UploaderComponent } from './uploader/uploader.component';
import { TestpostComponent } from './testpost/testpost.component';
import { TestnewpostComponent } from './testnewpost/testnewpost.component';
import { TestalbumComponent } from './testalbum/testalbum.component';
import { MypostComponent } from './mypost/mypost.component';
import { SlideComponent } from './controls/slide/slide.component';
import { HelpComponent } from './help/help.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { TermsComponent } from './terms/terms.component';
import { SearchComponent } from './search/search.component';
import { FindfriendComponent } from './findfriend/findfriend.component';
import { MessageComponent } from './message/message.component';
import { ViewallComponent } from './viewall/viewall.component';
import { ChatComponent } from './chat/chat.component';
import { ViewsinglepostComponent } from './viewsinglepost/viewsinglepost.component';
import {FollowersComponent} from './followers/followers.component';
import {PostComponent} from './post/post.component';



const routes: Routes = [
  
  { path: 'home', component: HomeComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'landing', component: LandingComponent },
  { path: 'login', component: LoginComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'account', component: AccountComponent },
  { path: 'lock', component: LockComponent },
  { path: 'friendlist', component: FriendlistComponent },
  { path: 'nearby', component: NearbyComponent },
  { path: 'timeline', component: TimeLineComponent },
  { path: 'about', component: AboutComponent },
  { path: 'newsfeed', component: NewsFeedComponent },
  { path: 'album', component: AlbumComponent },
  { path: 'changepwd', component: ChangePwdComponent },
  { path: 'fileupload', component: FileUploadComponent },
  { path: 'uploader', component: UploaderComponent },
  { path: 'newpost', component: NewPostComponent },
  { path: 'mypost', component: MypostComponent },
  { path: 'testpost', component: TestpostComponent },
  { path: 'testnewpost', component: TestnewpostComponent },
  { path: 'testalbum', component: TestalbumComponent },
  { path: 'Slide', component: SlideComponent },
  { path: 'help', component: HelpComponent },
  { path: 'privacy', component: PrivacyComponent },
  { path: 'terms', component: TermsComponent },
  { path: 'search', component: SearchComponent },
  { path: 'findfriend', component: FindfriendComponent },
  { path: 'message', component: MessageComponent },
  { path: 'viewpost', component: ViewsinglepostComponent },
  { path: 'viewall', component: ViewallComponent },
  { path: 'chat', component: ChatComponent },
  {path:'followers',component:FollowersComponent},
  {path:'post/:id',component:PostComponent},
  { path: '', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule] 
})
export class AppRoutingModule { }
