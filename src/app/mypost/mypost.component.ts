import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import {AlertsComponent} from "../controls/alerts/alerts.component"; 
import { Router, NavigationEnd } from '@angular/router';
import { FAppService } from '../service/fapp.service';
import { SharedService } from '../service/shared.service';
import { ConfirmService } from '../service/confirm.service';
import { ProfileResponse } from '../model/ProfileResponse';
import { profileInfo } from '../model/profile.model';
import { PostResponse } from '../model/PostResponse';
import { postInfo } from '../model/post.model';
import { ToastrService } from 'ngx-toastr';
import {AuthService} from '../service/auth.service';
import { finalize, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import * as _ from 'lodash';
import { NgxScrollEvent } from 'ngx-scroll-event';

@Component({
  selector: 'mypost',
  templateUrl: './mypost.component.html',
  styleUrls: ['./mypost.component.scss']
})
export class MypostComponent implements OnInit {
  posts: string[] = [];//["IP5DIQ5KVYYT6YYVIJT43","4TDEJfDDGVV5DKOTTTQ4P","YQGGDJHGTH3OJ4EKII4DQ"];
  people: any[] = [ {"PostId": "IP5DIQ5KVYYT6YYVIJT43"},{"PostId": "4TDEJfDDGVV5DKOTTTQ4P"},{"PostId": "YQGGDJHGTH3OJ4EKII4DQ"} ];
  savedData:ProfileResponse; profileInfoData : profileInfo; postInfo : postInfo;fireAsycLoad: string;noofBottomClicks:number;showAlert:boolean=false;

  @ViewChild(AlertsComponent, { static: true }) alert: AlertsComponent;

  constructor(private fappService:FAppService, private sharedService:SharedService, private router:Router, private toastr: ToastrService, 
    private auth: AuthService, private confirmService: ConfirmService) { 
    this.savedData = JSON.parse(localStorage.getItem("profileInfo")) as ProfileResponse;
    this.postInfo = new postInfo();
    this.noofBottomClicks = 0; this.fireAsycLoad = 'Y';

  }

  ngOnInit() {
    this.auth.isuserLoggedIn();
    this.LoadpostInfo(); 
  }

  ngAfterContentChecked() {
    
  }

  public RemovePost(postid : string)
  {
    //this.alert.openModal("MyPost Title" , "Are you sure you want to delete this post?");
    this.confirmService.confirm({ title:'Confirm deletion', message: 'Do you really want to delete this post permanently?' }).then(
      () => {
        console.log('deleting...');
        this.postInfo.postid = postid;
        this.fappService.RemovePostInfo(this.postInfo).subscribe(response => {
          if(response){
            const resp = response as PostResponse;
            if(response.key == "S" && response.status){ 
              _.remove(this.posts, function(e) {
                  return e === postid;
              });
              this.toastr.success(response.message);            
            }else{
              this.toastr.error(response.message);
            }
          }else{
            this.toastr.error("something gone wrong !");
          }
        });
      },
      () => {
        console.log('not deleting...');
      });
  }

  LoadpostInfo(){
    this.postInfo.mid = this.savedData.profileInfo.mid
    this.fappService.LoadMyPostIdInfo(this.postInfo).subscribe(response => {
      if(response){
        const resp = response as PostResponse;
        if(response.key == "S" && response.status){ 
          if(response.postInfo == null){
            this.showAlert=true;
            return;
          }
          if(response.postInfo)
            var postIds = response.postInfo.postids.split(",");
            for (let i = 0; i < postIds.length; i++) {
              this.posts.push(postIds[i]);
            }
        }else{
          this.toastr.error(response.message);
        }
      }else{
        this.toastr.error("something gone wrong !");
      }
    });
  }
  LoadAdditionalpostInfo(){
    this.noofBottomClicks += 1;
    this.postInfo.postid = this.noofBottomClicks.toString();
    this.postInfo.mid = this.savedData.profileInfo.mid
    this.fappService.LoadMyPostIdInfo(this.postInfo).subscribe(response => {
      if(response){
        const resp = response as PostResponse;
        this.fireAsycLoad = 'Y';
        if(response.key == "S" && response.status){
          if(response.postInfo == null){
            return;
          }
          if(response.postInfo)
            var postIds = response.postInfo.postids.split(",");
            for (let i = 0; i < postIds.length; i++) {
              this.posts.push(postIds[i]);
            }
        }else{
          this.toastr.error(response.message);
        }
      }else{
        this.toastr.error("something gone wrong !");
      }
    });
  }

  public handleScroll(event: NgxScrollEvent) {
    //console.log('scroll occurred', event.originalEvent);
    if (event.isReachingBottom) {
      console.log(`the user is reaching the bottom`);
      if(this.fireAsycLoad == 'Y'){
        this.LoadAdditionalpostInfo();
        this.fireAsycLoad = 'N';
      }
    }
    if (event.isReachingTop) {
      //console.log(`the user is reaching the top`);
    }
    if (event.isWindowEvent) {
      //console.log(`This event is fired on Window not on an element.`);
    }
 
  }


}
