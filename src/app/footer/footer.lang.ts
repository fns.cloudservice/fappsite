export class FooterInfoLang{
    public lang:any[];
    constructor(){        
        this.lang = new Array();
        var eng = new FooterInfoLangText();        
        eng.Contactus = "Contact us";
        eng.Termsofuse = "Terms of use";

        var spn = new FooterInfoLangText();
        spn.Contactus = "Contáctenos";
        spn.Termsofuse = "Términos de Uso";

        this.lang["english"] = eng;
        this.lang["spanish"] = spn;
    }
}

export class FooterInfoLangText{
    public Contactus: string;
    public Termsofuse: string;
}