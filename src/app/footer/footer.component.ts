import { Component, OnInit } from '@angular/core';
import {FooterInfoLang, FooterInfoLangText} from './footer.lang';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  public lang:FooterInfoLangText;
  constructor() { 
    if(localStorage.getItem("lang") == "spanish"){
    this.lang = new FooterInfoLang().lang["spanish"];
  }else{
    this.lang = new FooterInfoLang().lang["english"];
  }
}

  ngOnInit() {
  }

}
