import { Component, OnInit, ElementRef, ViewChild, ViewContainerRef, ComponentFactoryResolver, ComponentFactory, ComponentRef, Input } from '@angular/core';
import {AlertsComponent} from "../controls/alerts/alerts.component"; 
import { Router, NavigationEnd } from '@angular/router';
import { FAppService } from '../service/fapp.service';
import { SharedService } from '../service/shared.service';
import { ConfirmService } from '../service/confirm.service';
import { ProfileResponse } from '../model/ProfileResponse';
import { searchInfo } from '../model/search.model';
import { PostResponse } from '../model/PostResponse';
import { postInfo } from '../model/post.model';
import { friendInfo } from '../model/friend.model';
import { FriendResponse } from '../model/FriendResponse';
import { ToastrService } from 'ngx-toastr';
import {AuthService} from '../service/auth.service';
import { finalize, tap } from 'rxjs/operators';
import { from, Observable } from 'rxjs';
import * as _ from 'lodash';
import { SinglepostComponent } from '../controls/singlepost/singlepost.component';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  searchInfoData : searchInfo; searchText:string;  firstname : any; lastname : any; gender : any; location : any; language : any; PostId: string; 
  VP : any = ''; vPost : any = ''; aboutme : any; myinterest : any; searchIn : any; path : any; displaygender : any; currentPage:any; componentRef : any;
  savedData:ProfileResponse; friendInfo : friendInfo; totalRows : any; totalpages : any; pagearrObj : number[]; firstPage:any; lastPage:any; pagingclass:any;
  tempfirstPage:any; NoOfVisiblePages:any; firstPageReloadReq : string; lastPageReloadReq : string;

  //@ViewChild('ancSearch', { static: true }) ancSearch: ElementRef;
  @ViewChild('btnlaunch', { static: true }) btnlaunch: ElementRef;
  @ViewChild('btnpostlaunch', { static: true }) btnpostlaunch: ElementRef;
  @ViewChild("singlepostContainer", {static: true, read: ViewContainerRef }) container;

  constructor(private fappService:FAppService, private sharedService:SharedService, private router:Router, private toastr: ToastrService, 
    private auth: AuthService, private confirmService: ConfirmService, private resolver: ComponentFactoryResolver) { 
      this.searchIn = 'M'; this.NoOfVisiblePages = 3;
      this.searchInfoData = new  searchInfo();
      this.friendInfo = new friendInfo();
      this.savedData = JSON.parse(localStorage.getItem("profileInfo")) as ProfileResponse;
      this.sharedService.inSearchUrl.subscribe( value => {
        if(value){
          this.setSearchParams();
          this.SearchProfileInfo(1);
        }
      });
    }

  ngOnInit() {
    this.auth.isuserLoggedIn();
    this.setSearchParams();
    if(localStorage.getItem("searchText") !=null|| undefined){
      this.searchText= localStorage.getItem("searchText");
      this.UpdateSearchIn(localStorage.getItem("searchType"));
      this.SearchProfileInfo(1);
    }
  }

  private setSearchParams(){
    this.searchText= localStorage.getItem("searchText");
    this.UpdateSearchIn(localStorage.getItem("searchType"));
  }

  ngAfterContentChecked() {
    
  }
  
  public SetMemberInfo(index : any){
    this.firstname = this.VP[index].firstname;
    this.lastname = this.VP[index].lastname;
    this.gender = this.VP[index].gender;
    this.location = this.VP[index].location;
    this.language = this.VP[index].language;
    this.aboutme = this.VP[index].aboutme;
    this.myinterest = this.VP[index].myinterest;
    this.path = this.VP[index].path;

    if(this.gender == "M"){
      this.displaygender = 'Male';
    }else if(this.gender == "F"){
      this.displaygender = 'Female';
    }else {
      this.displaygender = 'Other';
    }
    let el: HTMLElement = this.btnlaunch.nativeElement;
    el.click();

    // let element: HTMLElement = document.getElementById('btnlaunch')[0] as HTMLElement;
    // element.click();
  }

  public UpdateSearchIn(searchIn : any){
    this.searchIn = searchIn;
  }

  public Getaboutme(aboutme : any) : any {
    if(String(aboutme).length <= 240)
      return aboutme;
    else{
      return String(aboutme).substr(0, 240) + '...';
    }
  }

  public SearchProfileInfo(frompage : any){
    //frompage = frompage -1;
    frompage=1;
    this.searchInfoData.searchText = this.searchText;
    this.searchInfoData.searchIn = this.searchIn;
    this.searchInfoData.mid = this.savedData.profileInfo.mid;
    this.searchInfoData.from = frompage - 1;
    this.currentPage = frompage;
    this.searchInfoData.size = 10;//(<HTMLSelectElement>document.getElementById('ddlSize')).value;

    this.fappService.SearchProfile(this.searchInfoData).subscribe(response => {
      if(response){
        if(response.status=="Success"){
          if(this.searchInfoData.searchIn == "P"){
            //this.vPost = response.vPost == null ? null : response.vPost.postDetails;
            this.VP = null;
            if(response.vPost == null){
              this.vPost = null;
              this.totalRows = 0;
            }else{
              this.vPost = response.vPost.postDetails;
              this.totalRows = response.vPost.postDetails[0].totalrows;
            }
          }else{
            this.vPost = null;
            if(response.vp == null){
              this.VP = null;
              this.totalRows = 0;
            }else{
              this.VP = response.vp.memberDetails;
              this.totalRows = response.vp.memberDetails[0].totalrows;
            }
          }
          this.ManagePaging(frompage);
         /* if(localStorage.getItem("searchText") !=null|| undefined){
            localStorage.removeItem("searchText");
          }*/
          //this.toastr.success(response.message);
          //this.router.navigateByUrl("profile");
        }else{
          this.toastr.error(response.message);
        }
      }else{
        this.toastr.error("something gone wrong !");
      }
    });
   
  }

  ManagePaging(cPage){
    this.pagearrObj = [];
    this.setPagingVariables(cPage);

    if(this.totalpages > 0){
      var J = 1;
      for(var i = this.firstPage; i <= this.totalpages && J <= this.NoOfVisiblePages; i++){
        this.pagearrObj.push(i);
        this.lastPage = i;
        J++;
      }
    }
  }

  setPagingVariables(currentPage : any){
    this.currentPage = currentPage;
      this.totalpages = Math.ceil(this.totalRows / this.searchInfoData.size);
      if(currentPage == 1){
        this.firstPage = 1; this.firstPageReloadReq = 'N'; 
        if(this.firstPage + this.NoOfVisiblePages < this.totalpages){
          this.lastPage = this.firstPage + this.NoOfVisiblePages;
          this.lastPageReloadReq = 'Y'
        }
        else{
          this.lastPage = this.totalpages; this.lastPageReloadReq = 'N';
        }
      }else if (currentPage == this.firstPage && this.firstPageReloadReq == 'Y'){
        this.firstPage = currentPage - (this.NoOfVisiblePages -1); 
        this.firstPageReloadReq = 'Y';
        if(this.firstPage + this.NoOfVisiblePages < this.totalpages){
          this.lastPage = this.firstPage + this.NoOfVisiblePages;
          this.lastPageReloadReq = 'Y'
        }
        else{
          this.lastPage = this.totalpages; this.lastPageReloadReq = 'N';
        }
      }else if (currentPage == this.lastPage && this.lastPageReloadReq == 'Y'){
        this.firstPage = currentPage; 
        this.firstPageReloadReq = 'Y';
        if(this.firstPage + this.NoOfVisiblePages < this.totalpages){
          this.lastPage = this.firstPage + this.NoOfVisiblePages;
          this.lastPageReloadReq = 'Y'
        }
        else{
          this.lastPage = this.totalpages; this.lastPageReloadReq = 'N';
        }
      }
      else if (currentPage == this.totalpages){
        this.firstPage = currentPage - (this.NoOfVisiblePages -1); 
        this.firstPageReloadReq = 'Y';
        this.lastPage = this.totalpages; this.lastPageReloadReq = 'N';
      }
  }

  GoToPrevNext(strDirection : string){
    if(strDirection == 'N'){
      if(this.currentPage == this.totalpages){
        return;
      }
      this.currentPage = this.currentPage + 1;
    }else{
      if(this.currentPage == 1){
        return;
      }
      this.currentPage = this.currentPage - 1;
    }
    this.GoToPage(this.currentPage);
  }

  GoToPage(page){
    //alert(page);
    this.currentPage = page;
    this.SearchProfileInfo(page);
  }

  GetPagingClass(page) : any {
    if(page == this.currentPage){
      this.pagingclass = 'active';
    }else{
      this.pagingclass = '';
    }
    return this.pagingclass;
  }

  GetPrevClass() : any {
    var prevClass = 'prev ';
    if(this.currentPage == 1){
      prevClass = prevClass + 'disabled';
    }
    return prevClass;
  }

  GetNextClass() : any {
    var nextClass = 'next ';
    if(this.currentPage == this.totalpages){
      nextClass = nextClass + 'disabled';
    }
    return nextClass;
  }

  OnManageRequest(mid : string, rmid : string, displayName : string, action : string){
    var title = 'Confirm friend request';
    var msg = 'Do you really want to send friend request to ' + displayName;
    
    this.confirmService.confirm({ title:title, message: msg + '?' }).then(
      () => {
        this.friendInfo.mid = mid; this.friendInfo.rmid = rmid; 
        this.friendInfo.action = action; this.friendInfo.since = Date.now().toString();
        this.fappService.ManageRequestFromElasticInfo(this.friendInfo).subscribe(response => {
          if(response){
            const resp = response as FriendResponse;
            if(response.key == "S"){ 
              this.toastr.success(response.message);
              //this.LoadGeneralSearchList();
            }else{
              this.toastr.error(response.message);
            }
          }else{
            this.toastr.error("something gone wrong !");
          }
        });
      },
      () => {
        //console.log('not deleting...');
      });
    
  }

  public ShowPostInfo(index : any){
    this.PostId = this.vPost[index].postid;
    let el: HTMLElement = document.getElementById("ancSearch");
    el.setAttribute("href", "#/post/" +this.PostId); 
    el.click();

    //window.open('#/post/'+this.PostId)
    
   /* this.container.clear(); 
    const factory = this.resolver.resolveComponentFactory(SinglepostComponent);
    this.componentRef = this.container.createComponent(factory);
    this.componentRef.instance.PostId = this.PostId;
    this.componentRef.instance.Seqn = this.vPost[index].seqn;
    this.componentRef.changeDetectorRef.detectChanges();

    let el: HTMLElement = this.btnpostlaunch.nativeElement;
    el.click();*/

  }


}
