import { Component, OnInit, Input, ChangeDetectorRef, ElementRef, ViewChild, TemplateRef  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { FAppService } from '../../service/fapp.service';
import {SharedService} from '../../service/shared.service';
import { PostResponse } from '../../model/PostResponse';
import { postInfo } from '../../model/post.model';
import { aboutInfo } from '../../model/about.model';
import { ToastrService } from 'ngx-toastr';
import {AuthService} from '../../service/auth.service';
import * as moment from 'moment';

@Component({
  selector: 'singlepost',
  templateUrl: './singlepost.component.html',
  styleUrls: ['./singlepost.component.scss']
})
export class SinglepostComponent implements OnInit {
  @Input() PostId: string; @Input() PostIdIndex: string; @Input() Parent: any; postInfo : postInfo; posthtmlToAdd:string; strpostdt:string; @Input() Seqn: any;
  registerForm: FormGroup; submitted = false; viewmode: boolean = true; btnText: string = "Edit"; faclass: string = "fa fa-pencil"; divClass : string;
  
  @ViewChild('blogdesc', { static: true }) blogdesc: ElementRef; 
  @ViewChild('btndescsave', { static: true }) btndescsave: ElementRef; 
  @ViewChild('divcomment', { static: true }) divcomment: ElementRef; 

  constructor(private fappService:FAppService, private sharedService:SharedService, private router:Router, private toastr: ToastrService, private auth: AuthService,
    private modalService: BsModalService,private storage: AngularFireStorage, private db: AngularFirestore, private formBuilder: FormBuilder) { 
    this.postInfo = new postInfo();
    this.divClass = 'blog-comments hide';
    //console.log("in spost postid is : " + this.PostId + " file : " + this.file);
    
  }

  ngOnInit() {
    this.auth.isuserLoggedIn();
    //this.Loadpreview();
    this.postInfo.postid = this.PostId;
    if(this.postInfo.postid === undefined){}else{ this.LoadpostInfo(); }

    this.registerForm = this.formBuilder.group({
      description: [this.posthtmlToAdd, Validators.required]
    });
  }

  get f() { return this.registerForm.controls; }

  onReset() {
      this.submitted = false;
      this.registerForm.reset();
  }

  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.registerForm.invalid) {
          return;
      }
      this.postInfo.description = this.registerForm.value.description;
      console.log(this.postInfo.description);
      this.posthtmlToAdd = this.postInfo.description;
      this.SavePostDescInfo();
      // display form values on success
      //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));
  }

  ngAfterContentChecked() {
    
  }

  LoadpostInfo(){
    this.fappService.LoadPostInfo(this.postInfo).subscribe(response => {
      if(response){
        const resp = response as PostResponse;
        if(response.key == "S" && response.status){ 
          if(response.postInfo)
            this.posthtmlToAdd = response.postInfo == null ? "" : response.postInfo.description;
            
            var medias = response.postInfo.mediaJson.split("#EMR#");

            for (let i = 0; i < medias.length; i++) {
              // this.galleryImages.push({
              //       small: medias[i].split("#,#")[4],
              //       medium: medias[i].split("#,#")[4],
              //       big: medias[i].split("#,#")[4]
              //   });
            }
            this.strpostdt = moment.unix(response.postInfo.posttime/1000).format("DD MMM YYYY hh:mm a");
            //console.log("posttime : " + response.postInfo.posttime + "date : " + this.strpostdt);
        }else{
          this.toastr.error(response.message);
        }
      }else{
        this.toastr.error("something gone wrong !");
      }
    });
  }

  SavePostDescInfo(){
    this.fappService.SavePostDescInfo(this.postInfo).subscribe(response => {
      if(response){
        const resp = response as PostResponse;
        if(response.key == "S" && response.status){ 
            this.toastr.success(response.message);
        }else{
          this.toastr.error(response.message);
        }
      }else{
        this.toastr.error("something gone wrong !");
      }
    });
  }
  
  toggleComment(){
    // let el: HTMLElement = this.divcomment.nativeElement;
    // el.click();
    if(this.divClass.indexOf('hide') >= 0){
      this.divClass = 'blog-comments';
    }else{
      this.divClass = 'blog-comments hide';
    } 
  }

}

