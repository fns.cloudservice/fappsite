import { Component, OnInit, TemplateRef, Output, EventEmitter , ViewChild,ElementRef} from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.scss']
})
export class AlertsComponent implements OnInit {

  @ViewChild('template', { static: true }) mymodal: ElementRef;
  
  modalRef: BsModalRef; alertTitle:string = "Alert"; alertMsg:string;

  constructor(private modalService: BsModalService) { }

  ngOnInit() {
  }
  
  openModal(title? : string, msg? : string) {
    if(title != null && title!=""){
      this.alertTitle = title;
    }
    if(msg != null && msg!=""){
      this.alertMsg = msg;
    }

    this.modalRef = this.modalService.show(this.mymodal);
  }

}
