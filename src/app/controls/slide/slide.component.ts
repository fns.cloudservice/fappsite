import { Component, OnInit, Input, ChangeDetectorRef, ElementRef, ViewChild, TemplateRef  } from '@angular/core';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { FAppService } from '../../service/fapp.service';
import {SharedService} from '../../service/shared.service';
import { PostResponse } from '../../model/PostResponse';
import { postInfo } from '../../model/post.model';
import { aboutInfo } from '../../model/about.model';
import { ToastrService } from 'ngx-toastr';
import {AuthService} from '../../service/auth.service';
import { ProfileResponse } from '../../model/ProfileResponse';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap'; 
import {AlertsComponent} from "../../controls/alerts/alerts.component"; 
import * as moment from 'moment';
import lozad from 'lozad'

declare function fnInitializeCarousel(elemId):any;
declare function fnPrevNextSlideCarousel(elemId, dir):any;

@Component({
  selector: 'slide',
  templateUrl: './slide.component.html',
  styleUrls: ['./slide.component.scss']
})
export class SlideComponent implements OnInit {
  @Input() PostId: string; @Input() PostIdIndex: string; postInfo : postInfo; SliderhtmlToAdd:string; sliderImages: string [] = []; sliderDesc: string [] = []; activeItem: number = 0;
  datatarget:string; ModaldivId:string; mdlSampleIsOpen : boolean = false; dummyImgURL:string = '../assets/img/dummy.png'; sliderPreviewImages: string [] = []; sliderList = [];
  sliderImagesOrg: string [] = []; sliderSeq: string [] = []; curComment : any; savedData:ProfileResponse; icmtCount = 0; commentList = []; curcommentList = []; likesList = [];
  likes : any;showComment:boolean=true; displayName:string; strpostdt:string; profilePath:string; userdefaultImgPath:string="../assets/layouts/layout4/img/usr.jpg";

  @ViewChild(AlertsComponent, { static: true }) alert: AlertsComponent;
  @ViewChild('carousl', { static: true }) carousl: ElementRef; 
  @ViewChild('dz', { static: true }) dz: ElementRef; 
  @ViewChild('cta', { static: true }) cta: ElementRef;
  
  constructor(private fappService:FAppService, private sharedService:SharedService, private router:Router, private toastr: ToastrService, private auth: AuthService,
    private modalService: BsModalService,private storage: AngularFireStorage, private db: AngularFirestore) {
    this.savedData = JSON.parse(localStorage.getItem("profileInfo")) as ProfileResponse;
    this.sharedService.cmtMessage.subscribe( value => { 
      if(value != ''){
        var strinput = value.split('#');
        if(this.PostId == strinput[3]){
          this.commentList.splice(0, 0, { cmt: strinput[0], cmttime: strinput[1], dsnm: strinput[4], imgpath: strinput[5], mid: strinput[2], pid: strinput[3], seqn: strinput[6],tCommentCount: strinput[7] });
          this.setCommentInfo();    
        }
      }
    });
    this.sharedService.ImgLikes.subscribe( value => { 
      if(value != ''){
        var strinput = value.split('#');
        if(strinput[0] == this.GetImgSeqn())
        this.likes = strinput[1];
        for(let item of this.likesList){
          if(item.seqn == this.GetImgSeqn()){
            item.likes = strinput[1];
          }
        }
      }
    });
    this.postInfo = new postInfo();
  }

  StartObserveForLazyLoad(){
    const observer = lozad(); // lazy loads elements with default selector as '.lozad'
    observer.observe();
  }

  InitialCode(){
    this.postInfo.postid = this.PostId;
    this.datatarget = "#" + this.PostId + "_Modal";
    this.ModaldivId = this.PostId + "_Modal";
    if(this.postInfo.postid === undefined){}else{ this.LoadpostInfo(); }
    //fnInitializeCarousel(this.PostId);
  }

  ngOnInit() {
    this.InitialCode();
    localStorage.setItem("PrevSeq",'0');
  }
  
  private openModal(open : boolean) : void {
    this.sliderImages = this.sliderImagesOrg.concat();
    this.mdlSampleIsOpen = open;
    this.setCommentInfo();
    if(this.curcommentList.length ==this.curcommentList[0].tCommentCount){
      this.showComment=true;
    }else{
      this.showComment=false;
    }
  }

  LoadpostInfo(){
    this.commentList = []; this.curcommentList = [];
    this.fappService.LoadPostInfo(this.postInfo).subscribe(response => {
      if(response){
        const resp = response as PostResponse;
        if(response.key == "S" && response.status){ 
          if(response.dt != undefined && response.dt.length > 0){
            if(response.dt.length === 0){
              this.showComment =true;
            }
            else{
              this.showComment =false;
            }
            this.icmtCount = response.dt.length;
            for(let item of response.dt){
              this.commentList.push(item); 
            }
          }
          if(response.dtlikes != undefined && response.dtlikes.length > 0){
            //this.icmtCount = response.dt.length;
            for(let item of response.dtlikes){
              this.likesList.push(item); 
            }
          }
          if(response.postInfo)
            var medias = response.postInfo.mediaJson.split("#EMR#");
            if(medias === undefined){
              return;
            }
            for (let i = 0; i < medias.length; i++) {
              if(i < 5){
                this.sliderPreviewImages.push(medias[i].split("#,#")[4]);
              }
              //this.sliderImages.push(medias[i].split("#,#")[4]);
              this.sliderImagesOrg.push(medias[i].split("#,#")[4]);
              this.sliderDesc.push(medias[i].split("#,#")[3]);
              this.sliderSeq.push(medias[i].split("#,#")[0]);

              let req = JSON.stringify({sno : i, seq : medias[i].split("#,#")[0] , imgurl : medias[i].split("#,#")[4], imgdesc : medias[i].split("#,#")[3], postid : this.PostId });
              this.sliderList.push(req); 

              this.displayName = response.postInfo.displayName;
              this.profilePath = response.postInfo.profilePath;
              this.strpostdt = moment.unix(response.postInfo.posttime/1000).format("DD MMM YYYY hh:mm a");

              this.StartObserveForLazyLoad();
            }
            //console.log(this.sliderList);
        }else{
          this.toastr.error(response.message);
        }
      }else{
        this.toastr.error("something gone wrong !");
      }
    });
  }

  public getprofilePath(profilePath){
    if(profilePath == null){
      return this.userdefaultImgPath;
    }else{
      return profilePath;
    }
  }

  private filterListBySno(key) {
    if(this.sliderList.length > 0){
      if(key == 'F'){
        // console.log(this.sliderList.filter(x => 
        //   { 
        //     console.log(JSON.parse(x));
        //     console.log(JSON.parse(x).seq);
        //     JSON.parse(x).seq < 3 
        //   }));
        return this.sliderList.filter(x => JSON.parse(x).seq < 3);
      }else if(key == 'S'){
        return this.sliderList.filter(x => JSON.parse(x).seq > 2 && JSON.parse(x).seq < 6);
      }
    }
    // else if(key == 'S'){
    //   return this.sliderList.slice(0, 5);//filter(x => x.sno < 6);
    // }
    // else if(key == 'F'){
    //   return this.sliderList.slice(0, 5);//this.FavList.filter(x => x.sno < 6);
    // }
  }

  SlideClick(elemId : string, dir : any){
  
 

    if(this.activeItem == this.sliderImages.length - 1 && dir == "N")
        this.activeItem = -1;

    if(dir == "P"){
      if(this.activeItem == 0){
        this.activeItem = this.activeItem + (this.sliderImages.length - 1);
      }
      else{
        this.activeItem--;
      }
    }
    else if(dir == "N"){
      this.activeItem++;
    }
    
    this.setCommentInfo();
    if(this.curcommentList.length>0){ 
      if(this.curcommentList.length ==this.curcommentList[0].tCommentCount){
        this.showComment=true;
      }else{
        this.showComment=false;
      }
    }
   

  }

  IsActiveItem(index: number) : string {
      return index == this.activeItem ? "active": "";
  }

  GetIsShowOrHideImage(index: number) : any {
    let iImgCount:any;
      
      iImgCount = parseInt(this.GetTotalImage());

      if(this.sliderPreviewImages.length > 0){
        if(index < iImgCount)
          return true;
        else
          return false;
      }
      
  }

  GetSRCImageURl(item: any) : string {
    
    item = JSON.parse(item);
    let ImgURL: string;

    ImgURL = item.imgurl;
    // if(this.PostIdIndex == '0'){
    //   ImgURL = item.imgurl;
    // }
    // else{
    //   ImgURL = "../assets/img/dummy.png";
    // }

    return ImgURL;
  }

  GetImageURl(item: any) : string {
    item = JSON.parse(item);

    let ImgURL: string;
    ImgURL = item.imgurl;
    //console.log(this.sliderPreviewImages);
    // if(this.sliderPreviewImages.length > 0){
    //   if(index <= this.sliderPreviewImages.length - 1)
    //     ImgURL = this.sliderPreviewImages[index];
    //   else
    //     ImgURL = this.sliderPreviewImages[0];
    // }
    // else{
    //   ImgURL = "../assets/img/dummy.png";
    // }

    return ImgURL;
  }

  GetImage(index: number) : string {
    if(this.sliderPreviewImages[0] === undefined){
      return;
    }
    let ImgURL: string;
    //console.log(this.sliderPreviewImages);
    if(this.sliderPreviewImages.length > 0){
      if(index <= this.sliderPreviewImages.length - 1)
        ImgURL = this.sliderPreviewImages[index];
      else
        ImgURL = this.sliderPreviewImages[0];
    }
    else{
      ImgURL = "../assets/img/dummy.png";
    }

    return ImgURL;
  }

  GetTotalImage() : string {
      return "+" + this.sliderImagesOrg.length.toString();
  }

  GetImgDescription() : string {
      let slidedesc: string;

      slidedesc = this.sliderDesc[this.activeItem];
      //this.setCommentInfo();
      
      return slidedesc;
  }

  setCommentInfo(){
    var x = this.GetImgSeqn();
    //console.log("set Comment ",x, this.curcommentList)
    this.curcommentList = [];
    for(let item of this.commentList){
      if(item.seqn == this.GetImgSeqn()){
        
        this.curcommentList.push(item);
        this.icmtCount = this.curcommentList[0].tCommentCount;
     
      }
     if ((item.seqn !=this.GetImgSeqn())&&this.curcommentList.length===0){
       this.icmtCount=0
       this.showComment=true;
     }
    
      
    }
/*    if(this.icmtCount>2){
      this.showComment=false;
    }else{
      this.showComment=true;
    }*/
   // this.icmtCount = this.curcommentList.length;
    this.setLikesInfo();
  }

  GetImgSeqn() : string {
      let slideseqn: string;

      slideseqn = this.sliderSeq[this.activeItem];
      slideseqn = slideseqn == '' ? '1' : slideseqn;
      return slideseqn;
  }

  LoadMoreComment(){
  var x= parseInt( this.GetImgSeqn())
  var arr=[]
  for(var i=0;i<=this.sliderSeq.length-1;i++){
  arr[i]=2;

  localStorage.setItem("PostId",this.curcommentList[0].pid)
  if(x===(i+1)){
  // console.log("get preve ",localStorage.getItem("PrevArr"), localStorage.getItem("PrevSeq"),localStorage.getItem("PostId") )

  if(localStorage.getItem("PrevSeq")==x.toString()||localStorage.getItem("PrevSeq")===null||undefined && localStorage.getItem("PostId")===this.curcommentList[0].pid)
  arr[x-1]+=parseInt(localStorage.getItem("PrevArr"))
  localStorage.setItem("PrevArr", JSON.stringify(arr[x-1]));
  localStorage.setItem("PrevSeq",x.toString())
  this.postInfo.connectionid=this.GetImgSeqn()

  this.postInfo.postids = arr[x-1]
  console.log('load more ', this.postInfo,this.postInfo.postids,this.postInfo.mid)

  this.fappService.LoadPostInfo(this.postInfo).subscribe(response => {
    if(response){
      const resp = response as PostResponse;
      if(response.key == "S" && response.status){ 
        
         
          if(response.dt != undefined && response.dt.length > 0){
           // this.icmtCount = response.dt.length;
          
            //this.commentList = response.dt.splice(0,1* parseInt(this.postInfo.postids),...this.commentList) 
            this.commentList = this.commentList.concat(response.dt.splice(1*parseInt(this.postInfo.postids),1+parseInt(this.postInfo.postids)))//..concat(this.commentList)
            console.log(this.commentList)
            
           this.setCommentInfo();
           
            
          }

         
          var y = parseInt(response.dt[0].tCommentCount);
          if(this.curcommentList.length === y){
            this.showComment =true;
          }
          else{
            this.showComment =false;
          }
          
          
      }
      else{
        this.toastr.error(response.message);
      }
    }else{
      this.toastr.error("something gone wrong !");
    }
  });
  

  
  

 }
  


}

    
}


  public GetCommentTime(cmttime) : any {
      let commentTime: string;

      commentTime = moment.unix(cmttime/1000).format("DD MMM YYYY hh:mm a");

      return commentTime;
  }

  OnSelectFiles(){
    let el: HTMLElement = this.cta.nativeElement;
    el.click();
  }

  SaveComment(){
    if(this.curComment === undefined || this.curComment == ''){
      return;
    }
    let curCommentObj = { mid: this.savedData.profileInfo.mid, postId : this.postInfo.postid, slideSeqn:this.GetImgSeqn(), comment:this.curComment, commenttime: Date.now().toString()  };
    
    this.fappService.SavePostCommentInfo(curCommentObj).subscribe(response => {
      if(response){
        const resp = response as PostResponse;
        if(response.key == "S" && response.status){ 
            this.toastr.success(response.message);
            this.curComment = '';
            this.icmtCount=response.dt[0].tCommentCount;

        }else{
          this.toastr.error(response.message);
        }
      }else{
        this.toastr.error("something gone wrong !");
      }
    });
  }
  
  public SaveLikes(){
    let curCommentObj = { mid: this.savedData.profileInfo.mid, postId : this.postInfo.postid, slideSeqn: this.GetImgSeqn(), comment:'L', commenttime: Date.now().toString()  };
  
    this.fappService.SaveLikesInfo(curCommentObj).subscribe(response => {
      if(response){
        const resp = response as PostResponse;
        if(response.key == "S" && response.status){ 
          this.likesList = [];
          if(response.dtlikes != undefined && response.dtlikes.length > 0){
            for(let item of response.dtlikes){
              this.likesList.push(item); 
            }
          }
          this.likes = response.message;
            //this.toastr.success(response.message);
        }else{
          this.toastr.error(response.message);
        }
      }else{
        this.toastr.error("something gone wrong !");
      }
    });
  }

  setLikesInfo(){
    this.likes = 0;
    for(let item of this.likesList){
      if(item.seqn == this.GetImgSeqn()){
        this.likes = item.likes;
      }
    }
  }

}
