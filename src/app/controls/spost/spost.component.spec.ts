import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { spostComponent } from './spost.component';

describe('spostComponent', () => {
  let component: spostComponent;
  let fixture: ComponentFixture<spostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ spostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(spostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
