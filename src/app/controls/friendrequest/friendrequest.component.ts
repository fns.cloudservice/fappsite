import { Component, OnInit, Input, ChangeDetectorRef, ElementRef, ViewChild, TemplateRef  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { FAppService } from '../../service/fapp.service';
import {SharedService} from '../../service/shared.service';
import { PostResponse } from '../../model/PostResponse';
import { ProfileResponse } from '../../model/ProfileResponse';
import { FriendResponse } from '../../model/FriendResponse';
import { postInfo } from '../../model/post.model';
import { aboutInfo } from '../../model/about.model';
import { friendInfo } from '../../model/friend.model';
import { ToastrService } from 'ngx-toastr';
import {AuthService} from '../../service/auth.service';
import * as moment from 'moment';

declare function fnManagePopOver(mid, displayName):any;

@Component({
  selector: 'friendrequest',
  templateUrl: './friendrequest.component.html',
  styleUrls: ['./friendrequest.component.scss']
})
export class FriendrequestComponent implements OnInit {

  @Input('MemberId') MemberId: string; @Input('MIdIndex') MIdIndex: string; @Input('displayName') displayName: string; @Input('imgPath') imgPath: string; 
  @Input('location') location: string; @Input('friendRequestCount') friendRequestCount: string; @Input() Parent: any; 
  @Input() email: string; savedData:ProfileResponse; friendInfo : friendInfo;

    @ViewChild('template', { static: true }) mymodal: ElementRef;
    
    modalRef: BsModalRef; alertTitle:string = "Alert"; alertMsg:string;
  
    constructor(private fappService:FAppService, private sharedService:SharedService, private router:Router, private toastr: ToastrService, private auth: AuthService,
        private modalService: BsModalService,private storage: AngularFireStorage, private db: AngularFirestore, private formBuilder: FormBuilder) {
            this.savedData = JSON.parse(localStorage.getItem("profileInfo")) as ProfileResponse;
            this.friendInfo = new friendInfo();
         }
  
    ngOnInit() {
        
    }
    
    OnPopOver(){
      //console.log(this.MemberId);
      fnManagePopOver(this.MemberId, this.displayName);
    }

    OnManageRequest(action : string){
      this.friendInfo.mid = this.savedData.profileInfo.mid; this.friendInfo.rmid = this.MemberId; this.friendInfo.action = action;
      this.fappService.ManageRequestInfo(this.friendInfo).subscribe(response => {
        if(response){
          const resp = response as FriendResponse;
          if(response.key == "S"){ 
            this.toastr.success(response.message);
          }else{
            this.toastr.error(response.message);
          }
        }else{
          this.toastr.error("something gone wrong !");
        }
      });
    }

}
