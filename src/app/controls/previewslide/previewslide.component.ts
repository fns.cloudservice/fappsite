import { Component, OnInit, Input, ChangeDetectorRef, ElementRef, ViewChild, TemplateRef  } from '@angular/core';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { FAppService } from '../../service/fapp.service';
import {SharedService} from '../../service/shared.service';
import { PostResponse } from '../../model/PostResponse';
import { postInfo } from '../../model/post.model';
import { newpostInfo } from '../../model/newpost.model';
import { aboutInfo } from '../../model/about.model';
import { ToastrService } from 'ngx-toastr';
import {AuthService} from '../../service/auth.service';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap'; 
import {AlertsComponent} from "../../controls/alerts/alerts.component"; 

@Component({
  selector: 'previewslide',
  templateUrl: './previewslide.component.html',
  styleUrls: ['./previewslide.component.scss']
})
export class PreviewSlideComponent implements OnInit {
  @Input() file: File; ImgDesc:string; imgURL: any; 
  public errormsg: string;
  public fName: string;
  modalRef: BsModalRef;

  constructor(private fappService:FAppService, private sharedService:SharedService, private router:Router, private toastr: ToastrService, 
    private auth: AuthService) { }

  ngOnInit() {
    this.auth.isuserLoggedIn();
    this.startDisplay();
  }

  ngAfterContentChecked() {
    
  }
  
  startDisplay() {
    var mimeType = this.file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.errormsg = "Only images are supported.";
      //return;
    }

    var reader = new FileReader();
    this.fName = this.file.name;
    reader.readAsDataURL(this.file); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
  }

}
