import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PreviewSlideComponent } from './previewslide.component';

describe('PreviewSlideComponent', () => {
  let component: PreviewSlideComponent;
  let fixture: ComponentFixture<PreviewSlideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviewSlideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewSlideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
