import { Component, OnInit, Input, ChangeDetectorRef, ElementRef, ViewChild, TemplateRef  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { FAppService } from '../../service/fapp.service';
import {SharedService} from '../../service/shared.service';
import { ProfileResponse } from '../../model/ProfileResponse';
import { PostResponse } from '../../model/PostResponse';
import { postInfo } from '../../model/post.model';
import { aboutInfo } from '../../model/about.model';
import { ToastrService } from 'ngx-toastr';
import {AuthService} from '../../service/auth.service';
import * as moment from 'moment';

@Component({
  selector: 'tspost',
  templateUrl: './tspost.component.html',
  styleUrls: ['./tspost.component.scss']
})
export class TSPostComponent implements OnInit {
  @Input() PostId: string; @Input() PostIdIndex: string; @Input() Parent: any; postInfo : postInfo; posthtmlToAdd:string; strpostdt:string;
  registerForm: FormGroup; submitted = false; viewmode: boolean = true; btnText: string = "Edit"; faclass: string = "fa fa-pencil";
  savedData:ProfileResponse; curComment : any; icmtCount = 0; commentList = []; curcommentList = []; likes: any; displayName:string;profilePath:string;
  noofseeMoreClicks : number;postmid:string; userdefaultImgPath:string="../assets/layouts/layout4/img/usr.jpg";
  displayComment:boolean=true;showComment:boolean=true;
  isfollow:boolean=true;




  @ViewChild('blogdesc', { static: true }) blogdesc: ElementRef; 
  @ViewChild('btndescsave', { static: true }) btndescsave: ElementRef; 

  constructor(private fappService:FAppService, private sharedService:SharedService, private router:Router, private toastr: ToastrService, private auth: AuthService,
    private modalService: BsModalService,private storage: AngularFireStorage, private db: AngularFirestore, private formBuilder: FormBuilder) { 
      this.savedData = JSON.parse(localStorage.getItem("profileInfo")) as ProfileResponse;
     
      this.postInfo = new postInfo();
      this.noofseeMoreClicks=0;
      localStorage.setItem("mid",this.savedData.profileInfo.mid)
      //console.log("in spost postid is : " + this.PostId + " file : " + this.file);
      this.sharedService.cmtMessage.subscribe( value => { 
        if(value != ''){
          var strinput = value.split('#');
          if(this.PostId == strinput[3]){
            this.commentList.splice(0, 0, { cmt: strinput[0], cmttime: strinput[1], dsnm: strinput[4], imgpath: strinput[5], mid: strinput[2], pid: strinput[3], seqn: strinput[6] });
            this.setPostCommentInfo();
            this.icmtCount= parseInt(strinput[7])
          }
         
        }
      });
      this.sharedService.Likes.subscribe( value => { 
        if(value != ''){
          var strinput = value.split('#');
          if(strinput[0] == "0")
          this.likes = strinput[1];
        }
      });
  }

  ngOnInit() {
    this.auth.isuserLoggedIn();
    this.sharedService.FollowUnFollow.subscribe( value => { 
      if(value != ""){
        var strinput = value.split(',');
        if(strinput[0] == this.postmid && value.indexOf(',U') > 0){
          this.isfollow = false; 
        }else if(strinput[0] == this.postmid && value.indexOf(',F') > 0){
          this.isfollow = true; 
        }
      }
      
    });
    this.commentList = []; this.curcommentList = []; this.icmtCount = 0;
    //this.Loadpreview();
    this.postInfo.postid = this.PostId;
    if(this.postInfo.postid === undefined){}else{ this.LoadpostInfo(); }

    this.registerForm = this.formBuilder.group({
      description: [this.posthtmlToAdd, Validators.required]
    });
  }

  get f() { return this.registerForm.controls; }

  onReset() {
      this.submitted = false;
      this.registerForm.reset();
  }

  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.registerForm.invalid) {
          return;
      }
      this.postInfo.description = this.registerForm.value.description;
      console.log(this.postInfo.description);
      this.posthtmlToAdd = this.postInfo.description;
      this.SavePostDescInfo();
      // display form values on success
      //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));
  }

  ngAfterContentChecked() {
    
  }

  LoadpostInfo(){

    this.postInfo.mid=this.savedData.profileInfo.mid;//passing cur user mid to check follo info
    this.fappService.LoadPostInfo(this.postInfo).subscribe(response => {
     
      if(response){

        //console.log(response,"response")
        const resp = response as PostResponse;
        if(response.key == "S" && response.status){ 
          if(response.dt != undefined && response.dt.length > 0 && response.dt[0].seqn ===0){
            if(response.dt[0].tCommentCount!= undefined ){
              this.icmtCount = response.dt[0].tCommentCount;
              this.displayComment=false;
            }
            else{
              this.icmtCount =0;
              this.displayComment=true;
            }
            
            if(this.icmtCount <3){
              this.showComment=false;
            }
            else{
              this.showComment=true;
            }

            for(let item of response.dt){
              this.commentList.push(item); 
            }
          }
          if(response.fInfo==="follow"){
            this.isfollow=false;
          }else{
            this.isfollow=true;
          }
          if(response.postInfo)
            this.posthtmlToAdd = response.postInfo == null ? "" : response.postInfo.description;
            this.likes = response.postInfo == null ? "0" : response.postInfo.likes; 

            if(response.postInfo === undefined || response.postInfo == null){
              return;
            }

            var medias = response.postInfo.mediaJson.split("#EMR#");

            for (let i = 0; i < medias.length; i++) {
              // this.galleryImages.push({
              //       small: medias[i].split("#,#")[4],
              //       medium: medias[i].split("#,#")[4],
              //       big: medias[i].split("#,#")[4]
              //   });
            }
            this.postmid = response.postInfo.mid;
            this.displayName = response.postInfo.displayName;
            this.profilePath = response.postInfo.profilePath == null ? this.userdefaultImgPath : response.postInfo.profilePath;
            this.strpostdt = moment.unix(response.postInfo.posttime/1000).format("DD MMM YYYY hh:mm a");
            //console.log("posttime : " + response.postInfo.posttime + "date : " + this.strpostdt);
            this.setPostCommentInfo();
        }else{
          this.toastr.error(response.message);
        }
      }else{
        this.toastr.error("something gone wrong !");
      }
    });
  }

  public getprofilePath(profilePath){
    if(profilePath == null){
      return this.userdefaultImgPath;
    }else{
      return profilePath;
    }
  }

  setPostCommentInfo(){
    this.curcommentList = [];
    for(let item of this.commentList){
      if(item.seqn == 0){
        this.curcommentList.push(item);
        this.icmtCount= item.tCommentCount
      }
    }
   // this.icmtCount = this.curcommentList.length;
  }

  SavePostDescInfo(){
    this.fappService.SavePostDescInfo(this.postInfo).subscribe(response => {
      if(response){
        const resp = response as PostResponse;
        if(response.key == "S" && response.status){ 
            this.toastr.success(response.message);
        }else{
          this.toastr.error(response.message);
        }
      }else{
        this.toastr.error("something gone wrong !");
      }
    });
  }
  
  SaveComment(){
    if(this.curComment === undefined || this.curComment == ''){
      return;
    }
    let curCommentObj = { mid: this.savedData.profileInfo.mid, postId : this.postInfo.postid, slideSeqn: 0, comment:this.curComment, commenttime: Date.now().toString()  };
    
    this.fappService.SavePostCommentInfo(curCommentObj).subscribe(response => {
      if(response){
        const resp = response as PostResponse;
        if(response.key == "S" && response.status){ 
            console.log(response)
            this.toastr.success(response.message);
            this.curComment = '';
            this.icmtCount=response.dt[0].tCommentCount;
        }else{
          this.toastr.error(response.message);
        }
      }else{
        this.toastr.error("something gone wrong !");
      }
    });

  }
  
  public GetCommentTime(cmttime) : any {
    let commentTime: string;

    commentTime = moment.unix(cmttime/1000).format("DD MMM YYYY hh:mm a");

    return commentTime;
}

public SaveLikes(){
  let curCommentObj = { mid: this.savedData.profileInfo.mid, postId : this.postInfo.postid, slideSeqn: 0, comment:'L', commenttime: Date.now().toString()  };

  this.fappService.SaveLikesInfo(curCommentObj).subscribe(response => {
    if(response){
      const resp = response as PostResponse;
      if(response.key == "S" && response.status){ 
        this.likes = response.message;
          //this.toastr.success(response.message);
      }else{
        this.toastr.error(response.message);
      }
    }else{
      this.toastr.error("something gone wrong !");
    }
  });
}

LoadMoreComment(){
  //console.log('Reached LoadMore comment',this.postInfo);
  this.postInfo.connectionid= '0';
  this.noofseeMoreClicks +=2;
  this.postInfo.postids = this.noofseeMoreClicks.toString()
  console.log('load more ', this.curcommentList[0].seqn)

  this.fappService.LoadPostInfo(this.postInfo).subscribe(response => {
    if(response){
      const resp = response as PostResponse;
      if(response.key == "S" && response.status){ 
          var x = parseInt(response.dt[0].tCommentCount);
          if(response.dt.length === x){
            this.showComment=false;
          }
          else{
            this.showComment=true;
          }
          console.log("HTTP Response", response)
          if(response.dt != undefined && response.dt.length > 0){
           // this.icmtCount = response.dt.length;
            this.curcommentList=response.dt
          }
      }
      else{
        this.toastr.error(response.message);
      }
    }else{
      this.toastr.error("something gone wrong !");
    }
  });

  
}
follow(elem:string){
  let followInfoObj = { mid: this.savedData.profileInfo.mid, fmid : this.postmid, since: Date.now().toString() ,unfollow:elem};
  this.fappService.follow(followInfoObj).subscribe(response => {
    if(response){
      console.log(response)
      const resp = response as PostResponse;
      if(response.status ==='Success'){ 
        if(response.notifymsg ==="follow"){
          this.isfollow=false;
          this.sharedService.FollowUnFollow.next(this.postmid + ',U');
        }else{
          this.isfollow=true;
          this.sharedService.FollowUnFollow.next(this.postmid + ',F');
        }
          this.toastr.success(response.message);
      }else{
        this.toastr.error(response.message);
      }
    }else{
      this.toastr.error("something gone wrong !");
    }
  });

}
SharePost(){
 
  let url ='';
  url ='localhost:4200/#/post/'+this.postInfo.postid; 
  let selBox = document.createElement('textarea');
  selBox.style.position = 'fixed';
  selBox.style.left = '0';
  selBox.style.top = '0';
  selBox.style.opacity = '0';
  selBox.value = url;
  document.body.appendChild(selBox);
  selBox.focus();
  selBox.select();
  document.execCommand('copy');
  document.body.removeChild(selBox);
  this.toastr.success("Post url copied")
}

}
