
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TSPostComponent } from './tspost.component';

describe('TSPostComponent', () => {
  let component: TSPostComponent;
  let fixture: ComponentFixture<TSPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TSPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TSPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
