import { Component, OnInit, Input, ChangeDetectorRef, ElementRef, ViewChild, TemplateRef  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { FAppService } from '../../service/fapp.service';
import {SharedService} from '../../service/shared.service';
import { PostResponse } from '../../model/PostResponse';
import { FriendResponse } from '../../model/FriendResponse';
import { postInfo } from '../../model/post.model';
import { aboutInfo } from '../../model/about.model';
import { friendInfo } from '../../model/friend.model';
import { ToastrService } from 'ngx-toastr';
import {AuthService} from '../../service/auth.service';
import * as moment from 'moment';
import { LoadingBarService, LoadingBarComponent } from '@ngx-loading-bar/core';
import { interval, timer } from 'rxjs';
import { map, take, delay, withLatestFrom, finalize, tap } from 'rxjs/operators';

@Component({
    selector: 'popover',
    templateUrl: './popover.component.html',
    styleUrls: ['./popover.component.scss']
  })
  export class PopOverComponent implements OnInit {
    @Input() MemberId: string; @Input() MIdIndex: string; @Input() Parent: any; imgPath:string; displayName:string; location:string;
    friendInfo : friendInfo; Friends: string[] = []; AllFriendsInfo: any[] = []; FriendsInfo: any[] = []; loading = false;

    @ViewChild('template', { static: true }) mymodal: ElementRef;
    
    modalRef: BsModalRef; alertTitle:string = "Alert"; alertMsg:string;
  
    constructor(private fappService:FAppService, private sharedService:SharedService, private router:Router, private toastr: ToastrService, private auth: AuthService,
        private modalService: BsModalService,private storage: AngularFireStorage, private db: AngularFirestore, private formBuilder: FormBuilder, 
        public loader: LoadingBarService) {// public loadingbarconfig : LoadingBarComponent
            this.friendInfo = new friendInfo();
            this.sharedService.popoverMId.subscribe( value => { 
              if(value !=""){
                this.friendInfo.mid = value;
                this.friendInfo.search = "";
                this.LoadFriendList(); 
              }
            });
         }
  
    ngOnInit() {
      
    }
    
    LoadFriendList(){
      if(this.AllFriendsInfo.length > 0 && this.AllFriendsInfo[0].rmid == this.friendInfo.mid){
        return;
      }
      //this.loadingbarconfig.includeSpinner = false;
      this.loader.start();
      this.AllFriendsInfo = []; this.FriendsInfo = [];
      this.fappService.LoadFriendList(this.friendInfo).subscribe(response => {
        if(response){
          this.AllFriendsInfo = []; this.FriendsInfo = [];
          for(let item of response.friendlist){
            item.rmid = this.friendInfo.mid;
            this.AllFriendsInfo.push(item);
            this.FriendsInfo.push(item);
          }
          this.endloader();
        }else{
          this.toastr.error("something gone wrong !");
        }
      });
    }
  
    private endloader(){
      this.loader.complete();
      this.loader.stop();
    }
  }
  