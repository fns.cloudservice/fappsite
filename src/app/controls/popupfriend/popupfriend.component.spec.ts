import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupfriendComponent } from './popupfriend.component';

describe('PopupfriendComponent', () => {
  let component: PopupfriendComponent;
  let fixture: ComponentFixture<PopupfriendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupfriendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupfriendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
