import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SFSComponent } from './sfs.component';

describe('SFSComponent', () => {
  let component: SFSComponent;
  let fixture: ComponentFixture<SFSComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SFSComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SFSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
