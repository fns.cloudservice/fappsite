import { Component, OnInit, Input, ChangeDetectorRef, ElementRef, ViewChild, TemplateRef  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { FAppService } from '../../service/fapp.service';
import {SharedService} from '../../service/shared.service';
import { PostResponse } from '../../model/PostResponse';
import { ProfileResponse } from '../../model/ProfileResponse';
import { FriendResponse } from '../../model/FriendResponse';
import { postInfo } from '../../model/post.model';
import { aboutInfo } from '../../model/about.model';
import { friendInfo } from '../../model/friend.model';
import { ToastrService } from 'ngx-toastr';
import {AuthService} from '../../service/auth.service';
import * as moment from 'moment';

declare function fnManagePopOver(mid, displayName):any;

@Component({
    selector: 'sfs',
    templateUrl: './sfs.component.html',
    styleUrls: ['./sfs.component.scss']
  })
  export class SFSComponent implements OnInit {
    @Input() MemberId: string; @Input() MIdIndex: string; @Input() Parent: any; imgPath:string; displayName:string; location:string;
    friendInfo : friendInfo; savedData:ProfileResponse; 

    @ViewChild('template', { static: true }) mymodal: ElementRef;
    
    modalRef: BsModalRef; alertTitle:string = "Alert"; alertMsg:string;
  
    constructor(private fappService:FAppService, private sharedService:SharedService, private router:Router, private toastr: ToastrService, private auth: AuthService,
        private modalService: BsModalService,private storage: AngularFireStorage, private db: AngularFirestore, private formBuilder: FormBuilder) {
            this.savedData = JSON.parse(localStorage.getItem("profileInfo")) as ProfileResponse;
            this.friendInfo = new friendInfo();
         }
  
    ngOnInit() {
        this.LoadFriendRequestInfo(); 
    }
    
    OnPopOver(){
      //console.log(this.MemberId);
      fnManagePopOver(this.MemberId, this.friendInfo.displayName);
    }

    LoadFriendRequestInfo(){
        this.friendInfo.mid = this.MemberId;
        this.fappService.LoadMemberInfo(this.friendInfo).subscribe(response => {
          if(response){
            const resp = response as FriendResponse;
            if(response.key == "S"){ 
              if(response.friendInfo){
                var defaultImgPath = response.friendInfo.imgPath;
                if(response.friendInfo.imgPath === undefined || response.friendInfo.imgPath == null || response.friendInfo.imgPath == ""){
                  defaultImgPath = "../assets/img/users/dummy.png";
                }
                this.friendInfo.imgPath = defaultImgPath;
                this.friendInfo.displayName = response.friendInfo.displayName;
                this.friendInfo.location = response.friendInfo.location;
                this.friendInfo.email = response.friendInfo.email;
              }
            }else{
              this.toastr.error(response.message);
            }
          }else{
            this.toastr.error("something gone wrong !");
          }
        });
      }
  
    OnManageRequest(action : string){
      this.friendInfo.mid = this.savedData.profileInfo.mid; this.friendInfo.rmid = this.MemberId; this.friendInfo.action = action;
      this.fappService.ManageRequestInfo(this.friendInfo).subscribe(response => {
        if(response){
          const resp = response as FriendResponse;
          if(response.key == "S"){ 
            this.toastr.success(response.message);
          }else{
            this.toastr.error(response.message);
          }
        }else{
          this.toastr.error("something gone wrong !");
        }
      });
    }



  }
  