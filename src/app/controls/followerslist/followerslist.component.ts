import { Component, OnInit,Input, } from '@angular/core';
import * as moment from 'moment';
import { SrvRecord } from 'dns';

@Component({
  selector: 'followerslist',
  templateUrl: './followerslist.component.html',
  styleUrls: ['./followerslist.component.scss']
})
export class FollowerslistComponent implements OnInit { 
  @Input('displayName') displayName: string;@Input('location') location:string;@Input('imgPath') imgPath:string;@Input('email') email:string;
  @Input('since') since:string;

  constructor() { }

  ngOnInit() {
    this.since = moment.unix(parseInt(this.since)/1000).format("DD MMM YYYY hh:mm a");
  }

}
