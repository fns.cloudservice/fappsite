import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FollowerslistComponent } from './followerslist.component';

describe('FollowerslistComponent', () => {
  let component: FollowerslistComponent;
  let fixture: ComponentFixture<FollowerslistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FollowerslistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FollowerslistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
