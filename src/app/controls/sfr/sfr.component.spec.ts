import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SFRComponent } from './sfr.component';

describe('SFRComponent', () => {
  let component: SFRComponent;
  let fixture: ComponentFixture<SFRComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SFRComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SFRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
