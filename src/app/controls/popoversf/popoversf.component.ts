import { Component, OnInit, Input, ChangeDetectorRef, ElementRef, ViewChild, TemplateRef  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { FAppService } from '../../service/fapp.service';
import {SharedService} from '../../service/shared.service';
import { PostResponse } from '../../model/PostResponse';
import { FriendResponse } from '../../model/FriendResponse';
import { postInfo } from '../../model/post.model';
import { aboutInfo } from '../../model/about.model';
import { friendInfo } from '../../model/friend.model';
import { ToastrService } from 'ngx-toastr';
import {AuthService} from '../../service/auth.service';
import * as moment from 'moment';

declare function fnManagePopOver(mid, displayName):any;

@Component({
    selector: 'popoversf',
    templateUrl: './popoversf.component.html',
    styleUrls: ['./popoversf.component.scss']
  })
  export class PopOverSFComponent implements OnInit {
    @Input() MemberId: string; @Input() MIdIndex: string; @Input() Parent: any; imgPath:string; displayName:string; location:string;
    friendInfo : friendInfo;

    @ViewChild('template', { static: true }) mymodal: ElementRef;
    
    modalRef: BsModalRef; alertTitle:string = "Alert"; alertMsg:string;
  
    constructor(private fappService:FAppService, private sharedService:SharedService, private router:Router, private toastr: ToastrService, private auth: AuthService,
        private modalService: BsModalService,private storage: AngularFireStorage, private db: AngularFirestore, private formBuilder: FormBuilder) {
            this.friendInfo = new friendInfo();
         }
  
    ngOnInit() {
        this.LoadmemberInfo(); 
    }
    
    OnPopOver(){
      //console.log(this.MemberId);
      fnManagePopOver(this.MemberId, this.friendInfo.displayName);
    }

    LoadmemberInfo(){
        this.friendInfo.mid = this.MemberId;
        this.fappService.LoadMemberInfo(this.friendInfo).subscribe(response => {
          if(response){
            const resp = response as FriendResponse;
            if(response.key == "S"){ 
              if(response.friendInfo){
                this.friendInfo.imgPath = response.friendInfo.imgPath;
                this.friendInfo.displayName = response.friendInfo.displayName;
                this.friendInfo.location = response.friendInfo.location;
                this.friendInfo.email = response.friendInfo.email;
              }
            }else{
              this.toastr.error(response.message);
            }
          }else{
            this.toastr.error("something gone wrong !");
          }
        });
      }
  
  }
  