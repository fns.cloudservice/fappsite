import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PopOverSFComponent } from '../popoversf/popoversf.component';

describe('PopOverSFComponent', () => {
  let component: PopOverSFComponent;
  let fixture: ComponentFixture<PopOverSFComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopOverSFComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopOverSFComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
