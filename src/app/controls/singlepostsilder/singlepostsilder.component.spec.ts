import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SinglepostsilderComponent } from './singlepostsilder.component';

describe('SinglepostsilderComponent', () => {
  let component: SinglepostsilderComponent;
  let fixture: ComponentFixture<SinglepostsilderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SinglepostsilderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SinglepostsilderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
