import { Component, OnInit, Input, ChangeDetectorRef, ElementRef, ViewChild, TemplateRef  } from '@angular/core';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { FAppService } from '../../service/fapp.service';
import {SharedService} from '../../service/shared.service';
import { PostResponse } from '../../model/PostResponse';
import { postInfo } from '../../model/post.model';
import { aboutInfo } from '../../model/about.model';
import { ToastrService } from 'ngx-toastr';
import {AuthService} from '../../service/auth.service';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap'; 
import {AlertsComponent} from "../../controls/alerts/alerts.component"; 
import * as moment from 'moment';
import lozad from 'lozad'

declare function fnInitializeCarousel(elemId):any;
declare function fnPrevNextSlideCarousel(elemId, dir):any;

@Component({
  selector: 'singlepostsilder',
  templateUrl: './singlepostsilder.component.html',
  styleUrls: ['./singlepostsilder.component.scss']
})
export class SinglepostsilderComponent implements OnInit {
  @Input() PostId: string; @Input() PostIdIndex: string; postInfo : postInfo; SliderhtmlToAdd:string; sliderImages: string [] = []; sliderDesc: string [] = []; activeItem: number = 0;
  datatarget:string; ModaldivId:string; mdlSampleIsOpen : boolean = false; dummyImgURL:string = '../assets/img/dummy.png'; sliderPreviewImages: string [] = [];
  sliderImagesOrg: string [] = []; @Input() Seqn: any;

  @ViewChild(AlertsComponent, { static: true }) alert: AlertsComponent;
  @ViewChild('carousl', { static: true }) carousl: ElementRef; 
  @ViewChild('dz', { static: true }) dz: ElementRef; 
  @ViewChild('cta', { static: true }) cta: ElementRef;
  
  constructor(private fappService:FAppService, private sharedService:SharedService, private router:Router, private toastr: ToastrService, private auth: AuthService,
    private modalService: BsModalService,private storage: AngularFireStorage, private db: AngularFirestore) {
    this.postInfo = new postInfo();
  }

  ngOnInit() {
    this.postInfo.postid = this.PostId;
    this.datatarget = "#" + this.PostId + "_Modal";
    this.ModaldivId = this.PostId + "_Modal";
    if(this.postInfo.postid === undefined){}else{ this.LoadpostInfo(); }
    this.activeItem = this.Seqn === undefined || this.Seqn == '0' ? 0 : this.Seqn - 1;
    //fnInitializeCarousel(this.PostId);
    const observer = lozad(); // lazy loads elements with default selector as '.lozad'
    observer.observe();
    
  }
  
  private openModal(open : boolean) : void {
    this.sliderImages = this.sliderImagesOrg.concat();
    this.mdlSampleIsOpen = open;
  }

  LoadpostInfo(){
    this.fappService.LoadPostInfo(this.postInfo).subscribe(response => {
      if(response){
        const resp = response as PostResponse;
        if(response.key == "S" && response.status){ 
          if(response.postInfo){
            var medias = response.postInfo.mediaJson.split("#EMR#");
            for (let i = 0; i < medias.length; i++) {
              if(i < 5){
                this.sliderPreviewImages.push(medias[i].split("#,#")[4]);
              }
              //this.sliderImages.push(medias[i].split("#,#")[4]);
              this.sliderImagesOrg.push(medias[i].split("#,#")[4]);
              this.sliderDesc.push(medias[i].split("#,#")[3]);
            }
            this.sliderImages = this.sliderImagesOrg.concat();
          }
        }else{
          this.toastr.error(response.message);
        }
      }else{
        this.toastr.error("something gone wrong !");
      }
    });
  }

  SlideClick(elemId : string, dir : any){
    if(this.activeItem == this.sliderImages.length - 1 && dir == "N")
        this.activeItem = -1;

    if(dir == "P"){
      if(this.activeItem == 0){
        this.activeItem = this.activeItem + (this.sliderImages.length - 1);
      }
      else{
        this.activeItem--;
      }
    }
    else if(dir == "N"){
      this.activeItem++;
    }
  }

  IsActiveItem(index: number) : string {
      return index == this.activeItem ? "active": "";
  }

  GetIsShowOrHideImage(index: number) : any {
    let iImgCount:any;
      
      iImgCount = parseInt(this.GetTotalImage());

      if(this.sliderPreviewImages.length > 0){
        if(index <= iImgCount)
          return true;
        else
          return false;
      }
      
  }

  GetImage(index: number) : string {
    let ImgURL: string;
console.log(this.sliderPreviewImages);
    if(this.sliderPreviewImages.length > 0){
      if(index <= this.sliderPreviewImages.length - 1)
        ImgURL = this.sliderPreviewImages[index];
      else
        ImgURL = this.sliderPreviewImages[0];
    }
    else{
      ImgURL = "../assets/img/dummy.png";
    }

    return ImgURL;
  }

  GetTotalImage() : string {
      return "+" + this.sliderImagesOrg.length.toString();
  }

  GetImgDescription() : string {
      let slidedesc: string;

      slidedesc = this.sliderDesc[this.activeItem];
      return slidedesc;
  }

  OnSelectFiles(){
    let el: HTMLElement = this.cta.nativeElement;
    el.click();
  }

  

}

