import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FAppService } from '../service/fapp.service';
import {SharedService} from '../service/shared.service';
import { ProfileResponse } from '../model/ProfileResponse';
import { profileInfo } from '../model/profile.model';
import {AuthService} from '../service/auth.service';
import { newpostInfo } from '../model/newpost.model';
import { ToastrService } from 'ngx-toastr';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { finalize, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Guid } from "guid-typescript";
import nanoid from 'nanoid/generate';

declare function fnWizardClick(e): any;
declare function fnCTAClick(): any;
declare function fnMoveNext(): any;
declare function fnMoveBack(): any;
declare function fnRemoveAlbumdiv(): any;
declare function fnInitializeCarousel():any;

@Component({
  selector: 'newpost',
  templateUrl: './newpost.component.html',
  styleUrls: ['./newpost.component.scss']
})
export class NewPostComponent implements OnInit {
  savedData:ProfileResponse; isHovering: boolean; htmlToAdd:string; imgURL: any; public imagePath; iUploadedFileCount : number;
  task: AngularFireUploadTask; percentage: Observable<number>; snapshot: Observable<any>; downloadURL: string;profileInfoData : profileInfo;
  postConent:string; fname:string; newpostInfoData : newpostInfo; objpost : newpostInfo; posttime : string; strfolder : string; strPostId : string;
  strkey :string = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; strPostJson: any; strMediaJson: any;

  @ViewChild('dz', { static: true }) dz: ElementRef;
  @ViewChild('cta', { static: true }) cta: ElementRef;

  files: File[] = [];
  filelist: newpostInfo[] = [];

  toggleHover(event: boolean) {
    this.isHovering = event;
  }

  onDrop(files: FileList) {
    //this.files = [];
    for (let i = 0; i < files.length; i++) {
      this.files.push(files.item(i));

      this.objpost = new newpostInfo();
      this.objpost.file_name = files.item(i).name;
      this.objpost.seqn = i.toString();
      this.filelist.push(this.objpost);
    }
  }

  constructor(private mehkService:FAppService, private router:Router, private sharedService: SharedService, private auth: AuthService,
    private storage: AngularFireStorage, private db: AngularFirestore, private toastr: ToastrService) {
    this.sharedService.FileEvent.subscribe( data => {
      if(data){
        console.log("data is : " + data.name);
        for (let i = 0; i < this.files.length; i++) {
          if(data.name == this.files[i].name)
            this.files.splice(i, 1);
        }
      }
    });

    this.savedData = JSON.parse(localStorage.getItem("profileInfo")) as ProfileResponse;
    if(this.savedData && this.savedData.profileInfo){
      this.profileInfoData = this.savedData.profileInfo;
    }else{
      this.profileInfoData = new  profileInfo();
    }

    var dt = new Date();
    this.posttime = Date.now().toString();
    this.strfolder = nanoid(this.strkey, 15);//this.replaceAll(Guid.create().toString(),"-","");

    this.iUploadedFileCount = 0;
    localStorage.setItem("ctab", "1");
   }

   ngOnInit() {
    this.auth.isuserLoggedIn();

  }

  ngAfterViewChecked(){

  }

  ngAfterContentChecked() {
    
  }

  OnSelectFiles(){
    let el: HTMLElement = this.cta.nativeElement;
    el.click();
  }

  OnBack(){

    fnMoveBack();
  }

  OnContinue(){
    var k =localStorage.getItem("ctab");

    if(this.postConent==''||this.postConent===undefined){
        this.toastr.warning('Post Content required!');
        return;
      }else if(k=='2'&&this.files.length==0){
        this.toastr.warning('Image Required!');
        return;
      }else{

        fnMoveNext();
      }

  }

  OnMenuClick(e : Event){
    var k=localStorage.getItem("ctab")
    if(this.postConent==""||this.postConent===undefined){
      this.toastr.warning('Post Content Required!');
      return;
    }
   else if(k=='2'&&this.files.length==0){
      this.toastr.warning('Image Required!');
      return;
    }
    else{
      fnWizardClick(e);
      fnInitializeCarousel();
    }
    fnWizardClick(e);
    fnInitializeCarousel();


  }

  getImgcontent(file : File) : any {
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
      this.objpost.img_content = reader.result;
        return reader.result;
      }
  }

  startDisplay(event: FileList) {
    //this.filelist = [];
    for (let i = 0; i < event.length; i++) {
        this.files.push(event.item(i));

        this.objpost = new newpostInfo();
        this.objpost.file_name = this.files[i].name;
        this.objpost.seqn = (i + 1).toString();
        console.log("file name : " + this.objpost.file_name + " seq : " + this.objpost.seqn);
        localStorage.setItem('Image',this.objpost.file_name)
        this.filelist.push(this.objpost);
    }
    this.sharedService.Img_desc.subscribe( value => {
      if(value.indexOf("#") > 0){
        this.fname = value.substr(value.indexOf("#") + 1);

        for(let i = 0; i < this.filelist.length; i++ ){
          if(this.filelist[i].file_name == this.fname){
            this.filelist[i].img_desc = value.substr(0,value.indexOf("#"));
          }
        }
      }
    });
    fnInitializeCarousel();
  }

  OnPostComment(){
    //this.sharedService.Img_desc.next(this.ImgDesc + "#" + this.file.name);
  }

  OnUploadFiles(){
    this.iUploadedFileCount = 0;
    this.strPostJson = []; this.strMediaJson = [];
    this.createPostJSON();
    for (let i = 0; i < this.files.length; i++) {
      this.startUpload(this.files[i]);
    }
    this.toastr.success(this.files.length + " files uploaded successfully!");
  }

  startUpload(file: File) {

    var mimeType = file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.toastr.warning(file.name + " is not uploaded! Only images are supported.");
      return;
    }

    var strfappfilename = nanoid(this.strkey, 6) + "." + file.name.substr(file.name.indexOf(".") + 1);
    //var strfappfilename = this.replaceAll(Guid.create().toString(),"-","").substr(0,6) + "." + file.name.substr(file.name.indexOf(".") + 1);

    // The storage path
    //const path = `post/${this.strfolder}/${Date.now()}_${file.name}`;
    const path = `post/${this.strfolder}/${Date.now()}_${strfappfilename}`;

    // Reference to storage bucket
    const ref = this.storage.ref(path);

    // The main task
    this.task = this.storage.upload(path, file);
//console.log("durl : " + ref.getDownloadURL);
    // Progress monitoring
    this.percentage = this.task.percentageChanges();

    this.task.snapshotChanges().pipe(
      tap(console.log),
      // The file's download URL
      finalize( async() =>  {
        //console.log("Okk");
        this.downloadURL = await ref.getDownloadURL().toPromise();

        this.db.collection('files').add( { downloadURL: this.downloadURL, path });

        var result = this.filelist.filter(obj => obj.file_name == file.name);

        this.iUploadedFileCount += 1;

        this.createMediaJSON(strfappfilename , this.downloadURL, result[0].img_desc, result[0].seqn);//file.name

        this.SavePostInfo();
      }),
    ).subscribe();
  }

  createPostJSON() {
      var item = {};

      item ["memberid"] = this.profileInfoData.mid; item ["posttime"] = this.posttime; item ["isdeleted"] = "N";
      item ["ispublic"] = "N"; item ["likes"] = "0"; item ["description"] = this.postConent;

      this.strPostJson.push(item);
  }

  createMediaJSON(filename:string, Url:string, Imgdesc : string, seqn : string) {
    var item = {};

    item ["memberid"] = this.profileInfoData.mid; item ["posttime"] = this.posttime; item ["isdeleted"] = "N";
    item ["ispublic"] = "N"; item ["img_desc"] = (Imgdesc === undefined ? "" : Imgdesc); item ["likes"] = "0";
    item ["filename"] = filename; item ["filepath"] = Url; item ["seqn"] = seqn;

    this.strMediaJson.push(item);
  }

  // setPostInfo(filename:string, Url:string, Imgdesc : string, seqn : string){
  //   console.log("file Name : " + filename + " , seqn : " + seqn);
  //   this.newpostInfoData = new newpostInfo();
  //   this.newpostInfoData.memberid = this.profileInfoData.mid; this.newpostInfoData.posttime = this.posttime; this.newpostInfoData.isdeleted = "N";
  //   this.newpostInfoData.ispublic = "N"; this.newpostInfoData.likes = "0"; //this.newpostInfoData.description = this.postConent;
  //   this.newpostInfoData.filepath = Url; this.newpostInfoData.file_name = filename; this.newpostInfoData.img_desc = (Imgdesc === undefined ? "" : Imgdesc);
  //   this.newpostInfoData.seqn = seqn; this.newpostInfoData.description = this.postConent; //this.newpostInfoData.postid = this.strPostId;
  // }

  isActive(snapshot) {
    return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes;
  }

  OnClick(){
    setTimeout(()=> { fnRemoveAlbumdiv(); } , 750);
  }

  public SavePostInfo(){
    if(this.files.length == this.iUploadedFileCount){
      this.newpostInfoData = new newpostInfo();
      this.newpostInfoData.postjson = JSON.stringify(this.strPostJson);
      this.newpostInfoData.mediajson = JSON.stringify(this.strMediaJson);
      this.mehkService.NewPostInfoFApp(this.newpostInfoData).subscribe(response => {
        if(response){
          if(response.status=="Success"){
            localStorage.setItem("pid", response.postid);
            this.strPostId = response.postid;
            //localStorage.setItem("profileInfo", JSON.stringify(this.savedData));
            //this.toastr.success(response.message);//, null, {closeButton: true}
            this.router.navigateByUrl("mypost");
          }else{
            this.toastr.error(response.message);
          }
        }else{
          this.toastr.error("something gone wrong !");
        }
      });
    }
  }

  public loadScript(url) {
    //console.log('preparing to load...')
    let node = document.createElement('script');
    node.src = url;
    node.type = 'text/javascript';
    document.getElementsByTagName('head')[0].appendChild(node);
 }

 private LoadJsFiles(){
    this.loadScript('../assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js');
    //this.loadScript('../assets/global/scripts/app.min.js');
    this.loadScript('../assets/pages/scripts/portfolio-1.min.js');
}

  escapeRegExp(string){
    return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
  }

  // replaceAll(str, term, replacement) {
  //   return str.replace(new RegExp(this.escapeRegExp(term), 'g'), replacement);
  // }

  replaceAll = function (str, find, replace):string {
    //var str = this;
    return str.replace(new RegExp(find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), replace);
  };

}
