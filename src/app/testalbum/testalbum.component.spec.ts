import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestalbumComponent } from './testalbum.component';

describe('TestalbumComponent', () => {
  let component: TestalbumComponent;
  let fixture: ComponentFixture<TestalbumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestalbumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestalbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
