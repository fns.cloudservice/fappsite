import { Component, OnInit, Input, ChangeDetectorRef, ElementRef, ViewChild, TemplateRef  } from '@angular/core';
import { Router } from '@angular/router';
import {SharedService} from '../service/shared.service';
import { MehkLoginResponse } from '../model/MehkLoginResponse';
import { DynamicScriptLoaderService } from '../service/dynamicscriptLoader.service';
import {AuthService} from '../service/auth.service';

declare function fnLoadJs(): any;
declare function fnRemoveAlbumdiv(): any;

@Component({
  selector: 'testalbum',
  templateUrl: './testalbum.component.html',
  styleUrls: ['./testalbum.component.scss']
})
export class TestalbumComponent implements OnInit {
  savedData:MehkLoginResponse;
  htmlToAdd:string;
  titleHtml:string;

  @ViewChild('cbp', { static: true }) cbp: ElementRef;

  constructor(private router:Router, private sharedService: SharedService, private dynamicScriptLoader: DynamicScriptLoaderService, private auth: AuthService) {
    console.log('constructor');
      localStorage.setItem("isdivcreated", "N");
      
      this.htmlToAdd = "<div class='cbp-item web-design logos'><div class='cbp-caption'><div class='cbp-caption-defaultWrap'><img src='../assets/global/img/portfolio/600x600/05.jpg' alt=''> </div><div class='cbp-caption-activeWrap'><div class='cbp-l-caption-alignCenter'><div class='cbp-l-caption-body'><a href='../assets/global/plugins/cubeportfolio/ajax/project2.html' class='cbp-singlePage cbp-l-caption-buttonLeft btn red uppercase btn red uppercase' rel='nofollow'>more info</a><a href='../assets/global/img/portfolio/1200x900/50.jpg' class='cbp-lightbox cbp-l-caption-buttonRight btn red uppercase btn red uppercase' style='width: 50%;' data-title=\"<div class='col col-sm-4 img-description' style='background: #ffffff;width:50%;'><div class='row'><div class='col-sm-1'>  <img src='../assets/img/profile.jpg' alt='' style='border-radius: 50%!important;'></div><div class='col-sm-11'><a href='javascript:;' style='color: #385898;font-size: 16px;font-weight: bold;margin-bottom: 3px;display: block;'>Sreenivas Sreenivas</a><span class='display-bl text-gray' style='display: block;color: #999;'>October 05, 2009</span></div></div><hr><div class='post-footer-option'><ul class='list-unstyled'><li style='float:left;margin-right:40px;padding-bottom:15px;'><a href='#'><em class='fa fa-thumbs-o-up' aria-hidden='true'></em> Like</a></li><li style='float:left;margin-right:40px;padding-bottom:15px;'><a href='#'><em class='fa fa-comment' aria-hidden='true'></em> Comment</a></li><li ><a href='#'><em class='fa fa-share' aria-hidden='true'></em> Share</a></li></ul></div><div class='row'><div class='col'><div class='comment-wrapper'><div class='panel-body'><textarea class='form-control' placeholder='write a comment...' rows='3'></textarea><br><button type='button' class='btn btn-info pull-right'>Post</button><div class='clearfix'></div></div></div></div></div></div>\" (click)='OnClick()'>view larger</a></div></div></div></div></div>";

  //     this.htmlToAdd = `<div class="cbp-item web-design logos">
  //     <div class="cbp-caption">
  //         <div class="cbp-caption-defaultWrap">
  //             <img src="../assets/global/img/portfolio/600x600/05.jpg" alt=""> </div>
  //         <div class="cbp-caption-activeWrap">
  //             <div class="cbp-l-caption-alignCenter">
  //                 <div class="cbp-l-caption-body">
  //                     <a href="../assets/global/plugins/cubeportfolio/ajax/project2.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn red uppercase btn red uppercase" rel="nofollow">more info</a>
  //                     <a href="../assets/global/img/portfolio/1200x900/50.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn red uppercase btn red uppercase" style="width: 50%;" data-title="<div class='col col-sm-4 img-description' style='background: #ffffff;width:50%;'><div class='row'><div class='col-sm-1'>  <img src='../assets/img/profile.jpg' alt='' style='border-radius: 50%!important;'></div>    
  //                         <div class='col-sm-11'><a href='javascript:;' style='color: #385898;font-size: 16px;font-weight: bold;margin-bottom: 3px;display: block;'>Sreenivas Sreenivas</a><span class='display-bl text-gray' style='display: block;color: #999;'>October 05, 2009</span></div>    
  //                         </div><hr><div class='post-footer-option'><ul class='list-unstyled'><li style='float:left;margin-right:40px;padding-bottom:15px;'><a href='#'><em class='fa fa-thumbs-o-up' aria-hidden='true'></em> Like</a></li><li style='float:left;margin-right:40px;padding-bottom:15px;'><a href='#'><em class='fa fa-comment' aria-hidden='true'></em> Comment</a></li><li ><a href='#'><em class='fa fa-share' aria-hidden='true'></em> Share</a></li></ul></div><div class='row'><div class='col'><div class='comment-wrapper'><div class='panel-body'>
  //                         <textarea class='form-control' placeholder='write a comment...' rows='3'></textarea><br><button type='button' class='btn btn-info pull-right'>Post</button><div class='clearfix'></div></div></div></div></div></div>  " (click)="OnClick()">view larger</a>
  //                 </div>
  //             </div>
  //         </div>
  //     </div>
  // </div>
  // <div class="cbp-item identity web-design hide">
  //     <div class="cbp-caption">
  //         <div class="cbp-caption-defaultWrap">
  //             <img src="../assets/global/img/portfolio/600x600/38.jpg" alt=""> </div>
  //         <div class="cbp-caption-activeWrap">
  //             <div class="cbp-l-caption-alignCenter">
  //                 <div class="cbp-l-caption-body">
  //                     <a href="../assets/global/plugins/cubeportfolio/ajax/project1.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn red uppercase btn red uppercase" rel="nofollow">more info</a>
  //                     <a href="../assets/global/img/portfolio/1200x900/4.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn red uppercase btn red uppercase" data-title="<div class='col col-sm-4 img-description' style='background: #ffffff;width:100%;'><div class='row'><div class='col-sm-1'>  <img src='../assets/img/profile.jpg' alt='' style='border-radius: 50%!important;'></div>    
  //                         <div class='col-sm-11'><a href='javascript:;' style='color: #385898;font-size: 16px;font-weight: bold;margin-bottom: 3px;display: block;'>Sreenivas Sreenivas</a><span class='display-bl text-gray' style='display: block;color: #999;'>October 05, 2009</span></div>    
  //                         </div><hr><div class='post-footer-option'><ul class='list-unstyled'><li style='float:left;margin-right:40px;padding-bottom:15px;'><a href='#'><em class='fa fa-thumbs-o-up' aria-hidden='true'></em> Like</a></li><li style='float:left;margin-right:40px;padding-bottom:15px;'><a href='#'><em class='fa fa-comment' aria-hidden='true'></em> Comment</a></li><li ><a href='#'><em class='fa fa-share' aria-hidden='true'></em> Share</a></li></ul></div><div class='rows'><div class='blog-page blog-content-2'><div class='blog-page blog-content-2'><div class='row'><div class='blog-single-content bordered1 blog-container'>
  //                                 <div class='blog-comments'><h3 class='sbold blog-comments-title' style='color:#333;'>Comments(30)</h3><div class='c-comment-list' style='color:#333;'><div class='media'><div class='media-left'><a href='#'><img class='media-object' alt='' src='../assets/pages/img/avatars/team1.jpg'></a></div><div class='media-body'><h4 class='media-heading'><a href='#'>Sean</a> on<span class='c-date'>23 May 2015, 10:40AM</span></h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. </div></div><div class='media'><div class='media-left'><a href='#'><img class='media-object' alt='' src='../assets/pages/img/avatars/team3.jpg'> </a></div><div class='media-body'><h4 class='media-heading'><a href='#'>Strong Strong</a> on<span class='c-date'>21 May 2015, 11:40AM</span></h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.<div class='media'><div class='media-left'><a href='#'><img class='media-object' alt='' src='../assets/pages/img/avatars/team4.jpg'> </a></div><div class='media-body'><h4 class='media-heading'><a href='#'>Emma Stone</a> on<span class='c-date'>30 May 2015, 9:40PM</span></h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. </div></div></div></div><div class='media'><div class='media-left'><a href='#'><img class='media-object' alt='' src='../assets/pages/img/avatars/team7.jpg'> </a></div><div class='media-body'><h4 class='media-heading'><a href='#'>Nick Nilson</a> on<span class='c-date'>30 May 2015, 9:40PM</span></h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. </div></div></div><h3 class='sbold blog-comments-title'>Leave A Comment</h3><form action='#'><div class='form-group'><textarea rows='8' name='message' placeholder='Write comment here ...' class='form-control c-square'></textarea></div><div class='form-group'><button type='submit' class='btn blue uppercase btn-md sbold btn-block'>Submit</button>
  //                                 </div></form></div></div></div></div></div><div class='col'><div class='comment-wrapper'><div class='panel-body'>
  //                         <textarea class='form-control' placeholder='write a comment...' rows='3'></textarea><br><button type='button' class='btn btn-info pull-right'>Post</button><div class='clearfix'></div></div></div></div></div></div>  " (click)="OnClick()">view larger</a>
  //                 </div>
  //             </div>
  //         </div>
  //     </div>
  // </div>
  // <div class="cbp-item identity web-design">
  //       <div class="cbp-caption">
  //           <div class="cbp-caption-defaultWrap">
  //               <img src="../assets/global/img/portfolio/600x600/88.jpg" alt=""> </div>
  //           <div class="cbp-caption-activeWrap">
  //               <div class="cbp-l-caption-alignCenter">
  //                   <div class="cbp-l-caption-body">
  //                       <a href="../assets/global/plugins/cubeportfolio/ajax/project2.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn red uppercase btn red uppercase" rel="nofollow">more info</a>
  //                       <a href="../assets/global/img/portfolio/1200x900/7.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn red uppercase btn red uppercase" data-title="Ski * Buddy<br>by Tiberiu Neamu" (click)="OnClick()">view larger</a>
  //                   </div>
  //               </div>
  //           </div>
  //       </div>
  //   </div>`;
      //this.cbp.nativeElement.
      this.titleHtml = `<div class='col col-sm-4 img-description' style='background: #ffffff;width:100%;'><div class='row'><div class='col-sm-1'>  <img src='../assets/img/profile.jpg' alt='' style='border-radius: 50%!important;'></div>    
      <div class='col-sm-11'><a href='javascript:;' style='color: #385898;font-size: 16px;font-weight: bold;margin-bottom: 3px;display: block;'>Sreenivas Sreenivas</a><span class='display-bl text-gray' style='display: block;color: #999;'>October 05, 2009</span></div>    
      </div><hr><div class='post-footer-option'><ul class='list-unstyled'><li style='float:left;margin-right:40px;padding-bottom:15px;'><a href='#'><em class='fa fa-thumbs-o-up' aria-hidden='true'></em> Like</a></li><li style='float:left;margin-right:40px;padding-bottom:15px;'><a href='#'><em class='fa fa-comment' aria-hidden='true'></em> Comment</a></li><li ><a href='#'><em class='fa fa-share' aria-hidden='true'></em> Share</a></li></ul></div><div class='rows'><div class='blog-page blog-content-2'><div class='blog-page blog-content-2'><div class='row'><div class='blog-single-content bordered1 blog-container'>
              <div class='blog-comments'><h3 class='sbold blog-comments-title' style='color:#333;'>Comments(30)</h3><div class='c-comment-list' style='color:#333;'><div class='media'><div class='media-left'><a href='#'><img class='media-object' alt='' src='../assets/pages/img/avatars/team1.jpg'></a></div><div class='media-body'><h4 class='media-heading'><a href='#'>Sean</a> on<span class='c-date'>23 May 2015, 10:40AM</span></h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. </div></div><div class='media'><div class='media-left'><a href='#'><img class='media-object' alt='' src='../assets/pages/img/avatars/team3.jpg'> </a></div><div class='media-body'><h4 class='media-heading'><a href='#'>Strong Strong</a> on<span class='c-date'>21 May 2015, 11:40AM</span></h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.<div class='media'><div class='media-left'><a href='#'><img class='media-object' alt='' src='../assets/pages/img/avatars/team4.jpg'> </a></div><div class='media-body'><h4 class='media-heading'><a href='#'>Emma Stone</a> on<span class='c-date'>30 May 2015, 9:40PM</span></h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. </div></div></div></div><div class='media'><div class='media-left'><a href='#'><img class='media-object' alt='' src='../assets/pages/img/avatars/team7.jpg'> </a></div><div class='media-body'><h4 class='media-heading'><a href='#'>Nick Nilson</a> on<span class='c-date'>30 May 2015, 9:40PM</span></h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. </div></div></div><h3 class='sbold blog-comments-title'>Leave A Comment</h3><form action='#'><div class='form-group'><textarea rows='8' name='message' placeholder='Write comment here ...' class='form-control c-square'></textarea></div><div class='form-group'><button type='submit' class='btn blue uppercase btn-md sbold btn-block'>Submit</button>
              </div></form></div></div></div></div></div><div class='col'><div class='comment-wrapper'><div class='panel-body'>
      <textarea class='form-control' placeholder='write a comment...' rows='3'></textarea><br><button type='button' class='btn btn-info pull-right'>Post</button><div class='clearfix'></div></div></div></div></div></div>`;
      this.LoadJsFiles();
   }

  ngOnInit() {
    //this.loadScripts();
    //fnLoadJs();
    console.log('ngOnInit');
    //this.LoadJsFiles();
    //this.LoadAllJsFiles();
  }

  ngAfterViewInit() {
    console.log('ngAfterViewInit');
    //We loading the player script on after view is loaded
    //this.loadScripts();
    //fnLoadJs();
    //this.LoadAllJsFiles();
    //this.LoadJsFiles();
    //fnRemoveAlbumdiv();
  }

  ngAfterContentChecked() {
    this.auth.isuserLoggedIn();
  }
  
  OnClick(){
    setTimeout(()=> { fnRemoveAlbumdiv(); } , 750);
  }

  private loadScripts() {
    // You can load multiple scripts by just providing the key as argument into load method of the service
    this.dynamicScriptLoader.load('jquery','bootstrap','js.cookie','slimscroll','jquery.blockui','bootstrap-switch','bootstrap-fileinput','jquery.sparkline',
    'bootstrap-datepicker','cubeportfolio','app.min','profile','components-date-time-pickers','portfolio1','ui-modals','layout','demo','quick-sidebar',
    'quick-nav').then(data => {
      // Script Loaded Successfully
    }).catch(error => console.log(error));
  }

  public loadScript(url) {
    //console.log('preparing to load...')
    let node = document.createElement('script');
    node.src = url;
    node.type = 'text/javascript';
    document.getElementsByTagName('head')[0].appendChild(node);
 }

 private LoadJsFiles(){
    this.loadScript('../assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min_key.js'); 
    //this.loadScript('../assets/global/scripts/app.min.js'); 
    this.loadScript('../assets/pages/scripts/portfolio-1.min.js'); 
} 

 private LoadJsFiles_old(){
    this.loadScript('../../assets/global/plugins/jquery.min.js');
    this.loadScript('../assets/global/plugins/bootstrap/js/bootstrap.min.js');
    this.loadScript('../assets/global/plugins/js.cookie.min.js');     
    this.loadScript('../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); 
    this.loadScript('../assets/global/plugins/jquery.blockui.min.js'); 
    this.loadScript('../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); 
    this.loadScript('../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js');
    this.loadScript('../assets/global/plugins/jquery.sparkline.min.js');     
    this.loadScript('../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js'); 
    this.loadScript('../assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js'); 
    this.loadScript('../assets/global/scripts/app.min.js'); 
    this.loadScript('../assets/pages/scripts/profile.min.js');
    this.loadScript('../assets/pages/scripts/components-date-time-pickers.min.js');     
    this.loadScript('../assets/pages/scripts/portfolio-1.min.js'); 
    //this.loadScript('../assets/pages/scripts/ui-modals.min.js'); 
    this.loadScript('../assets/layouts/layout4/scripts/layout.min.js'); 
    this.loadScript('../assets/layouts/layout4/scripts/demo.min.js');
    this.loadScript('../assets/layouts/global/scripts/quick-sidebar.min.js');     
    this.loadScript('../assets/layouts/global/scripts/quick-nav.min.js'); 
 }

 private LoadAllJsFiles(){
  this.loadScript('../../assets/global/plugins/jquery.min.js');
  this.loadScript('../assets/global/plugins/bootstrap/js/bootstrap.min.js');
  this.loadScript('../assets/global/plugins/js.cookie.min.js');     
  this.loadScript('../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); 
  this.loadScript('../assets/global/plugins/jquery.blockui.min.js'); 
  this.loadScript('../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
  this.loadScript('../assets/global/plugins/jquery-ui/jquery-ui.min.js');  
  this.loadScript('../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js');
  this.loadScript('../assets/global/plugins/jquery.sparkline.min.js');     
  this.loadScript('../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js'); 
  this.loadScript('../assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js'); 
  this.loadScript('../assets/global/plugins/select2/js/select2.full.min.js');
  this.loadScript('../assets/global/scripts/app.min.js'); 
  this.loadScript('../assets/pages/scripts/profile.min.js');
  this.loadScript('../assets/apps/scripts/todo-2.min.js');
  this.loadScript('../assets/pages/scripts/components-date-time-pickers.min.js');     
  this.loadScript('../assets/pages/scripts/portfolio-1.min.js'); 
  this.loadScript('../assets/pages/scripts/ui-modals.min.js'); 
  this.loadScript('../assets/layouts/layout4/scripts/layout.min.js'); 
  this.loadScript('../assets/layouts/layout4/scripts/demo.min.js');
  this.loadScript('../assets/layouts/global/scripts/quick-sidebar.min.js');     
  this.loadScript('../assets/layouts/global/scripts/quick-nav.min.js'); 
}

}
