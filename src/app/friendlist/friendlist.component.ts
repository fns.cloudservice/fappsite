import { MehkLoginResponse } from '../model/MehkLoginResponse';
import { Component, OnInit, ElementRef, ViewChild, HostListener } from '@angular/core';
import {AlertsComponent} from "../controls/alerts/alerts.component"; 
import { Router, NavigationEnd } from '@angular/router';
import { FAppService } from '../service/fapp.service';
import { SharedService } from '../service/shared.service';
import { ConfirmService } from '../service/confirm.service';
import { ProfileResponse } from '../model/ProfileResponse';
import { profileInfo } from '../model/profile.model';
import { FriendResponse } from '../model/FriendResponse';
import { friendInfo } from '../model/friend.model';
import { ToastrService } from 'ngx-toastr';
import {AuthService} from '../service/auth.service';
import { Observable } from 'rxjs';
import * as _ from 'lodash';
import { LoadingBarService, LoadingBarComponent } from '@ngx-loading-bar/core';
import { interval, timer } from 'rxjs';
import { map, take, delay, withLatestFrom, finalize, tap } from 'rxjs/operators';

declare function fnSetActiveli(obj):any;
declare function fnManagePopOver(mid, displayName):any;
declare function fnHidePopOver(): any;

@Component({
  selector: 'friendlist',
  templateUrl: './friendlist.component.html',
  styleUrls: ['./friendlist.component.scss']
})
export class FriendlistComponent implements OnInit {
  AllFriendsInfo: any[] = []; FriendsInfo: any[] = []; friendInfo : friendInfo; savedData:ProfileResponse; profileInfoData : profileInfo; FriendsCount:any;
  FriendRequests: any[] = []; AllFriendRequests: any[] = [];FriendRequestCount:any; PopOverMId: string; IsOpened:string;

  constructor(private fappService:FAppService, private sharedService:SharedService, private router:Router, private toastr: ToastrService, 
    private auth: AuthService, private confirmService: ConfirmService, public loader: LoadingBarService) {
      this.savedData = JSON.parse(localStorage.getItem("profileInfo")) as ProfileResponse;
      this.friendInfo = new friendInfo();
    
   }

   @HostListener("click") onClick(){
     if(this.IsOpened != "Y"){
      fnHidePopOver();
     }
     else{
      this.IsOpened = "N"
     }
    }

  ngOnInit() {
    this.auth.isuserLoggedIn();
    this.LoadFriendList(); 
    //this.LoadFriendRequestList();
  }
  
  ngAfterContentChecked() {
    
  }

  OnPopOver(MemberId : string, displayName : string){
    this.sharedService.popoverMId.next(MemberId);
    fnManagePopOver(MemberId, displayName);
    this.IsOpened = "Y";
  }

  OnliClick(liName : string){
    fnSetActiveli(liName);
  }

  LoadFriendRequestList(){
    this.FriendRequests = []; this.AllFriendRequests = [];
    this.friendInfo.mid = this.savedData.profileInfo.mid;
    if(this.friendInfo.search == null){
      this.friendInfo.search = "";
    }
    this.fappService.LoadFriendRequestList(this.friendInfo).subscribe(response => {
      if(response){
        const resp = response as FriendResponse;
      
        if(response.key == "S" && response.status){ 
          if(response.friendInfo)
            var mIds = response.friendInfo.mids.split(",");
            if(mIds === undefined || mIds == null){
              this.FriendRequestCount = 0;
              return;
            }
            this.FriendRequestCount = mIds.length;
            for (let i = 0; i < mIds.length; i++) {
              this.FriendRequests.push(mIds[i]);
            }
        }else{
          this.toastr.error(response.message);
        }
      }else{
        this.toastr.error("something gone wrong !");
      }
    });
  }

  LoadFriendList(){
    this.AllFriendsInfo = []; this.FriendsInfo = [];
    this.friendInfo.mid = this.savedData.profileInfo.mid;
    if(this.friendInfo.search == null){
      this.friendInfo.search = "";
    }
    this.loader.start();
    this.fappService.LoadFriendList(this.friendInfo).subscribe(response => {
      this.FriendsCount = 0;
      if(response){
        if(response.friendlist != null){
          console.log("response Load friend lIst ",response)
          for(let item of response.friendlist){
            this.AllFriendsInfo.push(item);
            this.FriendsInfo.push(item);
          }
        }
        if(response.friendrequest != null){
          for(let item of response.friendrequest){
            this.AllFriendRequests.push(item);
            this.FriendRequests.push(item);
          }
        }
        this.FriendsCount = this.FriendsInfo.length;
        this.FriendRequestCount = this.FriendRequests.length;
      }else{
        this.toastr.error("something gone wrong !");
      }
      this.endloader();
    });
  }

  private endloader(){
    this.loader.complete();
    this.loader.stop();
  }

  OnManageRequest(mid : string, rmid : string, displayName : string, action : string){
    var title = 'Confirm ';
    var msg = 'Do you really want to ';
    if(action == "A"){
      msg +=  " approve " + displayName; title += "approve";
    }else if(action == "R"){
      msg +=  " reject " + displayName; title += "reject";
    }else{
      msg +=  " unfriend " + displayName; title += "unfriend";
    }
    this.confirmService.confirm({ title:title, message: msg + '?' }).then(
      () => {
        this.friendInfo.mid = mid; this.friendInfo.rmid = rmid; 
        this.friendInfo.action = action; this.friendInfo.since = Date.now().toString();
        this.fappService.ManageRequestInfo(this.friendInfo).subscribe(response => {
          if(response){
            const resp = response as FriendResponse;
            if(response.key == "S"){ 
              this.toastr.success(response.message);
              this.LoadFriendList(); this.LoadFriendRequestList();
            }else{
              this.toastr.error(response.message);
            }
          }else{
            this.toastr.error("something gone wrong !");
          }
        });
      },
      () => {
        //console.log('not deleting...');
      });
    
  }

  OnSearchFriends(){
    this.FriendsInfo = this.filterFriendList(); 
    this.FriendRequests = this.filterFriendRequestList(); 
  }

  private filterFriendList() {
    if(this.friendInfo.search != ''){
      return this.AllFriendsInfo.filter(item => item.displayName.toLowerCase().indexOf(this.friendInfo.search.toLowerCase()) > -1);
    }else{
      return this.AllFriendsInfo;
    }
  }

  private filterFriendRequestList() {
    if(this.friendInfo.search != ''){
      return this.AllFriendRequests.filter(item => item.displayName.toLowerCase().indexOf(this.friendInfo.search.toLowerCase()) > -1);
    }else{
      return this.AllFriendRequests;
    }
  }

}
