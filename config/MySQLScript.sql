CREATE TABLE fans (
	mid VARCHAR(21) NOT NULL,
	fanid VARCHAR(21) NOT NULL,
	since INT NULL,
	CONSTRAINT "primary" PRIMARY KEY (mid ASC, fanid ASC),
	FAMILY "primary" (mid, fanid, since)
);

-- Drop table

-- DROP TABLE public.fmate;

CREATE TABLE fmate (
	mid VARCHAR(21) NOT NULL,
	mateid VARCHAR(21) NOT NULL,
	since INT8 NULL,
	act VARCHAR NULL DEFAULT 'Y':::VARCHAR,
	CONSTRAINT "primary" PRIMARY KEY (mid ASC, mateid ASC),
	FAMILY "primary" (mid, mateid, since, act)
);

-- Drop table

-- DROP TABLE public.freq;

CREATE TABLE freq (
	mid VARCHAR NULL,
	reqmid VARCHAR NULL,
	reqon INT8 NULL,
	FAMILY "primary" (rowid, mid, reqmid, reqon)
);

-- Drop table

-- DROP TABLE public.media;

CREATE TABLE media (
	mid VARCHAR(200) NOT NULL,
	posttime INT8 NOT NULL,
	seqn INT8 NOT NULL,
	postid VARCHAR(200) NULL,
	mediaid VARCHAR(200) NULL,
	isdeleted BOOL NULL,
	descrip VARCHAR NULL,
	diaryid VARCHAR NULL,
	folname VARCHAR NULL,
	filname VARCHAR NULL,
	filpath VARCHAR NULL,
	ispubl BOOL NULL,
	likes INT8 NULL,
	CONSTRAINT "primary" PRIMARY KEY (mid ASC, posttime ASC, seqn ASC),
	FAMILY "primary" (mid, posttime, seqn, postid, mediaid, isdeleted, descrip, diaryid, folname, filname, filpath, ispubl, likes)
);

-- Drop table

-- DROP TABLE public.post;

CREATE TABLE post (
	mid VARCHAR(200) NOT NULL,
	posttime INT8 NOT NULL,
	postid VARCHAR(200) NULL,
	isdeleted BOOL NULL,
	descrip VARCHAR NULL,
	ispubl BOOL NULL,
	likes INT8 NULL,
	CONSTRAINT "primary" PRIMARY KEY (mid ASC, posttime ASC),
	FAMILY "primary" (mid, posttime, postid, isdeleted, descrip, ispubl, likes)
);

-- Drop table

-- DROP TABLE public.usr;

CREATE TABLE usr (
	id VARCHAR(48) NOT NULL,
	abot VARCHAR NULL,
	emal VARCHAR(100) NULL,
	fmnm VARCHAR(100) NULL,
	gvnm VARCHAR(100) NULL,
	intr VARCHAR(2048) NULL,
	lang VARCHAR(150) NULL,
	locn VARCHAR(200) NULL,
	deac BOOL NULL,
	deat INT8 NULL,
	dsnm VARCHAR(100) NULL,
	del BOOL NULL,
	delt INT8 NULL,
	fans INT8 NULL,
	gend VARCHAR(1) NULL,
	mid VARCHAR(200) NULL,
	profid VARCHAR(50) NULL,
	path VARCHAR(300) NULL,
	pid VARCHAR(75) NULL,
	CONSTRAINT "primary" PRIMARY KEY (id ASC),
	FAMILY "primary" (id, abot, emal, fmnm, gvnm, intr, lang, locn, deac, deat, dsnm, del, delt, fans, gend, mid, profid, path, pid)
);
===============================================================
CREATE TABLE usr (
	id VARCHAR(48) NOT NULL,
	abot VARCHAR(2500) NULL,
	emal VARCHAR(100) NULL,
	fmnm VARCHAR(100) NULL,
	gvnm VARCHAR(100) NULL,
	intr VARCHAR(2048) NULL,
	lang VARCHAR(150) NULL,
	locn VARCHAR(200) NULL,
	deac BOOL NULL,
	deat INT8 NULL,
	dsnm VARCHAR(100) NULL,
	del BOOL NULL,
	delt INT8 NULL,
	fans INT8 NULL,
	gend VARCHAR(1) NULL,
	mid VARCHAR(200) NULL,
	profid VARCHAR(50) NULL,
	path VARCHAR(300) NULL,
	pid VARCHAR(75) NULL,
    PRIMARY KEY (id ASC)
);

INSERT INTO usr (id,abot,emal,fmnm,gvnm,intr,lang,locn,deac,deat,dsnm,del,delt,fans,gend,mid,profid,path,pid) VALUES 
('2adC3yjWDdd0liLrBLa9Ho09oEm2','','victorykingsrg@gmail.com','','','','','Saidapet',false,NULL,'Victory King',false,NULL,NULL,'','46UDGVQ4JU5GV4VTD46KV',NULL,'https://firebasestorage.googleapis.com/v0/b/fnsapp-c2770.appspot.com/o/fns%2F1568904601675_user-12.jpg?alt=media&token=6304bf11-9508-4910-83a3-07ef3b69839d','')
INSERT INTO usr (id,abot,emal,fmnm,gvnm,intr,lang,locn,deac,deat,dsnm,del,delt,fans,gend,mid,profid,path,pid) VALUES
('UuSk1XEIePeeJGowVJjam0IorBs2','','vijayacaterers52@gmail.com','','','','','Kodungaiyur',false,NULL,'Vijaya M',false,NULL,NULL,'','54GfV44f3K54HY54fYETG',NULL,'https://firebasestorage.googleapis.com/v0/b/fnsapp-c2770.appspot.com/o/fns%2F1571927966792_avatar80_7.jpg?alt=media&token=45097033-947b-412d-ad6a-d5c144417eef','')
INSERT INTO usr (id,abot,emal,fmnm,gvnm,intr,lang,locn,deac,deat,dsnm,del,delt,fans,gend,mid,profid,path,pid) VALUES
('f3GzPtkbNOarU9oaqg7kS51esAu2','agd','srinivasan.x.munuswamy@gmail.com','Srinivasan','M','agd','sag','Kotturpuram',false,NULL,'Srinivasan M',false,NULL,NULL,'M','6H6J45Y5IU46YQ5G53fGG',NULL,'https://firebasestorage.googleapis.com/v0/b/fnsapp-c2770.appspot.com/o/fns%2F1572325262350_avatar80_3.jpg?alt=media&token=2157f1d2-4d9f-4378-8536-9df2e5c67e6d','srini')
INSERT INTO usr (id,abot,emal,fmnm,gvnm,intr,lang,locn,deac,deat,dsnm,del,delt,fans,gend,mid,profid,path,pid) VALUES
('mN1iQC7WxhhoNDV8yWNII9anBeE2','','saravanantrainerno1@gmail.com','','','','','Adambakkam',false,NULL,'saravanan M',false,NULL,NULL,'','GV3YI5GTfYDfUEUDfUJK4',NULL,'https://firebasestorage.googleapis.com/v0/b/fnsapp-c2770.appspot.com/o/fns%2F1571928057559_avatar3.jpg?alt=media&token=1470d233-ea85-446c-9008-09b085d37491','')

CREATE TABLE post (
	mid VARCHAR(200) NOT NULL,
	posttime BIGINT NOT NULL,
	postid VARCHAR(200) NULL,
	isdeleted CHAR(1),
	descrip VARCHAR(8000) NULL,
	ispubl CHAR(1) NULL,
	likes INT NULL,
    PRIMARY KEY(mid ASC, posttime ASC)
)

CREATE TABLE media (
	mid VARCHAR(200) NOT NULL,
	posttime BIGINT NOT NULL,
	seqn INT NOT NULL,
	postid VARCHAR(200) NULL,
	mediaid VARCHAR(200) NULL,
	isdeleted VARCHAR(2) NULL,
	descrip VARCHAR(8000) NULL,
	diaryid VARCHAR(250) NULL,
	folname VARCHAR(50) NULL,
	filname VARCHAR(150) NULL,
	filpath VARCHAR(250) NULL,
	ispubl VARCHAR(2) NULL,
	likes INT NULL,
	PRIMARY KEY (mid ASC, posttime ASC, seqn ASC)
);

CREATE TABLE freq (
	mid VARCHAR(150) NULL,
	reqmid VARCHAR(150) NULL,
	reqon BIGINT NULL
)

CREATE TABLE fmate (
	mid VARCHAR(21) NOT NULL,
	mateid VARCHAR(21) NOT NULL,
	since BIGINT NULL,
	act VARCHAR(5) NULL DEFAULT 'Y',
	PRIMARY KEY (mid ASC, mateid ASC)
);

CREATE TABLE fans (
	mid VARCHAR(21) NOT NULL,
	fanid VARCHAR(21) NOT NULL,
	since BIGINT NULL,
	PRIMARY KEY (mid ASC, fanid ASC)
);

CREATE TABLE nfeed (
	mid VARCHAR(50) NOT NULL,
	postid VARCHAR(50) NOT NULL,
	posttime BIGINT NULL
);
CREATE TABLE nlive (
	mid VARCHAR(50) NOT NULL,
	conid VARCHAR(50) NOT NULL,
	crtime BIGINT NULL,
	cpage VARCHAR(50) NULL
);
CREATE TABLE notify (
	mid VARCHAR(50) NOT NULL,
	postid VARCHAR(50) NOT NULL,
	posttime BIGINT NOT NULL,
	msg VARCHAR(250) NOT NULL,
	isviewed VARCHAR(5) NOT NULL
);
CREATE TABLE chat (
	fmid VARCHAR(50) NOT NULL,
	tmid VARCHAR(50) NOT NULL,
	msg VARCHAR(2500) NOT NULL,
	msgtime BIGINT NOT NULL,
	flag VARCHAR(10) NOT NULL
);
CREATE TABLE cmt (
	mid VARCHAR(50) NOT NULL,
	pid VARCHAR(50) NOT NULL,
	seqn INT NOT NULL,
	cmt VARCHAR(2500) NOT NULL,
	cmttime BIGINT NOT NULL,
	act VARCHAR(10) NOT NULL
);
CREATE TABLE follow (
	mid 		VARCHAR(50) NOT NULL,
	fmid		VARCHAR(50) NOT NULL,
	since		BIGINT NOT NULL,
	unfollow 	BIGINT,
	ufolwdBy 	VARCHAR(50)

)