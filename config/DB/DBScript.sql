CREATE DATABASE fdb;
CREATE USER sanjai;
/*GRANT ALL ON DATABASE fdb TO sanjai;*/
USE fdb;
CREATE TABLE USR
(
    ID STRING(48) NOT NULL primary key,
    ABOT STRING,
    EMAL STRING(100),
    FMNM STRING(100),
    GVNM STRING(100),
    INTR STRING(2048),
    LANG STRING(150),
    LOCN STRING(200),
    DEAC BOOL,
    DEAT INT,
    DSNM STRING(100),
    DEL BOOL,
    DELT INT,
    FANS INT,
    GEND STRING(1),
    MID STRING(21),
    PID STRING(75),
    PROFID STRING(50),
    PATH STRING(300)
);
INSERT INTO USR
VALUES
    ('gsanjairaj', 'about', 'email', 'firstname', 'givenname', 'interests', 'languages', 'location',false, '20165', 'sanjai', false, '4532423', '32', NULL, NULL, null,NULL);

CREATE TABLE POST
(
    MID varchar(200) NOT NULL,
    POSTTIME INT,
    POSTID varchar(200),
    ISDELETED BOOL,
    DESCRIP STRING,
    ISPUBL BOOL,
    LIKES INT,
    PRIMARY KEY (MID, POSTTIME)
);

CREATE TABLE MEDIA
(
    MID varchar(200) NOT NULL,
    POSTTIME INT,
    SEQN INT,
    POSTID varchar(200),
    MEDIAID varchar(200),
    ISDELETED BOOL,
    DESCRIP STRING,
    DIARYID STRING,
    FOLNAME STRING,
    FILNAME STRING,
    FILPATH STRING,
    ISPUBL BOOL,
    LIKES INT,
    PRIMARY KEY (MID, POSTTIME, SEQN)
);

CREATE TABLE FANS
(
    MID STRING(21) NOT NULL,
    FANID STRING(21) NOT NULL,
    SINCE INT,
    PRIMARY KEY (MID, FANID)
);

INSERT INTO FANS
VALUES
    ('gsanjairaj', 'victory', '2016036');

CREATE TABLE FMATE
(
    MID STRING(21) NOT NULL,
    MATEID STRING(21) NOT NULL,
    SINCE INT,
    PRIMARY KEY (MID, MATEID)
);

INSERT INTO FMATE
VALUES ('gsanjairaj', 'victory', '20160326');

/* By Srini */
CREATE TABLE fappid (
    mid numeric NOT null,
    postid numeric NOT null,
    mediaid numeric NOT null
);

insert into fappid (mid,postid,mediaid)values(1,1,1);


